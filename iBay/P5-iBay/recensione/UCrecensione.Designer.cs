﻿namespace P5_iBay
{
    partial class UCrecensione
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCrecensione));
            this.lbl_nome = new System.Windows.Forms.Label();
            this.tb_testo = new System.Windows.Forms.TextBox();
            this.pb_star_5 = new System.Windows.Forms.PictureBox();
            this.pb_star_4 = new System.Windows.Forms.PictureBox();
            this.pb_star_3 = new System.Windows.Forms.PictureBox();
            this.pb_star_2 = new System.Windows.Forms.PictureBox();
            this.pb_star_1 = new System.Windows.Forms.PictureBox();
            this.btModifica = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pb_star_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_star_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_star_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_star_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_star_1)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_nome
            // 
            this.lbl_nome.AutoSize = true;
            this.lbl_nome.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_nome.Location = new System.Drawing.Point(4, 4);
            this.lbl_nome.Name = "lbl_nome";
            this.lbl_nome.Size = new System.Drawing.Size(297, 31);
            this.lbl_nome.TabIndex = 0;
            this.lbl_nome.Text = "Nome Cognome Autore";
            // 
            // tb_testo
            // 
            this.tb_testo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_testo.Location = new System.Drawing.Point(3, 38);
            this.tb_testo.Multiline = true;
            this.tb_testo.Name = "tb_testo";
            this.tb_testo.Size = new System.Drawing.Size(738, 230);
            this.tb_testo.TabIndex = 1;
            this.tb_testo.Text = "Recensione dell\'utente";
            // 
            // pb_star_5
            // 
            this.pb_star_5.Image = global::P5_iBay.Properties.Resources.star;
            this.pb_star_5.Location = new System.Drawing.Point(451, 5);
            this.pb_star_5.Name = "pb_star_5";
            this.pb_star_5.Size = new System.Drawing.Size(30, 30);
            this.pb_star_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_star_5.TabIndex = 2;
            this.pb_star_5.TabStop = false;
            this.pb_star_5.Visible = false;
            // 
            // pb_star_4
            // 
            this.pb_star_4.Image = ((System.Drawing.Image)(resources.GetObject("pb_star_4.Image")));
            this.pb_star_4.Location = new System.Drawing.Point(415, 5);
            this.pb_star_4.Name = "pb_star_4";
            this.pb_star_4.Size = new System.Drawing.Size(30, 30);
            this.pb_star_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_star_4.TabIndex = 3;
            this.pb_star_4.TabStop = false;
            this.pb_star_4.Visible = false;
            // 
            // pb_star_3
            // 
            this.pb_star_3.Image = ((System.Drawing.Image)(resources.GetObject("pb_star_3.Image")));
            this.pb_star_3.Location = new System.Drawing.Point(379, 5);
            this.pb_star_3.Name = "pb_star_3";
            this.pb_star_3.Size = new System.Drawing.Size(30, 30);
            this.pb_star_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_star_3.TabIndex = 4;
            this.pb_star_3.TabStop = false;
            this.pb_star_3.Visible = false;
            // 
            // pb_star_2
            // 
            this.pb_star_2.Image = ((System.Drawing.Image)(resources.GetObject("pb_star_2.Image")));
            this.pb_star_2.Location = new System.Drawing.Point(343, 5);
            this.pb_star_2.Name = "pb_star_2";
            this.pb_star_2.Size = new System.Drawing.Size(30, 30);
            this.pb_star_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_star_2.TabIndex = 5;
            this.pb_star_2.TabStop = false;
            this.pb_star_2.Visible = false;
            // 
            // pb_star_1
            // 
            this.pb_star_1.Image = ((System.Drawing.Image)(resources.GetObject("pb_star_1.Image")));
            this.pb_star_1.Location = new System.Drawing.Point(307, 5);
            this.pb_star_1.Name = "pb_star_1";
            this.pb_star_1.Size = new System.Drawing.Size(30, 30);
            this.pb_star_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_star_1.TabIndex = 6;
            this.pb_star_1.TabStop = false;
            this.pb_star_1.Visible = false;
            // 
            // btModifica
            // 
            this.btModifica.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btModifica.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(61)))), ((int)(((byte)(124)))));
            this.btModifica.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btModifica.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.btModifica.ForeColor = System.Drawing.Color.White;
            this.btModifica.Location = new System.Drawing.Point(563, 5);
            this.btModifica.Name = "btModifica";
            this.btModifica.Size = new System.Drawing.Size(178, 30);
            this.btModifica.TabIndex = 33;
            this.btModifica.Text = "Modifica";
            this.btModifica.UseVisualStyleBackColor = false;
            this.btModifica.Visible = false;
            this.btModifica.Click += new System.EventHandler(this.btModifica_Click);
            // 
            // UCrecensione
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btModifica);
            this.Controls.Add(this.pb_star_1);
            this.Controls.Add(this.pb_star_2);
            this.Controls.Add(this.pb_star_3);
            this.Controls.Add(this.pb_star_4);
            this.Controls.Add(this.pb_star_5);
            this.Controls.Add(this.tb_testo);
            this.Controls.Add(this.lbl_nome);
            this.Name = "UCrecensione";
            this.Size = new System.Drawing.Size(744, 271);
            ((System.ComponentModel.ISupportInitialize)(this.pb_star_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_star_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_star_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_star_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_star_1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_nome;
        private System.Windows.Forms.TextBox tb_testo;
        private System.Windows.Forms.PictureBox pb_star_5;
        private System.Windows.Forms.PictureBox pb_star_4;
        private System.Windows.Forms.PictureBox pb_star_3;
        private System.Windows.Forms.PictureBox pb_star_2;
        private System.Windows.Forms.PictureBox pb_star_1;
        private System.Windows.Forms.Button btModifica;
    }
}
