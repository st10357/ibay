﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySqlConnector;

namespace P5_iBay
{
    // Parte di Filonzi Jacopo
    public partial class UCrecensione : UserControl
    {
        #region Variabili
        int _IDRecensione;
        #endregion

        #region Costruttore
        /// <summary>
        /// Display della recensione
        /// </summary>
        /// <param name="idRecensione">id della recensione da mostrare</param>
        public UCrecensione(int idRecensione)
        {
            InitializeComponent();

            // Memorizzo l'ID della recensione visualizzata
            _IDRecensione = idRecensione;

            // Dichiarazione variabili
            int stelle = 0;
            string text = "";
            string email = "";
            string nomeUtente = "";
            var connection = new MySqlConnection(Program.CONNECTION_STRING);
            MySqlCommand command;
            MySqlDataReader reader;

            // Ottenimento recensione dal DB
            try
            {
                connection.Open();

                command = new MySqlCommand($"SELECT * FROM recensioni WHERE ID='{idRecensione}'", connection);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    stelle = reader.GetInt16(3);
                    text = reader.GetString(4);
                    email = reader.GetString(2);

                    // Se la recensione è dell'utente collegato consenti la modifica
                    if (email == Program._email)
                        btModifica.Visible = true;
                }

                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            // Ottenimento nome e cognome recensitore dal DB
            try
            {
                connection.Open();

                command = new MySqlCommand($"SELECT Nome, Cognome FROM utenti WHERE Email='{email}'", connection);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    nomeUtente = $"{reader.GetString(0)} {reader.GetString(1)}";
                }

                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            // Carico le informazioni ottenute sui controlli grafici
            popola(stelle, text, nomeUtente);
        }
        #endregion

        #region Metodi
        /// <summary>
        /// Carica le informazioni della recensione sui vari controlli grafici
        /// </summary>
        /// <param name="stelle"></param>
        /// <param name="contenuto"></param>
        /// <param name="nomeUtente"></param>
        private void popola(int stelle, string contenuto, string nomeUtente)
        {
            lbl_nome.Text = nomeUtente;
            tb_testo.Text = contenuto;

            if (stelle >= 1)
                pb_star_1.Visible = true;

            if (stelle >= 2)
                pb_star_2.Visible = true;

            if (stelle >= 3)
                pb_star_3.Visible = true;

            if (stelle >= 4)
                pb_star_4.Visible = true;

            if (stelle >= 5)
                pb_star_5.Visible = true;
        }
        #endregion

        #region Eventi
        private void btModifica_Click(object sender, EventArgs e)
        {
            FrmScriviRecensione frmScriviRecensione = new FrmScriviRecensione(-1, _IDRecensione);
            frmScriviRecensione.Show();
        }
        #endregion
    }
}
