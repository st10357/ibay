﻿namespace P5_iBay
{
    partial class FrmScriviRecensione
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmScriviRecensione));
            this.label1 = new System.Windows.Forms.Label();
            this.tbTesto = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btSalva = new System.Windows.Forms.Button();
            this.btAnnulla = new System.Windows.Forms.Button();
            this.pb_star_1 = new System.Windows.Forms.PictureBox();
            this.pb_star_2 = new System.Windows.Forms.PictureBox();
            this.pb_star_3 = new System.Windows.Forms.PictureBox();
            this.pb_star_4 = new System.Windows.Forms.PictureBox();
            this.pb_star_5 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pb_star_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_star_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_star_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_star_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_star_5)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Stelle:";
            // 
            // tbTesto
            // 
            this.tbTesto.Location = new System.Drawing.Point(18, 77);
            this.tbTesto.Multiline = true;
            this.tbTesto.Name = "tbTesto";
            this.tbTesto.Size = new System.Drawing.Size(383, 241);
            this.tbTesto.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Testo:";
            // 
            // btSalva
            // 
            this.btSalva.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(61)))), ((int)(((byte)(124)))));
            this.btSalva.ForeColor = System.Drawing.Color.White;
            this.btSalva.Location = new System.Drawing.Point(301, 324);
            this.btSalva.Name = "btSalva";
            this.btSalva.Size = new System.Drawing.Size(100, 23);
            this.btSalva.TabIndex = 17;
            this.btSalva.Text = "Salva";
            this.btSalva.UseVisualStyleBackColor = false;
            this.btSalva.Click += new System.EventHandler(this.btSalva_Click);
            // 
            // btAnnulla
            // 
            this.btAnnulla.Location = new System.Drawing.Point(187, 324);
            this.btAnnulla.Name = "btAnnulla";
            this.btAnnulla.Size = new System.Drawing.Size(100, 23);
            this.btAnnulla.TabIndex = 16;
            this.btAnnulla.Text = "Annulla";
            this.btAnnulla.UseVisualStyleBackColor = true;
            this.btAnnulla.Click += new System.EventHandler(this.btAnnulla_Click);
            // 
            // pb_star_1
            // 
            this.pb_star_1.Image = ((System.Drawing.Image)(resources.GetObject("pb_star_1.Image")));
            this.pb_star_1.Location = new System.Drawing.Point(18, 28);
            this.pb_star_1.Name = "pb_star_1";
            this.pb_star_1.Size = new System.Drawing.Size(30, 30);
            this.pb_star_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_star_1.TabIndex = 11;
            this.pb_star_1.TabStop = false;
            this.pb_star_1.Click += new System.EventHandler(this.pb_star_1_Click);
            // 
            // pb_star_2
            // 
            this.pb_star_2.Image = ((System.Drawing.Image)(resources.GetObject("pb_star_2.Image")));
            this.pb_star_2.Location = new System.Drawing.Point(54, 28);
            this.pb_star_2.Name = "pb_star_2";
            this.pb_star_2.Size = new System.Drawing.Size(30, 30);
            this.pb_star_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_star_2.TabIndex = 10;
            this.pb_star_2.TabStop = false;
            this.pb_star_2.Click += new System.EventHandler(this.pb_star_2_Click);
            // 
            // pb_star_3
            // 
            this.pb_star_3.Image = ((System.Drawing.Image)(resources.GetObject("pb_star_3.Image")));
            this.pb_star_3.Location = new System.Drawing.Point(90, 28);
            this.pb_star_3.Name = "pb_star_3";
            this.pb_star_3.Size = new System.Drawing.Size(30, 30);
            this.pb_star_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_star_3.TabIndex = 9;
            this.pb_star_3.TabStop = false;
            this.pb_star_3.Click += new System.EventHandler(this.pb_star_3_Click);
            // 
            // pb_star_4
            // 
            this.pb_star_4.Image = ((System.Drawing.Image)(resources.GetObject("pb_star_4.Image")));
            this.pb_star_4.Location = new System.Drawing.Point(126, 28);
            this.pb_star_4.Name = "pb_star_4";
            this.pb_star_4.Size = new System.Drawing.Size(30, 30);
            this.pb_star_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_star_4.TabIndex = 8;
            this.pb_star_4.TabStop = false;
            this.pb_star_4.Click += new System.EventHandler(this.pb_star_4_Click);
            // 
            // pb_star_5
            // 
            this.pb_star_5.Image = ((System.Drawing.Image)(resources.GetObject("pb_star_5.Image")));
            this.pb_star_5.Location = new System.Drawing.Point(162, 28);
            this.pb_star_5.Name = "pb_star_5";
            this.pb_star_5.Size = new System.Drawing.Size(30, 30);
            this.pb_star_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_star_5.TabIndex = 7;
            this.pb_star_5.TabStop = false;
            this.pb_star_5.Click += new System.EventHandler(this.pb_star_5_Click);
            // 
            // FrmScriviRecensione
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(416, 358);
            this.Controls.Add(this.btSalva);
            this.Controls.Add(this.btAnnulla);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbTesto);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pb_star_1);
            this.Controls.Add(this.pb_star_2);
            this.Controls.Add(this.pb_star_3);
            this.Controls.Add(this.pb_star_4);
            this.Controls.Add(this.pb_star_5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FrmScriviRecensione";
            this.Text = "Scrivi recensione";
            ((System.ComponentModel.ISupportInitialize)(this.pb_star_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_star_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_star_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_star_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_star_5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pb_star_1;
        private System.Windows.Forms.PictureBox pb_star_2;
        private System.Windows.Forms.PictureBox pb_star_3;
        private System.Windows.Forms.PictureBox pb_star_4;
        private System.Windows.Forms.PictureBox pb_star_5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbTesto;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btSalva;
        private System.Windows.Forms.Button btAnnulla;
    }
}