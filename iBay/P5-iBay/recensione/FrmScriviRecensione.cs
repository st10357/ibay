﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySqlConnector;

namespace P5_iBay
{
    // Parte di Filonzi Jacopo
    public partial class FrmScriviRecensione : Form
    {
        #region Variabili
        int _IDProdottoDaRecensire;
        int _IDRecensioneDaModificare;
        int _nStelle = 0;

        MySqlConnection _connection = new MySqlConnection(Program.CONNECTION_STRING);
        #endregion

        #region Metodi
        /// <summary>
        /// Mostra le stelle della recensione graficamente sulle PictureBox
        /// </summary>
        /// <param name="nStelle">Numero di stelle della recensione</param>
        private void mostraStelle(int nStelle)
        {
            // Array di stelle
            PictureBox[] _stelle = { pb_star_1, pb_star_2, pb_star_3, pb_star_4, pb_star_5 };

            // Mostro le stelle vuote e non selezionate graficamente
            for (int i = 0; i < nStelle; i++)
            {
                _stelle[i].Image = P5_iBay.Properties.Resources.star;
            }
            for (int k = nStelle; k < 5; k++)
            {
                _stelle[k].Image = P5_iBay.Properties.Resources.emptyStar;
            }
        }
        #endregion

        #region Costruttore
        public FrmScriviRecensione(int IDProdottoDaRecensire, int IDRecensione)
        {
            InitializeComponent();

            // Memorizzo l'ID del prodotto da recensire
            _IDProdottoDaRecensire = IDProdottoDaRecensire;

            // Memorizzo l'ID della recensione da modificare (-1 se in inserimento)
            _IDRecensioneDaModificare = IDRecensione;

            // Ottieni la recensione dal DB e caricala sui vari controlli grafici (se in modifica)
            if(_IDRecensioneDaModificare != -1)
            {
                try
                {
                    _connection.Open();
                    MySqlCommand _query = new MySqlCommand($"SELECT * FROM recensioni WHERE ID = '{_IDRecensioneDaModificare}'", _connection);
                    MySqlDataReader _reader = _query.ExecuteReader();
                    _reader.Read();

                    mostraStelle((int)_reader["Stelle"]);
                    tbTesto.Text = (string)_reader["Testo"];

                    _connection.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
            }
        }
        #endregion

        #region Eventi
        private void pb_star_1_Click(object sender, EventArgs e)
        {
            mostraStelle(1);
            _nStelle = 1;
        }

        private void pb_star_2_Click(object sender, EventArgs e)
        {
            mostraStelle(2);
            _nStelle = 2;
        }

        private void pb_star_3_Click(object sender, EventArgs e)
        {
            mostraStelle(3);
            _nStelle = 3;
        }

        private void pb_star_4_Click(object sender, EventArgs e)
        {
            mostraStelle(4);
            _nStelle = 4;
        }

        private void pb_star_5_Click(object sender, EventArgs e)
        {
            mostraStelle(5);
            _nStelle = 5;
        }

        private void btSalva_Click(object sender, EventArgs e)
        {
            // Se è stato compilato tutto correttamente inserisci/modifica la recensione nel DB
            if (_nStelle > 0 && !String.IsNullOrEmpty(tbTesto.Text))
            {
                try
                {
                    _connection.Open();

                    // Se l'ID della recensione da modificare è diverso da -1 devo effettuare una modifica
                    if (_IDRecensioneDaModificare != -1)
                    {
                        MySqlCommand _query = new MySqlCommand($"UPDATE recensioni SET Stelle = {_nStelle}, Testo = '{tbTesto.Text}' WHERE ID = {_IDRecensioneDaModificare}", _connection);
                        _query.ExecuteNonQuery();
                    }
                    // Altrimenti un inserimento
                    else
                    {
                        MySqlCommand _query = new MySqlCommand($"INSERT INTO recensioni (IDProdotto, EmailUtente, Stelle, Testo) VALUES ({_IDProdottoDaRecensire}, '{Program._email}', {_nStelle}, '{tbTesto.Text}')", _connection);
                        _query.ExecuteNonQuery();
                    }

                    _connection.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }


                // Conferma di operazione avvenuta con successo
                MessageBox.Show("Salvataggio effettuato con successo.", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
                MessageBox.Show("Alcuni campi non sono stati compilati. Verificare che tutti i campi siano stati compilati prima di continuare.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btAnnulla_Click(object sender, EventArgs e)
        {
            // Richiesta di conferma per l'annullamento dell'inserimento
            DialogResult _dr = MessageBox.Show("Sei sicuro di voler annullare l'inserimento della recensione? I dati inseriti non verranno salvati.", "Avviso", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

            // Se si conferma l'annullamento chiudi questa form
            if (_dr == DialogResult.Yes)
            {
                this.DialogResult = DialogResult.Abort;
                this.Close();
            }
        }
        #endregion
    }
}