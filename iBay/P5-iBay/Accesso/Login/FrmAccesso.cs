﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySqlConnector;

namespace P5_iBay
{
    // Parte di Filonzi Jacopo
    public partial class FrmAccesso : Form
    {
        #region Costruttore
        public FrmAccesso()
        {
            InitializeComponent();
            Program.roudedEdges(btn_accedi);
        }
        #endregion

        #region Eventi
        private void pnlAccedi_Stile(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics, this.pnlAccedi.ClientRectangle, Color.DarkGray, ButtonBorderStyle.Solid);
        }

        private void btn_accedi_Click(object sender, EventArgs e)
        {
            // Cambia lo stato del cursore alla modalità caricamento
            this.Cursor = Cursors.WaitCursor;

            // Dichiarazione variabili
            string _email = tb_email.Text.Trim().ToLower();
            string _password = tb_password.Text.Trim();
            bool _success = false;
            var _connection = new MySqlConnection(Program.CONNECTION_STRING);

            // Verifica se username e password inseriti sono presenti nel DB ed effettua l'accesso in caso di successo
            try
            {
                _connection.Open();

                var _command = new MySqlCommand($"SELECT Email, Password, Venditore, Amministratore FROM utenti WHERE Email='{_email}' AND Password='{_password}'", _connection);
                var _reader = _command.ExecuteReader();

                while (_reader.Read())
                {
                    if (_email == _reader.GetString(0))
                    {
                        // Memorizzazione informazioni utente connesso
                        Program._email = _email;
                        Program._venditore = (bool)_reader["Venditore"];
                        Program._amministratore = (bool)_reader["Amministratore"];

                        // Conferma successo dell'operazione
                        _success = true;
                    }
                }

                // Se l'accesso è riuscito apri la homepage
                if (_success)
                {
                    frmHomePage frmHomePage = new frmHomePage();
                    frmHomePage.Owner = this;
                    DialogResult dr = frmHomePage.ShowDialog();

                    // Se ho chiuso l'app chiudi anche la FrmAccesso
                    if(dr == DialogResult.Cancel)
                        Close();
                }
                else
                    MessageBox.Show("Credenziali errate", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);

                _connection.Close();

                // Riporto il cursore a quello normale
                this.Cursor = Cursors.Arrow;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        private void llblRegistrati_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            FrmRegistrazione frmRegistrazione = new FrmRegistrazione();
            frmRegistrazione.ShowDialog();
        }
        #endregion
    }
}
