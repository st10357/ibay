﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySqlConnector;

namespace P5_iBay
{
    // Parte di Filonzi Jacopo
    public partial class FrmRegistrazione : Form
    {
        #region Variabili
        MySqlConnection _connection = new MySqlConnection(Program.CONNECTION_STRING);
        #endregion

        #region Costruttore
        public FrmRegistrazione()
        {
            InitializeComponent();
            Program.roudedEdges(btn_Registrati);
        }
        #endregion

        #region Eventi
        private void pnlRegistrati_Stile(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics, this.pnlRegistrati.ClientRectangle, Color.DarkGray, ButtonBorderStyle.Solid);
        }

        private void btn_Registrati_Click(object sender, EventArgs e)
        {
            // Imposto il cursore di attesa
            this.Cursor = Cursors.WaitCursor;

            // Dichiarazione variabili
            string _nome = tb_nome.Text.Trim();
            string _cognome = tb_cognome.Text.Trim();
            string _email = tb_email.Text.Trim().ToLower();
            string _telefono = tb_telefono.Text.Replace(" ", "");
            string _password = tb_password.Text.Trim();
            string _passwordCheck = tb_passwordConferma.Text.Trim();
            string _codiceFiscale = tb_codiceFiscale.Text.Trim();
            char _genere = ' ';
            if (cbGenere.SelectedIndex != -1)
                _genere = cbGenere.SelectedItem.ToString()[0];
            DateTime _dataDiNascita = dtp_dataNascita.Value;

            // Controllo se i campi sono stati compilati correttamente
            if (_password != _passwordCheck)
            {
                MessageBox.Show("Le password non coincidono", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Cursor = Cursors.Arrow;
                return;
            }

            if (_nome == "" || _cognome == "" || _codiceFiscale.Length < 16 || _telefono.Length < 10 || _password == "" || _passwordCheck == "" || _genere == ' ')
            {
                MessageBox.Show("Non sono stati compilati tutti i campi. Compilare e riprovare.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Cursor = Cursors.Arrow;
                return;
            }

            if (_email.IndexOf('@') == -1 || _email.Split('@')[1].IndexOf('.') == -1 || _email.Split('@')[1].Split('.').Count() != 2)
            {
                MessageBox.Show("Formato email non corretto. Reinserirla e riprovare.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Cursor = Cursors.Arrow;
                return;
            }

            // Controllo se l'e-mail è già stata registrata precedentemente
            if (IsEmailRegistered(_email))
            {
                MessageBox.Show("L'e-mail risulta già associata. Utilizzarne un'altra o accedere all'account.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Cursor = Cursors.Arrow;
                return;
            }

            // In caso di successo inserisco l'utente nel DB
            try
            {
                _connection.Open();

                var command = new MySqlCommand($"INSERT INTO utenti (CF, Nome, Cognome, Genere, DataNascita, Email, NumeroCellulare, Password) VALUES ('{tb_codiceFiscale.Text}', '{_nome}', '{_cognome}', '{_genere}', '{GetStringData(_dataDiNascita)}', '{_email}', '{_telefono}', '{_password}')", _connection);

                var reader = command.ExecuteReader();
                _connection.Close();

                MessageBox.Show("Utente registrato con successo", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Cursor = Cursors.Arrow;
                this.Close();
            }
        }

        private void llblAccedi_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // Chiedi conferma prima di uscire
            DialogResult dr = MessageBox.Show("Sei sicuro di voler annullare la registrazione?", "Conferma annullamento", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dr == DialogResult.Yes)
                this.Close();
        }

        private void FrmRegistrazione_Load(object sender, EventArgs e)
        {
            // Impostazione data minima e massima selezionabile dal DateTimePicker
            dtp_dataNascita.MinDate = new DateTime(DateTime.Now.Year - 150, 1, 1);
            dtp_dataNascita.MaxDate = new DateTime(DateTime.Now.Year - 18, DateTime.Now.Month, DateTime.Now.Day);
        }
        #endregion

        #region Metodi
        private string span(int number)
        {
            if (number > 9)
                return number.ToString();
            else
                return "0" + number.ToString();
        }

        private string GetStringData(DateTime data)
        {
            return $"{span(data.Year)}-{span(data.Month)}-{span(data.Day)} {span(data.Hour)}:{span(data.Minute)}:{span(data.Second)}";
        }

        /// <summary>
        /// Controlla dalla tabella degli utenti nel DB se l'e-mail è già registrata
        /// </summary>
        /// <param name="email">E-mail da controllare</param>
        /// <returns></returns>
        private bool IsEmailRegistered(string email)
        {
            try
            {
                _connection.Open();

                MySqlCommand _query = new MySqlCommand($"SELECT Email from utenti WHERE Email = '{email}'", _connection);
                MySqlDataReader _reader = _query.ExecuteReader();
                _reader.Read();

                if (_reader.HasRows)
                {
                    _connection.Close();
                    return true;
                }
                else
                {
                    _connection.Close();
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _connection.Close();
                return true;
            }
        }
        #endregion
    }
}