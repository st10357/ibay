﻿namespace P5_iBay
{
    partial class FrmRegistrazione
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.dtp_dataNascita = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.tb_codiceFiscale = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tb_passwordConferma = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_cognome = new System.Windows.Forms.TextBox();
            this.tb_nome = new System.Windows.Forms.TextBox();
            this.llblAccedi = new System.Windows.Forms.LinkLabel();
            this.btn_Registrati = new System.Windows.Forms.Button();
            this.tb_email = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_password = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlRegistrati = new System.Windows.Forms.Panel();
            this.tb_telefono = new System.Windows.Forms.MaskedTextBox();
            this.cbGenere = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlRegistrati.SuspendLayout();
            this.SuspendLayout();
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label10.Location = new System.Drawing.Point(43, 363);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(143, 16);
            this.label10.TabIndex = 20;
            this.label10.Text = "Possiedi già un account?";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label9.Location = new System.Drawing.Point(18, 172);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 16);
            this.label9.TabIndex = 19;
            this.label9.Text = "Telefono*";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label8.Location = new System.Drawing.Point(124, 92);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 16);
            this.label8.TabIndex = 17;
            this.label8.Text = "Data di nascita*";
            // 
            // dtp_dataNascita
            // 
            this.dtp_dataNascita.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.dtp_dataNascita.Location = new System.Drawing.Point(127, 108);
            this.dtp_dataNascita.Name = "dtp_dataNascita";
            this.dtp_dataNascita.Size = new System.Drawing.Size(107, 21);
            this.dtp_dataNascita.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label7.Location = new System.Drawing.Point(18, 92);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 16);
            this.label7.TabIndex = 15;
            this.label7.Text = "Codice Fiscale*";
            // 
            // tb_codiceFiscale
            // 
            this.tb_codiceFiscale.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_codiceFiscale.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tb_codiceFiscale.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.tb_codiceFiscale.Location = new System.Drawing.Point(21, 108);
            this.tb_codiceFiscale.MaxLength = 16;
            this.tb_codiceFiscale.Name = "tb_codiceFiscale";
            this.tb_codiceFiscale.Size = new System.Drawing.Size(103, 21);
            this.tb_codiceFiscale.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label6.Location = new System.Drawing.Point(22, 289);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(123, 16);
            this.label6.TabIndex = 13;
            this.label6.Text = "Conferma password*";
            // 
            // tb_passwordConferma
            // 
            this.tb_passwordConferma.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_passwordConferma.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.tb_passwordConferma.Location = new System.Drawing.Point(21, 305);
            this.tb_passwordConferma.MaxLength = 50;
            this.tb_passwordConferma.Name = "tb_passwordConferma";
            this.tb_passwordConferma.PasswordChar = '•';
            this.tb_passwordConferma.Size = new System.Drawing.Size(213, 21);
            this.tb_passwordConferma.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label5.Location = new System.Drawing.Point(126, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 16);
            this.label5.TabIndex = 11;
            this.label5.Text = "Cognome*";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label4.Location = new System.Drawing.Point(20, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 16);
            this.label4.TabIndex = 10;
            this.label4.Text = "Nome*";
            // 
            // tb_cognome
            // 
            this.tb_cognome.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_cognome.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.tb_cognome.Location = new System.Drawing.Point(129, 69);
            this.tb_cognome.MaxLength = 50;
            this.tb_cognome.Name = "tb_cognome";
            this.tb_cognome.Size = new System.Drawing.Size(105, 21);
            this.tb_cognome.TabIndex = 2;
            // 
            // tb_nome
            // 
            this.tb_nome.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_nome.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.tb_nome.Location = new System.Drawing.Point(21, 69);
            this.tb_nome.MaxLength = 50;
            this.tb_nome.Name = "tb_nome";
            this.tb_nome.Size = new System.Drawing.Size(105, 21);
            this.tb_nome.TabIndex = 1;
            // 
            // llblAccedi
            // 
            this.llblAccedi.AutoSize = true;
            this.llblAccedi.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.llblAccedi.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(61)))), ((int)(((byte)(124)))));
            this.llblAccedi.Location = new System.Drawing.Point(192, 363);
            this.llblAccedi.Name = "llblAccedi";
            this.llblAccedi.Size = new System.Drawing.Size(46, 16);
            this.llblAccedi.TabIndex = 11;
            this.llblAccedi.TabStop = true;
            this.llblAccedi.Text = "Accedi";
            this.llblAccedi.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(31)))), ((int)(((byte)(66)))));
            this.llblAccedi.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llblAccedi_LinkClicked);
            // 
            // btn_Registrati
            // 
            this.btn_Registrati.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Registrati.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(61)))), ((int)(((byte)(124)))));
            this.btn_Registrati.FlatAppearance.BorderSize = 0;
            this.btn_Registrati.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Registrati.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.btn_Registrati.ForeColor = System.Drawing.Color.White;
            this.btn_Registrati.Location = new System.Drawing.Point(21, 331);
            this.btn_Registrati.Name = "btn_Registrati";
            this.btn_Registrati.Size = new System.Drawing.Size(213, 29);
            this.btn_Registrati.TabIndex = 10;
            this.btn_Registrati.Text = "Registrati";
            this.btn_Registrati.UseVisualStyleBackColor = false;
            this.btn_Registrati.Click += new System.EventHandler(this.btn_Registrati_Click);
            // 
            // tb_email
            // 
            this.tb_email.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_email.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.tb_email.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.tb_email.Location = new System.Drawing.Point(21, 227);
            this.tb_email.MaxLength = 50;
            this.tb_email.Name = "tb_email";
            this.tb_email.Size = new System.Drawing.Size(213, 21);
            this.tb_email.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label3.Location = new System.Drawing.Point(20, 250);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Password*";
            // 
            // tb_password
            // 
            this.tb_password.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_password.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.tb_password.Location = new System.Drawing.Point(21, 266);
            this.tb_password.MaxLength = 50;
            this.tb_password.Name = "tb_password";
            this.tb_password.PasswordChar = '•';
            this.tb_password.Size = new System.Drawing.Size(213, 21);
            this.tb_password.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label2.Location = new System.Drawing.Point(18, 211);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Email*";
            // 
            // pnlRegistrati
            // 
            this.pnlRegistrati.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlRegistrati.Controls.Add(this.tb_telefono);
            this.pnlRegistrati.Controls.Add(this.cbGenere);
            this.pnlRegistrati.Controls.Add(this.label11);
            this.pnlRegistrati.Controls.Add(this.label10);
            this.pnlRegistrati.Controls.Add(this.label9);
            this.pnlRegistrati.Controls.Add(this.label8);
            this.pnlRegistrati.Controls.Add(this.dtp_dataNascita);
            this.pnlRegistrati.Controls.Add(this.label7);
            this.pnlRegistrati.Controls.Add(this.tb_codiceFiscale);
            this.pnlRegistrati.Controls.Add(this.label6);
            this.pnlRegistrati.Controls.Add(this.tb_passwordConferma);
            this.pnlRegistrati.Controls.Add(this.label5);
            this.pnlRegistrati.Controls.Add(this.label4);
            this.pnlRegistrati.Controls.Add(this.tb_cognome);
            this.pnlRegistrati.Controls.Add(this.tb_nome);
            this.pnlRegistrati.Controls.Add(this.llblAccedi);
            this.pnlRegistrati.Controls.Add(this.label1);
            this.pnlRegistrati.Controls.Add(this.btn_Registrati);
            this.pnlRegistrati.Controls.Add(this.tb_email);
            this.pnlRegistrati.Controls.Add(this.label3);
            this.pnlRegistrati.Controls.Add(this.tb_password);
            this.pnlRegistrati.Controls.Add(this.label2);
            this.pnlRegistrati.Location = new System.Drawing.Point(275, 30);
            this.pnlRegistrati.MaximumSize = new System.Drawing.Size(250, 352);
            this.pnlRegistrati.MinimumSize = new System.Drawing.Size(250, 390);
            this.pnlRegistrati.Name = "pnlRegistrati";
            this.pnlRegistrati.Size = new System.Drawing.Size(250, 390);
            this.pnlRegistrati.TabIndex = 7;
            this.pnlRegistrati.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlRegistrati_Stile);
            // 
            // tb_telefono
            // 
            this.tb_telefono.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.tb_telefono.Location = new System.Drawing.Point(21, 188);
            this.tb_telefono.Mask = "+00 000 000 0000";
            this.tb_telefono.Name = "tb_telefono";
            this.tb_telefono.Size = new System.Drawing.Size(213, 21);
            this.tb_telefono.TabIndex = 6;
            this.tb_telefono.Text = "39";
            // 
            // cbGenere
            // 
            this.cbGenere.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.cbGenere.FormattingEnabled = true;
            this.cbGenere.Items.AddRange(new object[] {
            "Maschio",
            "Femmina",
            "Altro"});
            this.cbGenere.Location = new System.Drawing.Point(21, 148);
            this.cbGenere.Name = "cbGenere";
            this.cbGenere.Size = new System.Drawing.Size(213, 24);
            this.cbGenere.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label11.Location = new System.Drawing.Point(18, 132);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 16);
            this.label11.TabIndex = 21;
            this.label11.Text = "Genere*";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 20.25F);
            this.label1.Location = new System.Drawing.Point(15, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 33);
            this.label1.TabIndex = 0;
            this.label1.Text = "Registrati";
            // 
            // FrmRegistrazione
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::P5_iBay.Properties.Resources.SfondoAccesso;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pnlRegistrati);
            this.Name = "FrmRegistrazione";
            this.Text = "FrmRegistrazione";
            this.Load += new System.EventHandler(this.FrmRegistrazione_Load);
            this.pnlRegistrati.ResumeLayout(false);
            this.pnlRegistrati.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dtp_dataNascita;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tb_codiceFiscale;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tb_passwordConferma;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_cognome;
        private System.Windows.Forms.TextBox tb_nome;
        private System.Windows.Forms.LinkLabel llblAccedi;
        private System.Windows.Forms.Button btn_Registrati;
        private System.Windows.Forms.TextBox tb_email;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_password;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnlRegistrati;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbGenere;
        private System.Windows.Forms.MaskedTextBox tb_telefono;
    }
}