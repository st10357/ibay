﻿namespace P5_iBay
{
    partial class FrmUtente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbNome = new System.Windows.Forms.TextBox();
            this.tbCognome = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpdataNascita = new System.Windows.Forms.DateTimePicker();
            this.cbGenere = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbEmail = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbTelefono = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btAreaVenditore = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btSalva = new System.Windows.Forms.Button();
            this.btAreaAmministratore = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.tbVecchiaPassword = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tbNuovaPassword = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tbConfermaNuovaPassword = new System.Windows.Forms.TextBox();
            this.btCambiaPassword = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbNome
            // 
            this.tbNome.Location = new System.Drawing.Point(27, 107);
            this.tbNome.Name = "tbNome";
            this.tbNome.Size = new System.Drawing.Size(100, 20);
            this.tbNome.TabIndex = 0;
            // 
            // tbCognome
            // 
            this.tbCognome.Location = new System.Drawing.Point(133, 107);
            this.tbCognome.Name = "tbCognome";
            this.tbCognome.Size = new System.Drawing.Size(100, 20);
            this.tbCognome.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nome";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(130, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Cognome";
            // 
            // dtpdataNascita
            // 
            this.dtpdataNascita.Enabled = false;
            this.dtpdataNascita.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpdataNascita.Location = new System.Drawing.Point(239, 107);
            this.dtpdataNascita.Name = "dtpdataNascita";
            this.dtpdataNascita.Size = new System.Drawing.Size(100, 20);
            this.dtpdataNascita.TabIndex = 4;
            // 
            // cbGenere
            // 
            this.cbGenere.FormattingEnabled = true;
            this.cbGenere.Items.AddRange(new object[] {
            "Maschio",
            "Femmina",
            "Altro"});
            this.cbGenere.Location = new System.Drawing.Point(345, 107);
            this.cbGenere.Name = "cbGenere";
            this.cbGenere.Size = new System.Drawing.Size(100, 21);
            this.cbGenere.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(236, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Data di nascita";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(342, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Genere";
            // 
            // tbEmail
            // 
            this.tbEmail.Enabled = false;
            this.tbEmail.Location = new System.Drawing.Point(27, 147);
            this.tbEmail.Name = "tbEmail";
            this.tbEmail.Size = new System.Drawing.Size(206, 20);
            this.tbEmail.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 131);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Email";
            // 
            // tbTelefono
            // 
            this.tbTelefono.Location = new System.Drawing.Point(239, 147);
            this.tbTelefono.Name = "tbTelefono";
            this.tbTelefono.Size = new System.Drawing.Size(206, 20);
            this.tbTelefono.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(236, 131);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Telefono";
            // 
            // btAreaVenditore
            // 
            this.btAreaVenditore.Location = new System.Drawing.Point(30, 385);
            this.btAreaVenditore.Name = "btAreaVenditore";
            this.btAreaVenditore.Size = new System.Drawing.Size(206, 23);
            this.btAreaVenditore.TabIndex = 13;
            this.btAreaVenditore.Text = "Area venditore";
            this.btAreaVenditore.UseVisualStyleBackColor = true;
            this.btAreaVenditore.Visible = false;
            this.btAreaVenditore.Click += new System.EventHandler(this.btAreaVenditore_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(75, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(318, 37);
            this.label7.TabIndex = 14;
            this.label7.Text = "Impostazioni Utente";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(27, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Informazioni:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(27, 369);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(107, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Collegamenti utili:";
            // 
            // btSalva
            // 
            this.btSalva.Location = new System.Drawing.Point(27, 173);
            this.btSalva.Name = "btSalva";
            this.btSalva.Size = new System.Drawing.Size(418, 23);
            this.btSalva.TabIndex = 17;
            this.btSalva.Text = "Salva";
            this.btSalva.UseVisualStyleBackColor = true;
            this.btSalva.Click += new System.EventHandler(this.btSalva_Click);
            // 
            // btAreaAmministratore
            // 
            this.btAreaAmministratore.Location = new System.Drawing.Point(242, 385);
            this.btAreaAmministratore.Name = "btAreaAmministratore";
            this.btAreaAmministratore.Size = new System.Drawing.Size(206, 23);
            this.btAreaAmministratore.TabIndex = 18;
            this.btAreaAmministratore.Text = "Area amministratore";
            this.btAreaAmministratore.UseVisualStyleBackColor = true;
            this.btAreaAmministratore.Visible = false;
            this.btAreaAmministratore.Click += new System.EventHandler(this.btAreaAmministratore_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(27, 199);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(109, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Cambia password:";
            // 
            // tbVecchiaPassword
            // 
            this.tbVecchiaPassword.Location = new System.Drawing.Point(27, 239);
            this.tbVecchiaPassword.Name = "tbVecchiaPassword";
            this.tbVecchiaPassword.PasswordChar = '•';
            this.tbVecchiaPassword.Size = new System.Drawing.Size(418, 20);
            this.tbVecchiaPassword.TabIndex = 20;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(27, 223);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(97, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "Vecchia password:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(27, 262);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "Nuova password:";
            // 
            // tbNuovaPassword
            // 
            this.tbNuovaPassword.Location = new System.Drawing.Point(27, 278);
            this.tbNuovaPassword.Name = "tbNuovaPassword";
            this.tbNuovaPassword.PasswordChar = '•';
            this.tbNuovaPassword.Size = new System.Drawing.Size(418, 20);
            this.tbNuovaPassword.TabIndex = 22;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(27, 301);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(136, 13);
            this.label13.TabIndex = 25;
            this.label13.Text = "Conferma nuova password:";
            // 
            // tbConfermaNuovaPassword
            // 
            this.tbConfermaNuovaPassword.Location = new System.Drawing.Point(27, 317);
            this.tbConfermaNuovaPassword.Name = "tbConfermaNuovaPassword";
            this.tbConfermaNuovaPassword.PasswordChar = '•';
            this.tbConfermaNuovaPassword.Size = new System.Drawing.Size(418, 20);
            this.tbConfermaNuovaPassword.TabIndex = 24;
            // 
            // btCambiaPassword
            // 
            this.btCambiaPassword.Location = new System.Drawing.Point(27, 343);
            this.btCambiaPassword.Name = "btCambiaPassword";
            this.btCambiaPassword.Size = new System.Drawing.Size(418, 23);
            this.btCambiaPassword.TabIndex = 26;
            this.btCambiaPassword.Text = "Cambia password";
            this.btCambiaPassword.UseVisualStyleBackColor = true;
            this.btCambiaPassword.Click += new System.EventHandler(this.btCambiaPassword_Click);
            // 
            // FrmUtente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 430);
            this.Controls.Add(this.btCambiaPassword);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.tbConfermaNuovaPassword);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.tbNuovaPassword);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.tbVecchiaPassword);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btAreaAmministratore);
            this.Controls.Add(this.btSalva);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btAreaVenditore);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbTelefono);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbEmail);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbGenere);
            this.Controls.Add(this.dtpdataNascita);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbCognome);
            this.Controls.Add(this.tbNome);
            this.Name = "FrmUtente";
            this.Text = "Impostazioni Utente";
            this.Load += new System.EventHandler(this.FrmUtente_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbNome;
        private System.Windows.Forms.TextBox tbCognome;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpdataNascita;
        private System.Windows.Forms.ComboBox cbGenere;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbEmail;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbTelefono;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btAreaVenditore;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btSalva;
        private System.Windows.Forms.Button btAreaAmministratore;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbVecchiaPassword;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbNuovaPassword;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tbConfermaNuovaPassword;
        private System.Windows.Forms.Button btCambiaPassword;
    }
}