﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySqlConnector;

namespace P5_iBay
{
    // Parte di Sebastianelli Tomas
    public partial class FrmUtente : Form
    {
        #region Variabili
        MySqlConnection _connection = new MySqlConnection(Program.CONNECTION_STRING);
        #endregion

        #region Costruttore
        public FrmUtente()
        {
            InitializeComponent();
        }
        #endregion

        #region Eventi
        private void FrmUtente_Load(object sender, EventArgs e)
        {
            // Ottengo le informazioni dell'utente connesso dal DB e le carico nei vari controlli grafici
            try
            {
                _connection.Open();

                MySqlCommand _query = new MySqlCommand($"SELECT Nome, Cognome, DataNascita, Genere, Email, NumeroCellulare, Venditore, Amministratore FROM utenti WHERE Email = '{Program._email}'", _connection);
                MySqlDataReader _reader = _query.ExecuteReader();
                _reader.Read();

                tbNome.Text = (string)_reader["Nome"];
                tbCognome.Text = (string)_reader["Cognome"];
                dtpdataNascita.Value = Convert.ToDateTime(_reader["DataNascita"]);
                switch (Convert.ToString(_reader["Genere"])[0])
                {
                    case 'M':
                        cbGenere.SelectedIndex = 0;
                        break;

                    case 'F':
                        cbGenere.SelectedIndex = 1;
                        break;

                    case 'A':
                        cbGenere.SelectedIndex = 2;
                        break;
                }
                tbEmail.Text = Program._email;
                tbTelefono.Text = (string)_reader["NumeroCellulare"];

                // Stabilisco quali collegamenti rendere visibili
                if ((bool)_reader["Venditore"])
                    btAreaVenditore.Visible = true;
                if ((bool)_reader["Amministratore"])
                {
                    btAreaVenditore.Visible = true;
                    btAreaAmministratore.Visible = true;
                }  

                _connection.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btSalva_Click(object sender, EventArgs e)
        {
            // Dichiarazione variabili
            string _nome = tbNome.Text.Trim();
            string _cognome = tbCognome.Text.Trim();
            int _genere = cbGenere.SelectedIndex;
            string _telefono = tbTelefono.Text.Trim();

            // Controllo se i campi sono stati compilati correttamente
            if (_nome == "" || _cognome == "" || _telefono == "")
            {
                MessageBox.Show("Non sono stati compilati tutti i campi. Compilare e riprovare.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // In caso di successo inserisco l'utente nel DB
            try
            {
                _connection.Open();

                var command = new MySqlCommand($"UPDATE utenti SET Nome = '{_nome}', Cognome = '{_cognome}', Genere = '{cbGenere.SelectedItem.ToString()[0]}', NumeroCellulare = '{_telefono}' WHERE Email = '{Program._email}'", _connection);
                command.ExecuteReader();

                _connection.Close();

                MessageBox.Show("Informazioni aggiornate con successo", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        private void btCambiaPassword_Click(object sender, EventArgs e)
        {
            // Dichiarazione variabili
            string _vecchiaPassword = "";

            // Ottenimento vecchia password
            try
            {
                _connection.Open();

                MySqlCommand _query = new MySqlCommand($"SELECT Password FROM utenti WHERE Email = '{Program._email}'", _connection);
                MySqlDataReader _reader = _query.ExecuteReader();
                _reader.Read();

                _vecchiaPassword = (string)_reader["Password"];

                _connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            // Effettuo il cambio password solo se quella vecchia è corretta e se quelle nuove coincidono
            if (_vecchiaPassword == tbVecchiaPassword.Text && tbNuovaPassword.Text == tbConfermaNuovaPassword.Text)
            {
                _connection.Open();

                MySqlCommand _query = new MySqlCommand($"UPDATE utenti SET Password = '{tbNuovaPassword.Text}' WHERE Email = '{Program._email}'", _connection);
                _query.ExecuteReader();

                _connection.Close();

                MessageBox.Show("Password aggiornata con successo", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                MessageBox.Show("La vecchia password inserita non è valida, riprovare.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btAreaAmministratore_Click(object sender, EventArgs e)
        {
            // Apro la form amministratore
            FrmAmministratore frmAmministratore = new FrmAmministratore();
            frmAmministratore.ShowDialog();
        }

        private void btAreaVenditore_Click(object sender, EventArgs e)
        {
            // Apro la form venditore
            FrmVenditore frmVenditore = new FrmVenditore();
            frmVenditore.Show();
        }
        #endregion
    }
}
