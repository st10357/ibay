﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace P5_iBay
{
    static class Program
    {
        #region Costanti
        public const string CONNECTION_STRING = "Server=localhost;Database=ibay;Uid=root;Pwd=root;";
        #endregion

        #region Enumeratori
        public enum eCategoriaProdotto
        {
            Bellezza,
            Salute,
            Alimentari,
            Prima_infanzia,
            Animali,
            Casa,
            Sport,
            Tempo_libero,
            Industria,
            Scienza,
            Moda,
            Giardino,
            Elettronica,
            Fai_da_te,
            Videogiochi,
            Auto_moto,
            Cancelleria,
            Informatica,
            Giochi,
            CD_vinili,
            Elettrodomestici,
            Altro
        }

        public enum eStatoOrdine
        {
            Elaborato,
            Approvato,
            Spedito,
            Consegnato,
            Reso
        }
        #endregion

        #region Struct
        public struct rigaSelezionata
        {
            public static string Indirizzo, Città, Nome, CAP, Provincia, Nazione, IstruzioniConsegna;
            public static int TipoProprieta;
        }

        public struct sProdottiCarrello
        {
            public string emailUtente;
            public int idProdotto, quantitaSelezionata;
        }
        #endregion

        #region Variabili
        // Informazioni utente connesso
        public static string _email;
        public static bool _venditore;
        public static bool _amministratore;

        // Variabili per il metodo di pagamento
        public static string codiceCartaSelezionata = null;
        public static string ibanContoSelezionato = null;

        // Variabili per l'indirizzo di spedizione
        public static int idIndirizzoSelezionato = -1;

        // Variabili per il carrello
        public static string pathCarrello = "prodotti.txt";
        public static bool riepilogoCarrello = false;
        public static int idProdottoSelezionato = -1;
        public static string nomeBtPremuto;
        public static bool frmCarrelloAperta = false;
        public static sProdottiCarrello ProdottoCarrello;
        public static List<sProdottiCarrello> ProdottiCarrello = new List<sProdottiCarrello>(0);
        #endregion

        #region Costruttore
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FrmAccesso());
        }
        #endregion

        #region Metodi
        /// <summary>
        /// Metodo per rendere i bordi dei bottoni smussati
        /// Cambiare le seguenti proprietà dei bottoni: FlatAppearance - BorderSize = 0; FlatStyle = Flat
        /// </summary>
        /// <param name="bt">passare il bottone</param>
        public static void roudedEdges(Button bt)
        {
            GraphicsPath newgraph = new GraphicsPath();
            int radius = 10; // Raggio dei bordi arrotondati, puoi modificarlo

            newgraph.AddArc(new Rectangle(0, 0, radius * 2, radius * 2), 180, 90);
            newgraph.AddLine(radius, 0, bt.Width - radius * 2, 0);
            newgraph.AddArc(new Rectangle(bt.Width - radius * 2, 0, radius * 2, radius * 2), -90, 90);
            newgraph.AddLine(bt.Width, radius, bt.Width, bt.Height - radius * 2);
            newgraph.AddArc(new Rectangle(bt.Width - radius * 2, bt.Height - radius * 2, radius * 2, radius * 2), 0, 90);
            newgraph.AddLine(bt.Width - radius * 2, bt.Height, radius * 2, bt.Height);
            newgraph.AddArc(new Rectangle(0, bt.Height - radius * 2, radius * 2, radius * 2), 90, 90);
            newgraph.CloseFigure();

            bt.Region = new Region(newgraph);

            // Abilita l'anti-aliasing per ottenere bordi più lisci
            bt.Paint += (sender, e) =>
            {
                e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            };

            //modifico alcune proprietà del bottone
            bt.FlatAppearance.BorderSize = 0;
            bt.FlatStyle = FlatStyle.Flat;
        }
        #endregion
    }
}
