﻿namespace P5_iBay
{
    partial class UCsingoloOrdine
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tb_nomeProdotto = new System.Windows.Forms.TextBox();
            this.tb_descrizione = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tb_indirizzoDiSpedizione = new System.Windows.Forms.TextBox();
            this.tb_statoOrdine = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = global::P5_iBay.Properties.Resources.product_icon;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // tb_nomeProdotto
            // 
            this.tb_nomeProdotto.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_nomeProdotto.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_nomeProdotto.Location = new System.Drawing.Point(100, 0);
            this.tb_nomeProdotto.Name = "tb_nomeProdotto";
            this.tb_nomeProdotto.ReadOnly = true;
            this.tb_nomeProdotto.Size = new System.Drawing.Size(400, 29);
            this.tb_nomeProdotto.TabIndex = 1;
            this.tb_nomeProdotto.Text = "Nome prodotto";
            // 
            // tb_descrizione
            // 
            this.tb_descrizione.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_descrizione.Location = new System.Drawing.Point(100, 29);
            this.tb_descrizione.Multiline = true;
            this.tb_descrizione.Name = "tb_descrizione";
            this.tb_descrizione.ReadOnly = true;
            this.tb_descrizione.Size = new System.Drawing.Size(400, 71);
            this.tb_descrizione.TabIndex = 2;
            this.tb_descrizione.Text = "Descrizione prodotto...";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tb_indirizzoDiSpedizione);
            this.panel1.Controls.Add(this.tb_statoOrdine);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(100, 78);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(400, 22);
            this.panel1.TabIndex = 3;
            // 
            // tb_indirizzoDiSpedizione
            // 
            this.tb_indirizzoDiSpedizione.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_indirizzoDiSpedizione.Location = new System.Drawing.Point(0, 0);
            this.tb_indirizzoDiSpedizione.Name = "tb_indirizzoDiSpedizione";
            this.tb_indirizzoDiSpedizione.ReadOnly = true;
            this.tb_indirizzoDiSpedizione.Size = new System.Drawing.Size(300, 20);
            this.tb_indirizzoDiSpedizione.TabIndex = 0;
            this.tb_indirizzoDiSpedizione.Text = "Consegna prevista GG/MM/AAAA - HH:MM";
            this.tb_indirizzoDiSpedizione.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tb_statoOrdine
            // 
            this.tb_statoOrdine.Dock = System.Windows.Forms.DockStyle.Right;
            this.tb_statoOrdine.Location = new System.Drawing.Point(300, 0);
            this.tb_statoOrdine.Name = "tb_statoOrdine";
            this.tb_statoOrdine.ReadOnly = true;
            this.tb_statoOrdine.Size = new System.Drawing.Size(100, 20);
            this.tb_statoOrdine.TabIndex = 1;
            this.tb_statoOrdine.Text = "Stato dell\'ordine";
            this.tb_statoOrdine.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // UCsingoloOrdine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tb_descrizione);
            this.Controls.Add(this.tb_nomeProdotto);
            this.Controls.Add(this.pictureBox1);
            this.Name = "UCsingoloOrdine";
            this.Size = new System.Drawing.Size(500, 100);
            this.Click += new System.EventHandler(this.UCsingoloOrdine_Click);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox tb_nomeProdotto;
        private System.Windows.Forms.TextBox tb_descrizione;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox tb_indirizzoDiSpedizione;
        private System.Windows.Forms.TextBox tb_statoOrdine;
    }
}
