﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySqlConnector;

namespace P5_iBay
{
    public partial class UCsingoloOrdine : UserControl
    {
        int idOrdine;
        public UCsingoloOrdine(int IDordine)
        {
            InitializeComponent();
            idOrdine = IDordine;
            popolaCampi(IDordine);
        }

        private void popolaCampi(int IDordine)
        {
            bool founded = false;
            int idProdotto = -1;
            int indirizzoSpedizioneID = -1;


            var connection = new MySqlConnection("Server=127.0.0.1;User ID=root;Password=root;Database=ibay");
            connection.Open();

            #region Fetch ordine
            var command = new MySqlCommand($"SELECT Stato, idProdotto, indirizzoSpedizioneID FROM ordini WHERE IDOrdine='{IDordine}'", connection);

            var reader = command.ExecuteReader();

            while (reader.Read())
            {
                founded = true;

                tb_statoOrdine.Text = Enum.GetName(typeof(Program.eStatoOrdine), Convert.ToInt16(reader["Stato"])).Replace('_', ' ');
                idProdotto = Convert.ToInt16(reader["idProdotto"]);
                indirizzoSpedizioneID = Convert.ToInt16(reader["indirizzoSpedizioneID"]);
            }
            connection.Close();


            if (!founded)
                throw new Exception($"Ordine [{IDordine}] non trovato");
            else
                founded = false;

            #endregion

            #region Fetch prodotto
            connection.Open();
            command = new MySqlCommand($"SELECT Nome, Descrizione FROM prodotti WHERE ID='{idProdotto}'", connection);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                tb_nomeProdotto.Text = reader["Nome"] as string;
                tb_descrizione.Text = reader["Descrizione"] as string;
            }
            connection.Close();

            #endregion

            #region fetch IndirizzoSpedizione
            connection.Open();
            command = new MySqlCommand($"SELECT Indirizzo, Città, Provincia FROM indirizzispedizione WHERE ID='{indirizzoSpedizioneID}'", connection);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                tb_indirizzoDiSpedizione.Text = $"{reader["Indirizzo"]}, {reader["Città"]}, {reader["Provincia"]}";
            }
            connection.Close();
            #endregion
        }

        private void UCsingoloOrdine_Click(object sender, EventArgs e)
        {
            //new FrmOrdine(idOrdine).Show();
            //apri dettaglio
        }
    }
}
