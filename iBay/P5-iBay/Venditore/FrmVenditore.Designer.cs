﻿namespace P5_iBay
{
    partial class FrmVenditore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lv_prodotti = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bt_aggiungi = new System.Windows.Forms.Button();
            this.bt_rimuovi = new System.Windows.Forms.Button();
            this.bt_modifica = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lv_prodotti
            // 
            this.lv_prodotti.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.lv_prodotti.FullRowSelect = true;
            this.lv_prodotti.HideSelection = false;
            this.lv_prodotti.Location = new System.Drawing.Point(12, 12);
            this.lv_prodotti.Name = "lv_prodotti";
            this.lv_prodotti.Size = new System.Drawing.Size(582, 368);
            this.lv_prodotti.TabIndex = 0;
            this.lv_prodotti.UseCompatibleStateImageBehavior = false;
            this.lv_prodotti.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "ID";
            this.columnHeader1.Width = 45;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Nome";
            this.columnHeader2.Width = 115;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Descrizione";
            this.columnHeader3.Width = 198;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Categoria";
            this.columnHeader4.Width = 148;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Quantità";
            this.columnHeader5.Width = 71;
            // 
            // bt_aggiungi
            // 
            this.bt_aggiungi.Location = new System.Drawing.Point(600, 12);
            this.bt_aggiungi.Name = "bt_aggiungi";
            this.bt_aggiungi.Size = new System.Drawing.Size(140, 23);
            this.bt_aggiungi.TabIndex = 1;
            this.bt_aggiungi.Text = "Aggiungi";
            this.bt_aggiungi.UseVisualStyleBackColor = true;
            this.bt_aggiungi.Click += new System.EventHandler(this.bt_aggiungi_Click);
            // 
            // bt_rimuovi
            // 
            this.bt_rimuovi.Location = new System.Drawing.Point(600, 41);
            this.bt_rimuovi.Name = "bt_rimuovi";
            this.bt_rimuovi.Size = new System.Drawing.Size(140, 23);
            this.bt_rimuovi.TabIndex = 2;
            this.bt_rimuovi.Text = "Rimuovi";
            this.bt_rimuovi.UseVisualStyleBackColor = true;
            this.bt_rimuovi.Click += new System.EventHandler(this.bt_rimuovi_Click);
            // 
            // bt_modifica
            // 
            this.bt_modifica.Location = new System.Drawing.Point(600, 70);
            this.bt_modifica.Name = "bt_modifica";
            this.bt_modifica.Size = new System.Drawing.Size(140, 23);
            this.bt_modifica.TabIndex = 3;
            this.bt_modifica.Text = "Modifica";
            this.bt_modifica.UseVisualStyleBackColor = true;
            this.bt_modifica.Click += new System.EventHandler(this.bt_modifica_Click);
            // 
            // FrmVenditore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 392);
            this.Controls.Add(this.bt_modifica);
            this.Controls.Add(this.bt_rimuovi);
            this.Controls.Add(this.bt_aggiungi);
            this.Controls.Add(this.lv_prodotti);
            this.Name = "FrmVenditore";
            this.Text = "Area venditore";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lv_prodotti;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Button bt_aggiungi;
        private System.Windows.Forms.Button bt_rimuovi;
        private System.Windows.Forms.Button bt_modifica;
    }
}