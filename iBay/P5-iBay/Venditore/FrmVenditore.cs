﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySqlConnector;

namespace P5_iBay
{
    // Parte di Filonzi Jacopo
    public partial class FrmVenditore : Form
    {
        #region Variabili
        MySqlConnection connection = new MySqlConnection("Server=127.0.0.1;User ID=root;Password=root;Database=ibay");
        #endregion

        #region Costruttore
        public FrmVenditore()
        {
            InitializeComponent();
            initializeListView();
        }
        #endregion

        #region Metodi
        private void initializeListView()
        {
            lv_prodotti.Items.Clear();

            connection.Open();

            var command = new MySqlCommand($"SELECT ID, Nome, Descrizione, Categoria, Quantita FROM prodotti", connection);

            var reader = command.ExecuteReader();

            while (reader.Read())
            {
                ListViewItem row = new ListViewItem(Convert.ToString(reader["ID"]));
                row.Tag = Convert.ToInt32(reader["ID"]);

                row.SubItems.Add(reader["Nome"] as string);
                row.SubItems.Add(reader["Descrizione"] as string);
                row.SubItems.Add(Enum.GetName(typeof(Program.eCategoriaProdotto), reader["Categoria"]).Replace('_', ' '));
                row.SubItems.Add(Convert.ToString(reader["Quantita"]));

                lv_prodotti.Items.Add(row);
            }

            connection.Close();
        }
        #endregion

        #region Eventi
        private void bt_rimuovi_Click(object sender, EventArgs e)
        {
            if (lv_prodotti.SelectedItems.Count != 1)
                MessageBox.Show("Seleziona un prodotto");
            else
            {
                DialogResult dr = MessageBox.Show("Sei sicuro di voler eliminare questo prodotto?", "ATTENZIONE", MessageBoxButtons.YesNo);

                if (dr == DialogResult.No)
                    return;

                try
                {
                    connection.Open();

                    var command = new MySqlCommand($"DELETE FROM prodotti WHERE ID = {lv_prodotti.SelectedItems[0].Tag}", connection);
                    command.ExecuteReader();

                    connection.Close();
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Errore di connessione al DB:\r\n " + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                initializeListView();

            }

        }

        private void bt_aggiungi_Click(object sender, EventArgs e)
        {
            // Apri la form di aggiunta prodotto in modalità inserimento
            FrmInserisciProdotto frmInserisciProdotto = new FrmInserisciProdotto(-1);
            DialogResult dr = frmInserisciProdotto.ShowDialog();

            // Se l'inserimento è avvenuto con successo refresha la ListView
            if (dr == DialogResult.OK)
            {
                initializeListView();
                frmHomePage frmHome = new frmHomePage();
                frmHome.popolaProdotti();
            }
                
        }

        private void bt_modifica_Click(object sender, EventArgs e)
        {
            // Verifico che sia stato selezionato un elemento
            if(lv_prodotti.SelectedIndices.Count == 1)
            {
                // Apri la form di aggiunta prodotto in modalità modifica
                FrmInserisciProdotto frmInserisciProdotto = new FrmInserisciProdotto((int)lv_prodotti.SelectedItems[0].Tag);
                DialogResult dr = frmInserisciProdotto.ShowDialog();

                // Se l'la modifica è avvenuta con successo refresha la ListView
                if (dr == DialogResult.OK)
                    initializeListView();
            }
        }
        #endregion
    }
}
