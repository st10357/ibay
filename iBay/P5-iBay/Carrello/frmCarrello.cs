﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySqlConnector;

namespace P5_iBay
{
    public partial class frmCarrello : Form
    {
        public frmCarrello()
        {
            InitializeComponent();
        }

        //Variabili
        MySqlConnection _connection = new MySqlConnection(Program.CONNECTION_STRING);
        MySqlCommand _query;
        MySqlDataReader _reader;

        #region Eventi
        private void frmCarrello_Resize(object sender, EventArgs e)
        {
            int x = (pbLinea.Width - lblRiepilogo.Width) / 2;
            int y = 9;

            lblRiepilogo.Location = new Point(x, y);
        }

        private void frmCarrello_Load(object sender, EventArgs e)
        {
            ucProdotto ucProdotto = new ucProdotto();

            Program.frmCarrelloAperta = true;
            Program.roudedEdges(btProcedi);
            PopolaProdotti();
        }

        private void frmCarrello_FormClosed(object sender, FormClosedEventArgs e)
        {
            Program.riepilogoCarrello = false;
            Program.frmCarrelloAperta = false;
        }

        private void btProcedi_Click(object sender, EventArgs e)
        {
            if (Program.idProdottoSelezionato == -1)
                MessageBox.Show("Selezionare un prodotto da acquistare!");
            else
            {
                frmRiepilogoOrdine riepilogoOrdine = new frmRiepilogoOrdine();
                riepilogoOrdine.ShowDialog();
                Close();
            }
        }
        #endregion

        #region Metodi
        private void PopolaProdotti()
        {
            int x = 10, y = 10, i = 0;

            foreach (Program.sProdottiCarrello prodotto in Program.ProdottiCarrello)
            {
                //dichiarazione userControl
                ucProdotto ucProdotto = new ucProdotto();

                //posizione dell'user control nel panel
                ucProdotto.Location = new Point(x, y);

                //accedo agli elementi grafici dell'userControl
                Label lblNomeProdotto = ucProdotto.Controls["lblNomeProdotto"] as Label;
                Label lblCategoria = ucProdotto.Controls["lblCategoria"] as Label;
                Label lblQuantita = ucProdotto.Controls["lblQuantita"] as Label;
                Label lblPeso = ucProdotto.Controls["lblPeso"] as Label;
                Label lblPrezzo = ucProdotto.Controls["lblPrezzo"] as Label;
                Label lblDescrizione = ucProdotto.Controls["lblDescrizione"] as Label;
                Label lblDimensioni = ucProdotto.Controls["lblDimensioni"] as Label;
                Button btSeleziona = ucProdotto.Controls["btAggiungiAlCarrello"] as Button;
                Button btMeno = ucProdotto.Controls["btMeno"] as Button;
                Button btPiu = ucProdotto.Controls["btPiu"] as Button;
                TextBox tbQuantita = ucProdotto.Controls["tbQuantita"] as TextBox;
                PictureBox pbLinea = ucProdotto.Controls["pbLinea"] as PictureBox;

                // controllo se il prodotto nella lista corrisponde con l'utente loggato
                if (prodotto.emailUtente == Program._email)
                {
                    //apro la connessione con il db
                    _connection.Open();

                    //mi ricavo il prodotto usando come chiave l'ID
                    _query = new MySqlCommand($"SELECT * from prodotti WHERE ID = {prodotto.idProdotto}", _connection);
                    _reader = _query.ExecuteReader();

                    //leggo la riga acquisita
                    _reader.Read();

                    //popolo i campi dell'userControl
                    lblNomeProdotto.Text = Convert.ToString(_reader["Nome"]);
                    lblCategoria.Text = "Categoria - " + Enum.GetName(typeof(Program.eCategoriaProdotto), Convert.ToInt32(_reader["Categoria"]));
                    lblQuantita.Text = "Quantità - " + prodotto.quantitaSelezionata + " pcs";
                    lblPeso.Text = "Peso - " + Convert.ToString(_reader["Peso"]) + " grammi";
                    lblPrezzo.Text = "Prezzo - €" + Convert.ToString(_reader["Prezzo"]);
                    lblDescrizione.Text = Convert.ToString(_reader["Descrizione"]);
                    lblDimensioni.Text = "Lunghezza - " + Convert.ToString(_reader["Lunghezza"]) + "\nAltezza - " + Convert.ToString(_reader["Lunghezza"]) + "\nProfondità - " + Convert.ToString(_reader["Profondita"]);
                    ucProdotto.idProdotto = Convert.ToInt32(_reader["ID"]);

                    btSeleziona.Name = "bt" + i;
                    i++;

                    //chiudo la connessione
                    _connection.Close();

                    //modifica di alcune proprietà
                    btSeleziona.Text = "SELEZIONA PRODOTTO";
                    btMeno.Visible = false;
                    btPiu.Visible = false;
                    tbQuantita.Visible = false;
                    btSeleziona.Location = new Point(918, 74);

                    //aggiungo l'uc al panel
                    pnlCarrello.Controls.Add(ucProdotto);

                    //spaziatura per il prossimo uc
                    y += 190;
                }
            }

        }

        public void ModificaColoreBottone()
        {
            ucProdotto ucProdotto = new ucProdotto();

            Button bt = ucProdotto.Controls[Program.nomeBtPremuto] as Button;
            bt.BackColor = SystemColors.ActiveBorder;
        }


        #endregion


    }
}
