﻿namespace P5_iBay
{
    partial class frmCarrello
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCarrello));
            this.btProcedi = new System.Windows.Forms.Button();
            this.lblRiepilogo = new System.Windows.Forms.Label();
            this.pnlCarrello = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pbLinea = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLinea)).BeginInit();
            this.SuspendLayout();
            // 
            // btProcedi
            // 
            this.btProcedi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btProcedi.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btProcedi.FlatAppearance.BorderSize = 0;
            this.btProcedi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btProcedi.Location = new System.Drawing.Point(1221, 668);
            this.btProcedi.Name = "btProcedi";
            this.btProcedi.Size = new System.Drawing.Size(117, 49);
            this.btProcedi.TabIndex = 0;
            this.btProcedi.Text = "PROCEDI ALL\'ORDINE";
            this.btProcedi.UseVisualStyleBackColor = false;
            this.btProcedi.Click += new System.EventHandler(this.btProcedi_Click);
            // 
            // lblRiepilogo
            // 
            this.lblRiepilogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRiepilogo.AutoSize = true;
            this.lblRiepilogo.Font = new System.Drawing.Font("MS Reference Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRiepilogo.Location = new System.Drawing.Point(550, 9);
            this.lblRiepilogo.Name = "lblRiepilogo";
            this.lblRiepilogo.Size = new System.Drawing.Size(251, 26);
            this.lblRiepilogo.TabIndex = 1;
            this.lblRiepilogo.Text = "RIEPILOGO CARRELLO";
            // 
            // pnlCarrello
            // 
            this.pnlCarrello.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlCarrello.Location = new System.Drawing.Point(12, 64);
            this.pnlCarrello.Name = "pnlCarrello";
            this.pnlCarrello.Size = new System.Drawing.Size(1326, 572);
            this.pnlCarrello.TabIndex = 3;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::P5_iBay.Properties.Resources.lineaOrizzontale;
            this.pictureBox1.Location = new System.Drawing.Point(12, 642);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1326, 20);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // pbLinea
            // 
            this.pbLinea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbLinea.Image = global::P5_iBay.Properties.Resources.lineaOrizzontale;
            this.pbLinea.Location = new System.Drawing.Point(12, 38);
            this.pbLinea.Name = "pbLinea";
            this.pbLinea.Size = new System.Drawing.Size(1326, 20);
            this.pbLinea.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLinea.TabIndex = 2;
            this.pbLinea.TabStop = false;
            // 
            // frmCarrello
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 729);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pnlCarrello);
            this.Controls.Add(this.pbLinea);
            this.Controls.Add(this.lblRiepilogo);
            this.Controls.Add(this.btProcedi);
            this.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MinimumSize = new System.Drawing.Size(1366, 768);
            this.Name = "frmCarrello";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CARRELLO";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmCarrello_FormClosed);
            this.Load += new System.EventHandler(this.frmCarrello_Load);
            this.Resize += new System.EventHandler(this.frmCarrello_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLinea)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btProcedi;
        private System.Windows.Forms.Label lblRiepilogo;
        private System.Windows.Forms.PictureBox pbLinea;
        private System.Windows.Forms.Panel pnlCarrello;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}