﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySqlConnector;

namespace P5_iBay
{
    public partial class FrmOrdine : Form
    {
        public enum eStatoOrdine
        {
            Elaborato,
            Approvato,
            Spedito,
            Consegnato,
            Reso
        }

        public FrmOrdine(int idOrdine)
        {
            InitializeComponent();
            popola(idOrdine);
        }

        private void popola(int idOrdine)
        {
            var connection = new MySqlConnection("Server=127.0.0.1;User ID=root;Password=root;Database=ibay");
            int idProdotto = -1;
            char metodoPagamento = ' ';
            int indirizzoSpedizione = -1;

            #region fetch ordine
            connection.Open();
            var command = new MySqlCommand($"SELECT Stato, IndirizzoSpedizioneID, MetodoPagamento, idProdotto FROM ordini WHERE IDOrdine='{idOrdine}'", connection);

            var reader = command.ExecuteReader();
            while (reader.Read())
            {
                lbl_statoOrdine.Text = Enum.GetName(typeof(eStatoOrdine), Convert.ToInt16(reader["Stato"])).Replace('_', ' ');
                lbl_metodoPagamento.Text = reader["MetodoPagamento"] as string;
                idProdotto = Convert.ToInt32(reader["idProdotto"]);
                indirizzoSpedizione = Convert.ToInt32(reader["IndirizzoSpedizioneID"]);
            }

            connection.Close();
            #endregion

            #region fetch indirizzo spedizione
            connection.Open();
            command = new MySqlCommand($"SELECT Nome FROM indirizzispedizione WHERE ID='{indirizzoSpedizione}'", connection);

            reader = command.ExecuteReader();
            while (reader.Read())
            {
                lbl_indirizzoSpedizione.Text = reader["Nome"] as string;
            }

            connection.Close();
            #endregion

            #region fetch prodotto
            connection.Open();
            command = new MySqlCommand($"SELECT Nome FROM prodotti WHERE ID='{idProdotto}'", connection);

            reader = command.ExecuteReader();
            while (reader.Read())
            {
                lbl_nomeProdotto.Text = reader["Nome"] as string;
            }

            connection.Close();
            #endregion
        }
    }
}
