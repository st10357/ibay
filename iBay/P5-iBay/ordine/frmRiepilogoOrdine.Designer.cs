﻿namespace P5_iBay
{
    partial class frmRiepilogoOrdine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblPrezzoSpedizione = new System.Windows.Forms.Label();
            this.lblPrezzoArticoli = new System.Windows.Forms.Label();
            this.lblPrezzoTotale = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btAcquista = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblIndirizzoConsegna = new System.Windows.Forms.Label();
            this.lblArticoli = new System.Windows.Forms.Label();
            this.lblPagamento = new System.Windows.Forms.Label();
            this.lblModificaIndirizzo = new System.Windows.Forms.LinkLabel();
            this.lblModificaPagamento = new System.Windows.Forms.LinkLabel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(562, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Compila i seguenti dati per procedere con l\'acquisto";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.lblPrezzoSpedizione);
            this.panel1.Controls.Add(this.lblPrezzoArticoli);
            this.panel1.Controls.Add(this.lblPrezzoTotale);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.btAcquista);
            this.panel1.Location = new System.Drawing.Point(811, 61);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(267, 223);
            this.panel1.TabIndex = 26;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::P5_iBay.Properties.Resources.lineaOrizzontale;
            this.pictureBox2.Location = new System.Drawing.Point(12, 163);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(243, 20);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 34;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::P5_iBay.Properties.Resources.lineaOrizzontale;
            this.pictureBox1.Location = new System.Drawing.Point(12, 81);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(243, 20);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 27;
            this.pictureBox1.TabStop = false;
            // 
            // lblPrezzoSpedizione
            // 
            this.lblPrezzoSpedizione.AutoSize = true;
            this.lblPrezzoSpedizione.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrezzoSpedizione.Location = new System.Drawing.Point(213, 144);
            this.lblPrezzoSpedizione.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPrezzoSpedizione.Name = "lblPrezzoSpedizione";
            this.lblPrezzoSpedizione.Size = new System.Drawing.Size(42, 16);
            this.lblPrezzoSpedizione.TabIndex = 33;
            this.lblPrezzoSpedizione.Text = "€ 0,00";
            // 
            // lblPrezzoArticoli
            // 
            this.lblPrezzoArticoli.AutoSize = true;
            this.lblPrezzoArticoli.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrezzoArticoli.Location = new System.Drawing.Point(213, 125);
            this.lblPrezzoArticoli.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPrezzoArticoli.Name = "lblPrezzoArticoli";
            this.lblPrezzoArticoli.Size = new System.Drawing.Size(42, 16);
            this.lblPrezzoArticoli.TabIndex = 32;
            this.lblPrezzoArticoli.Text = "€ 0,00";
            // 
            // lblPrezzoTotale
            // 
            this.lblPrezzoTotale.AutoSize = true;
            this.lblPrezzoTotale.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrezzoTotale.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblPrezzoTotale.Location = new System.Drawing.Point(195, 186);
            this.lblPrezzoTotale.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPrezzoTotale.Name = "lblPrezzoTotale";
            this.lblPrezzoTotale.Size = new System.Drawing.Size(60, 20);
            this.lblPrezzoTotale.TabIndex = 31;
            this.lblPrezzoTotale.Text = "€ 0,00";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label10.Location = new System.Drawing.Point(8, 186);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(125, 20);
            this.label10.TabIndex = 30;
            this.label10.Text = "Totale ordine:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(9, 144);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(121, 16);
            this.label9.TabIndex = 29;
            this.label9.Text = "Costi di spedizione";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(9, 125);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 16);
            this.label8.TabIndex = 28;
            this.label8.Text = "Articoli";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(9, 104);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(128, 16);
            this.label7.TabIndex = 27;
            this.label7.Text = "Riepilogo Ordine";
            // 
            // btAcquista
            // 
            this.btAcquista.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btAcquista.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btAcquista.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAcquista.Location = new System.Drawing.Point(11, 14);
            this.btAcquista.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btAcquista.Name = "btAcquista";
            this.btAcquista.Size = new System.Drawing.Size(244, 59);
            this.btAcquista.TabIndex = 26;
            this.btAcquista.Text = "Acquista ora";
            this.btAcquista.UseVisualStyleBackColor = false;
            this.btAcquista.Click += new System.EventHandler(this.btAcquista_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 24);
            this.label2.TabIndex = 36;
            this.label2.Text = "1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(9, 368);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 24);
            this.label3.TabIndex = 37;
            this.label3.Text = "3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(9, 235);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(22, 24);
            this.label4.TabIndex = 38;
            this.label4.Text = "2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(108, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 48);
            this.label5.TabIndex = 39;
            this.label5.Text = "Indirizzo di \r\nconsegna";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS Reference Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(108, 223);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(123, 48);
            this.label6.TabIndex = 40;
            this.label6.Text = "Modalità di \r\npagamento";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS Reference Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(108, 356);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(107, 48);
            this.label11.TabIndex = 41;
            this.label11.Text = "Riepilogo \r\narticoli";
            // 
            // lblIndirizzoConsegna
            // 
            this.lblIndirizzoConsegna.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblIndirizzoConsegna.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblIndirizzoConsegna.Location = new System.Drawing.Point(393, 61);
            this.lblIndirizzoConsegna.Name = "lblIndirizzoConsegna";
            this.lblIndirizzoConsegna.Size = new System.Drawing.Size(275, 107);
            this.lblIndirizzoConsegna.TabIndex = 42;
            this.lblIndirizzoConsegna.Text = "selezionare un indirizzo di consegna";
            // 
            // lblArticoli
            // 
            this.lblArticoli.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblArticoli.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblArticoli.Location = new System.Drawing.Point(393, 327);
            this.lblArticoli.Name = "lblArticoli";
            this.lblArticoli.Size = new System.Drawing.Size(275, 107);
            this.lblArticoli.TabIndex = 43;
            this.lblArticoli.Text = "label13";
            // 
            // lblPagamento
            // 
            this.lblPagamento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPagamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPagamento.Location = new System.Drawing.Point(393, 194);
            this.lblPagamento.Name = "lblPagamento";
            this.lblPagamento.Size = new System.Drawing.Size(275, 107);
            this.lblPagamento.TabIndex = 44;
            this.lblPagamento.Text = "selezionare un metodo di pagamento";
            // 
            // lblModificaIndirizzo
            // 
            this.lblModificaIndirizzo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblModificaIndirizzo.AutoSize = true;
            this.lblModificaIndirizzo.Location = new System.Drawing.Point(694, 106);
            this.lblModificaIndirizzo.Name = "lblModificaIndirizzo";
            this.lblModificaIndirizzo.Size = new System.Drawing.Size(62, 16);
            this.lblModificaIndirizzo.TabIndex = 45;
            this.lblModificaIndirizzo.TabStop = true;
            this.lblModificaIndirizzo.Text = "Modifica";
            this.lblModificaIndirizzo.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblModificaIndirizzo_LinkClicked);
            // 
            // lblModificaPagamento
            // 
            this.lblModificaPagamento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblModificaPagamento.AutoSize = true;
            this.lblModificaPagamento.Location = new System.Drawing.Point(694, 239);
            this.lblModificaPagamento.Name = "lblModificaPagamento";
            this.lblModificaPagamento.Size = new System.Drawing.Size(62, 16);
            this.lblModificaPagamento.TabIndex = 47;
            this.lblModificaPagamento.TabStop = true;
            this.lblModificaPagamento.Text = "Modifica";
            this.lblModificaPagamento.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblModificaPagamento_LinkClicked);
            // 
            // pictureBox6
            // 
            this.pictureBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox6.Image = global::P5_iBay.Properties.Resources.lineaVerticale;
            this.pictureBox6.Location = new System.Drawing.Point(784, 61);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(20, 378);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 50;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox5.Image = global::P5_iBay.Properties.Resources.lineaOrizzontale;
            this.pictureBox5.Location = new System.Drawing.Point(13, 304);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(765, 20);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 49;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox4.Image = global::P5_iBay.Properties.Resources.lineaOrizzontale;
            this.pictureBox4.Location = new System.Drawing.Point(13, 171);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(765, 20);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 48;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.Image = global::P5_iBay.Properties.Resources.lineaOrizzontale;
            this.pictureBox3.Location = new System.Drawing.Point(12, 38);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(1065, 20);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 35;
            this.pictureBox3.TabStop = false;
            // 
            // frmRiepilogoOrdine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1092, 470);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.lblModificaPagamento);
            this.Controls.Add(this.lblModificaIndirizzo);
            this.Controls.Add(this.lblPagamento);
            this.Controls.Add(this.lblArticoli);
            this.Controls.Add(this.lblIndirizzoConsegna);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1108, 509);
            this.Name = "frmRiepilogoOrdine";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Riepilogo ordine";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmRiepilogoOrdine_FormClosed);
            this.Load += new System.EventHandler(this.frmRiepilogoOrdine_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblPrezzoSpedizione;
        private System.Windows.Forms.Label lblPrezzoArticoli;
        private System.Windows.Forms.Label lblPrezzoTotale;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btAcquista;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblIndirizzoConsegna;
        private System.Windows.Forms.Label lblArticoli;
        private System.Windows.Forms.Label lblPagamento;
        private System.Windows.Forms.LinkLabel lblModificaIndirizzo;
        private System.Windows.Forms.LinkLabel lblModificaPagamento;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}