﻿namespace P5_iBay
{
    partial class FrmOrdine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.lbl_nomeProdotto = new System.Windows.Forms.Label();
            this.lbl_metodoPagamento = new System.Windows.Forms.Label();
            this.lbl_indirizzoSpedizione = new System.Windows.Forms.Label();
            this.lbl_statoOrdine = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Prodotto:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::P5_iBay.Properties.Resources.lineaOrizzontale;
            this.pictureBox1.Location = new System.Drawing.Point(12, 25);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(301, 20);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Metodo di pagamento:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Indirizzo di spedizione:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Stato dell\'ordine:";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::P5_iBay.Properties.Resources.lineaOrizzontale;
            this.pictureBox2.Location = new System.Drawing.Point(12, 112);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(301, 20);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 138);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(301, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Vai alla pagina del prodotto";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // lbl_nomeProdotto
            // 
            this.lbl_nomeProdotto.AutoSize = true;
            this.lbl_nomeProdotto.Location = new System.Drawing.Point(65, 9);
            this.lbl_nomeProdotto.Name = "lbl_nomeProdotto";
            this.lbl_nomeProdotto.Size = new System.Drawing.Size(35, 13);
            this.lbl_nomeProdotto.TabIndex = 7;
            this.lbl_nomeProdotto.Text = "label5";
            // 
            // lbl_metodoPagamento
            // 
            this.lbl_metodoPagamento.AutoSize = true;
            this.lbl_metodoPagamento.Location = new System.Drawing.Point(125, 48);
            this.lbl_metodoPagamento.Name = "lbl_metodoPagamento";
            this.lbl_metodoPagamento.Size = new System.Drawing.Size(35, 13);
            this.lbl_metodoPagamento.TabIndex = 8;
            this.lbl_metodoPagamento.Text = "label6";
            // 
            // lbl_indirizzoSpedizione
            // 
            this.lbl_indirizzoSpedizione.AutoSize = true;
            this.lbl_indirizzoSpedizione.Location = new System.Drawing.Point(124, 72);
            this.lbl_indirizzoSpedizione.Name = "lbl_indirizzoSpedizione";
            this.lbl_indirizzoSpedizione.Size = new System.Drawing.Size(35, 13);
            this.lbl_indirizzoSpedizione.TabIndex = 9;
            this.lbl_indirizzoSpedizione.Text = "label7";
            // 
            // lbl_statoOrdine
            // 
            this.lbl_statoOrdine.AutoSize = true;
            this.lbl_statoOrdine.Location = new System.Drawing.Point(97, 96);
            this.lbl_statoOrdine.Name = "lbl_statoOrdine";
            this.lbl_statoOrdine.Size = new System.Drawing.Size(35, 13);
            this.lbl_statoOrdine.TabIndex = 10;
            this.lbl_statoOrdine.Text = "label8";
            // 
            // FrmOrdine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(325, 173);
            this.Controls.Add(this.lbl_statoOrdine);
            this.Controls.Add(this.lbl_indirizzoSpedizione);
            this.Controls.Add(this.lbl_metodoPagamento);
            this.Controls.Add(this.lbl_nomeProdotto);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Name = "FrmOrdine";
            this.Text = "FrmOrdine";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lbl_nomeProdotto;
        private System.Windows.Forms.Label lbl_metodoPagamento;
        private System.Windows.Forms.Label lbl_indirizzoSpedizione;
        private System.Windows.Forms.Label lbl_statoOrdine;
    }
}