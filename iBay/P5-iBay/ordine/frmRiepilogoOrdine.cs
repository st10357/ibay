﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySqlConnector;
using System.IO;

// Parte di Dello Preite Maurizio
namespace P5_iBay
{
    public partial class frmRiepilogoOrdine : Form
    {
        #region Variabili
        MySqlConnection _connection = new MySqlConnection(Program.CONNECTION_STRING);
        MySqlCommand _query;
        MySqlDataReader _reader;
        #endregion

        #region Costruttore
        public frmRiepilogoOrdine()
        {
            InitializeComponent();
        }
        #endregion

        #region Eventi
        private void frmRiepilogoOrdine_Load(object sender, EventArgs e)
        {
            Program.roudedEdges(btAcquista);
            PopolaProdotti();
            AggiornaPrezzi();
        }
        #endregion

        #region Eventi
        private void btAcquista_Click(object sender, EventArgs e)
        {
            // Controlla che tutto sia stato compilato correttamente
            if (lblIndirizzoConsegna.Text == "selezionare un indirizzo di consegna")
                MessageBox.Show("Non è stato selezionato alcun indirizzo di spedizione, selezionarne uno e riprovare.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else if (lblPagamento.Text == "selezionare un metodo di pagamento")
                MessageBox.Show("Non è stato selezionato alcun metodo di pagamento, selezionarne uno e riprovare.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                // Salva l'ordine sul DB
                SalvaOrdine();

                // In caso positivo rimuovi l'articolo acquistato dal file
                int numeroDaEliminare = Program.idProdottoSelezionato;

                // Leggi tutte le righe dal file
                string[] lines = File.ReadAllLines(Program.pathCarrello);

                // Filtra le righe che non contengono il numero da eliminare
                lines = Array.FindAll(lines, line => !line.Contains(numeroDaEliminare.ToString()));

                // Sovrascrivi il file originale con il contenuto aggiornato
                File.WriteAllLines(Program.pathCarrello, lines);

                Close();

                // Resetta le variabili dell'ordine
                Program.ibanContoSelezionato = null;
                Program.codiceCartaSelezionata = null;
                Program.idIndirizzoSelezionato = -1;
                Program.idProdottoSelezionato = -1;
                Program.ProdottiCarrello.Clear();
            }
        }

        private void lblModificaIndirizzo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frmIndirizziSpedizione frmIndirizziSpedizione = new frmIndirizziSpedizione();
            frmIndirizziSpedizione.ShowDialog();

            if (Program.idIndirizzoSelezionato != -1)
                PopolaIndirizzoSpedizione();
        }

        private void lblModificaPagamento_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frmSelezionaMetodoPagamento SelezionaMetodoPagamento = new frmSelezionaMetodoPagamento();
            SelezionaMetodoPagamento.ShowDialog();

            if (Program.codiceCartaSelezionata != null || Program.ibanContoSelezionato != null)
                PopolaMetodoPagamento();
        }

        private void frmRiepilogoOrdine_FormClosed(object sender, FormClosedEventArgs e)
        {
            //quando chiudo la form devo azzerare l'id del prodotto che ho selezionato per non lasciare memorizzato quello precedente
            Program.idProdottoSelezionato = -1;


        }
        #endregion

        #region Metodi
        private void PopolaIndirizzoSpedizione()
        {
            //apro la connessione
            _connection.Open();

            //invio il comando
            _query = new MySqlCommand($"SELECT * FROM indirizzispedizione WHERE ID = {Program.idIndirizzoSelezionato}", _connection);
            _reader = _query.ExecuteReader();

            //leggo la riga
            _reader.Read();

            //popolo la label con le informazioni dell'indirizzo selezionato
            lblIndirizzoConsegna.Text = "Indirizzo - " + Convert.ToString(_reader["Indirizzo"]) +
                "\nCittà - " + Convert.ToString(_reader["Città"]) + " (" + Convert.ToString(_reader["Provincia"]) + ")" +
                "\nNome citofono - " + Convert.ToString(_reader["Nome"]);

            //chiudo la connessione
            _connection.Close();
        }

        private void PopolaMetodoPagamento()
        {
            //apro la connessione
            _connection.Open();

            //invio il comando
            if (Program.codiceCartaSelezionata != null)
            {
                _query = new MySqlCommand($"SELECT * FROM cartedicredito WHERE CodiceCarta = {Program.codiceCartaSelezionata}", _connection);
                _reader = _query.ExecuteReader();

                //leggo la riga
                _reader.Read();

                //popolo la label con le informazioni della carta di credito
                lblPagamento.Text = "Nominativo - " + Convert.ToString(_reader["NomeIntestatario"]) + " " + Convert.ToString(_reader["CognomeIntestatario"]) +
                    "\nCodice carta - " + Convert.ToString(_reader["CodiceCarta"]) +
                    "\nScadenza - " + Convert.ToString(_reader["MeseScadenza"]) + "/" + Convert.ToString(_reader["AnnoScadenza"]);

                //Program.codiceCartaSelezionata = "";
            }
            else
            {
                _query = new MySqlCommand($"SELECT * FROM conticorrente WHERE IBAN = {Program.ibanContoSelezionato}", _connection);
                _reader = _query.ExecuteReader();

                //leggo la riga
                _reader.Read();

                //popolo la label con le informazioni del conto corrente
                lblPagamento.Text = "Nominativo - " + Convert.ToString(_reader["NomeIntestatario"]) + " " + Convert.ToString(_reader["CognomeIntestatario"]) +
                    "\nIBAN - " + Convert.ToString(_reader["IBAN"]) +
                    "\nBIC - " + Convert.ToString(_reader["BIC"]);

                //Program.ibanContoSelezionato = "";
            }

            //chiudo la connessione
            _connection.Close();
        }

        private void PopolaProdotti()
        {
            int quantita = 0;

            //apro la connessione con il db
            _connection.Open();

            //mi ricavo il prodotto usando come chiave l'ID
            _query = new MySqlCommand($"SELECT * from prodotti WHERE ID = {Program.idProdottoSelezionato}", _connection);
            _reader = _query.ExecuteReader();

            //leggo la riga acquisita
            _reader.Read();

            //mi ricavo la quantità selezionata dalla lista dei prodotti
            foreach (Program.sProdottiCarrello prodotto in Program.ProdottiCarrello)
            {
                if (prodotto.emailUtente == Program._email && prodotto.idProdotto == Program.idProdottoSelezionato)
                    quantita = prodotto.quantitaSelezionata;
            }


            //popolo la label prodotto
            lblArticoli.Text = Convert.ToString(_reader["Nome"]) + " - " + Enum.GetName(typeof(Program.eCategoriaProdotto), Convert.ToInt32(_reader["Categoria"])) +
                "\nDimensioni: " + Convert.ToString(_reader["Lunghezza"]) + "L x " + Convert.ToString(_reader["Altezza"]) + "A x " + Convert.ToString(_reader["Profondita"]) + "P" +
                "\nPeso: " + Convert.ToString(_reader["Peso"]) + " grammi" +
                "\nQuantità selezionata: " + quantita;

            //popolo la label prezzo
            lblPrezzoArticoli.Text = "€ " + (quantita * Convert.ToInt16(_reader["Prezzo"]));
            lblPrezzoTotale.Text = "€ " + (quantita * Convert.ToInt16(_reader["Prezzo"])); //c'è da aggiungere i costi di spedizione...

            //chiudo la connessione
            _connection.Close();

        }

        private void AggiornaPrezzi()
        {
            // TODO: ?
        }
        
        /// <summary>
        /// Salva sul DB l'ordine appena effettuato
        /// </summary>
        private void SalvaOrdine()
        {
            try
            {
                _connection.Open();

                if(Program.ibanContoSelezionato != null)
                {
                    _query = new MySqlCommand($"INSERT INTO ordini (emailUtente, IBANcontoCorrente, Stato, indirizzoSpedizioneID, idProdotto) VALUES ('{Program._email}', '{Program.ibanContoSelezionato}', {(int)Program.eStatoOrdine.Elaborato}, '{Program.idIndirizzoSelezionato}', {Program.idProdottoSelezionato});", _connection);
                    _query.ExecuteNonQuery();
                }
                else
                {
                    _query = new MySqlCommand($"INSERT INTO ordini (emailUtente, CodiceCartaCredito, Stato, indirizzoSpedizioneID, idProdotto) VALUES ('{Program._email}', '{Program.codiceCartaSelezionata}', '{(int)Program.eStatoOrdine.Elaborato}', '{Program.idIndirizzoSelezionato}', {Program.idProdottoSelezionato});", _connection);
                    _query.ExecuteNonQuery();
                }
                

                _connection.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Errore di connessione al DB: \r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion
    }
}