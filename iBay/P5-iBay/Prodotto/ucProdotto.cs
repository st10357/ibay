﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using MySqlConnector;

namespace P5_iBay
{
    public partial class ucProdotto : UserControl
    {
        public ucProdotto()
        {
            InitializeComponent();
        }

        //variabili
        int i = 0;
        public int idProdotto = 0;

        frmHomePage frmHomePage = new frmHomePage();

        #region Eventi
        private void ucProdotto_Load(object sender, EventArgs e)
        {
            Program.roudedEdges(btAggiungiAlCarrello);
            Program.roudedEdges(btMeno);
            Program.roudedEdges(btPiu);
        }

        private void btPiu_Click(object sender, EventArgs e)
        {
            i++;
            tbQuantita.Text = i.ToString();
        }

        private void btMeno_Click(object sender, EventArgs e)
        {
            i--;

            if (i == -1)
                i = 0;
            tbQuantita.Text = i.ToString();
        }

        /// <summary>
        /// popolo un file di testo inserendo emailCompratore, ID prodotto e quantità selezionata
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btAggiungiAlCarrello_Click(object sender, EventArgs e)
        {
            if (Program.riepilogoCarrello)
            {
                btAggiungiAlCarrello.BackColor = SystemColors.ActiveCaption;
                Program.idProdottoSelezionato = idProdotto;
            }
            else
            {
                //variabili
                int quantitaAttuale;

                // Effettuo la connessione al DB
                MySqlConnection _connection = new MySqlConnection(Program.CONNECTION_STRING);

                try
                {
                    //apro la connessione
                    _connection.Open();

                    //invio il comando sql
                    MySqlCommand _query = new MySqlCommand($"SELECT * FROM prodotti WHERE ID = {idProdotto}", _connection);
                    MySqlDataReader _reader = _query.ExecuteReader();

                    _reader.Read();

                    //mi acquisisco la quantità disponibile di quel prodotto
                    quantitaAttuale = Convert.ToInt32(_reader["Quantita"]);

                    //chiude la connessione
                    _connection.Close();

                    if (quantitaAttuale > 0)
                    {
                        if (tbQuantita.Text != "0" && (Convert.ToInt32(tbQuantita.Text) <= quantitaAttuale))
                        {
                            //aggiorno o aggiungo la lista dei prodotti nel carrello
                            AggiornaLista();

                            //popolo il file di testo
                            PopolaFile();

                            //apro la connessione al db per aggiornare la quantità del prodotto aggiunto al carrello
                            _connection.Open();

                            //invio il comando
                            quantitaAttuale -= Convert.ToInt32(tbQuantita.Text);
                            _query = new MySqlCommand($"UPDATE prodotti SET Quantita = {quantitaAttuale} WHERE ID = {idProdotto}", _connection);

                            //eseguo il comando
                            _query.ExecuteNonQuery();

                            //chiudo la connessione
                            _connection.Close();

                            //aggiorno il testo della label
                            lblQuantita.Text = "Quantità - " + quantitaAttuale + " pcs";
                        }
                        else
                            MessageBox.Show("Inserire una quantità valida!", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                        MessageBox.Show("Questo prodotto non è più disponibile!", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                frmHomePage.popolaLbCarrello();
                tbQuantita.Text = "0";
                i = 0;
            }
        }




        #region Eventi di controllo textBox
        private void tbQuantita_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != '\b')
            {
                // Impedisce l'inserimento del carattere nella TextBox
                e.Handled = true;
            }
        }

        private void tbQuantita_TextChanged(object sender, EventArgs e)
        {
            if (tbQuantita.Text == "")
                tbQuantita.Text = "0";
        }
        #endregion

        #endregion

        #region Metodi

        private void AggiornaLista()
        {
            bool trovato = false;

            //controllo se ho gia almeno una quantità del prodotto che voglio mettere nel carrello
            for (int i = 0; i < Program.ProdottiCarrello.Count; i++)
            {
                if (Program.ProdottiCarrello[i].idProdotto == idProdotto && Program._email == Program.ProdottiCarrello[i].emailUtente)
                {
                    trovato = true;


                    Program.ProdottoCarrello.emailUtente = Program.ProdottiCarrello[i].emailUtente;
                    Program.ProdottoCarrello.idProdotto = Program.ProdottiCarrello[i].idProdotto;
                    Program.ProdottoCarrello.quantitaSelezionata = Program.ProdottiCarrello[i].quantitaSelezionata + Convert.ToInt32(tbQuantita.Text);

                    Program.ProdottiCarrello[i] = Program.ProdottoCarrello;


                    //Program.ProdottiCarrello[i].quantitaSelezionata += Convert.ToInt32(tbQuantita.Text);  //da errore
                }

            }

            //se non è presente l'articolo nel carrello lo aggiungo alla lista
            if (!trovato)
            {
                Program.ProdottoCarrello.emailUtente = Program._email;
                Program.ProdottoCarrello.idProdotto = idProdotto;
                Program.ProdottoCarrello.quantitaSelezionata = Convert.ToInt32(tbQuantita.Text);

                Program.ProdottiCarrello.Add(Program.ProdottoCarrello);
            }
        }

        private void PopolaFile()
        {
            //apro la connessione
            StreamWriter sw = new StreamWriter(Program.pathCarrello);

            foreach (Program.sProdottiCarrello prodottoCarrello in Program.ProdottiCarrello)
            {
                //mi salvo le informazioni chiave del prodotto su file
                string linea = prodottoCarrello.emailUtente + "|" + prodottoCarrello.idProdotto + "|" + prodottoCarrello.quantitaSelezionata;
                sw.WriteLine(linea);
            }

            //chiudo lo streamWriter
            sw.Close();
        }

        #endregion


    }
}
