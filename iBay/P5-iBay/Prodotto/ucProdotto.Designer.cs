﻿namespace P5_iBay
{
    partial class ucProdotto
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblNomeProdotto = new System.Windows.Forms.Label();
            this.lblCategoria = new System.Windows.Forms.Label();
            this.lblPrezzo = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblPeso = new System.Windows.Forms.Label();
            this.lblQuantita = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblDescrizione = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.lblDimensioni = new System.Windows.Forms.Label();
            this.btMeno = new System.Windows.Forms.Button();
            this.tbQuantita = new System.Windows.Forms.TextBox();
            this.btPiu = new System.Windows.Forms.Button();
            this.pbLinea = new System.Windows.Forms.PictureBox();
            this.btAggiungiAlCarrello = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLinea)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::P5_iBay.Properties.Resources.product_icon;
            this.pictureBox1.Location = new System.Drawing.Point(10, 45);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(144, 130);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // lblNomeProdotto
            // 
            this.lblNomeProdotto.Font = new System.Drawing.Font("MS Reference Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeProdotto.Location = new System.Drawing.Point(4, 0);
            this.lblNomeProdotto.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNomeProdotto.Name = "lblNomeProdotto";
            this.lblNomeProdotto.Size = new System.Drawing.Size(1228, 41);
            this.lblNomeProdotto.TabIndex = 1;
            this.lblNomeProdotto.Text = "Cavo USB C - molto potente - grigio - S";
            // 
            // lblCategoria
            // 
            this.lblCategoria.AutoSize = true;
            this.lblCategoria.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCategoria.Location = new System.Drawing.Point(162, 45);
            this.lblCategoria.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCategoria.Name = "lblCategoria";
            this.lblCategoria.Size = new System.Drawing.Size(241, 20);
            this.lblCategoria.TabIndex = 2;
            this.lblCategoria.Text = "Categoria - Elettrodomestici";
            // 
            // lblPrezzo
            // 
            this.lblPrezzo.AutoSize = true;
            this.lblPrezzo.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrezzo.Location = new System.Drawing.Point(162, 144);
            this.lblPrezzo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPrezzo.Name = "lblPrezzo";
            this.lblPrezzo.Size = new System.Drawing.Size(122, 20);
            this.lblPrezzo.TabIndex = 3;
            this.lblPrezzo.Text = "Prezzo - €104";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(680, 45);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "Dimensioni";
            // 
            // lblPeso
            // 
            this.lblPeso.AutoSize = true;
            this.lblPeso.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeso.Location = new System.Drawing.Point(162, 111);
            this.lblPeso.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPeso.Name = "lblPeso";
            this.lblPeso.Size = new System.Drawing.Size(106, 20);
            this.lblPeso.TabIndex = 5;
            this.lblPeso.Text = "Peso - 10kg";
            // 
            // lblQuantita
            // 
            this.lblQuantita.AutoSize = true;
            this.lblQuantita.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantita.Location = new System.Drawing.Point(162, 78);
            this.lblQuantita.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblQuantita.Name = "lblQuantita";
            this.lblQuantita.Size = new System.Drawing.Size(143, 20);
            this.lblQuantita.TabIndex = 6;
            this.lblQuantita.Text = "Quantità - 5 pcs";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(440, 45);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 20);
            this.label7.TabIndex = 7;
            this.label7.Text = "Descrizione";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::P5_iBay.Properties.Resources.lineaVerticale;
            this.pictureBox2.Location = new System.Drawing.Point(418, 45);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(20, 131);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 8;
            this.pictureBox2.TabStop = false;
            // 
            // lblDescrizione
            // 
            this.lblDescrizione.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDescrizione.Location = new System.Drawing.Point(444, 75);
            this.lblDescrizione.Name = "lblDescrizione";
            this.lblDescrizione.Size = new System.Drawing.Size(208, 101);
            this.lblDescrizione.TabIndex = 9;
            this.lblDescrizione.Text = "label2";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::P5_iBay.Properties.Resources.lineaVerticale;
            this.pictureBox3.Location = new System.Drawing.Point(658, 45);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(20, 131);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 10;
            this.pictureBox3.TabStop = false;
            // 
            // lblDimensioni
            // 
            this.lblDimensioni.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDimensioni.Location = new System.Drawing.Point(684, 72);
            this.lblDimensioni.Name = "lblDimensioni";
            this.lblDimensioni.Size = new System.Drawing.Size(202, 100);
            this.lblDimensioni.TabIndex = 11;
            this.lblDimensioni.Text = "Lunghezza - \r\nAltezza - \r\nProfondità -\r\n";
            // 
            // btMeno
            // 
            this.btMeno.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btMeno.FlatAppearance.BorderSize = 0;
            this.btMeno.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btMeno.Location = new System.Drawing.Point(918, 67);
            this.btMeno.Name = "btMeno";
            this.btMeno.Size = new System.Drawing.Size(35, 35);
            this.btMeno.TabIndex = 12;
            this.btMeno.Text = "-";
            this.btMeno.UseVisualStyleBackColor = false;
            this.btMeno.Click += new System.EventHandler(this.btMeno_Click);
            // 
            // tbQuantita
            // 
            this.tbQuantita.Location = new System.Drawing.Point(959, 74);
            this.tbQuantita.Name = "tbQuantita";
            this.tbQuantita.Size = new System.Drawing.Size(45, 24);
            this.tbQuantita.TabIndex = 13;
            this.tbQuantita.Text = "0";
            this.tbQuantita.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbQuantita.TextChanged += new System.EventHandler(this.tbQuantita_TextChanged);
            this.tbQuantita.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbQuantita_KeyPress);
            // 
            // btPiu
            // 
            this.btPiu.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btPiu.FlatAppearance.BorderSize = 0;
            this.btPiu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btPiu.Location = new System.Drawing.Point(1010, 67);
            this.btPiu.Name = "btPiu";
            this.btPiu.Size = new System.Drawing.Size(35, 35);
            this.btPiu.TabIndex = 14;
            this.btPiu.Text = "+";
            this.btPiu.UseVisualStyleBackColor = false;
            this.btPiu.Click += new System.EventHandler(this.btPiu_Click);
            // 
            // pbLinea
            // 
            this.pbLinea.Image = global::P5_iBay.Properties.Resources.lineaVerticale;
            this.pbLinea.Location = new System.Drawing.Point(892, 41);
            this.pbLinea.Name = "pbLinea";
            this.pbLinea.Size = new System.Drawing.Size(20, 131);
            this.pbLinea.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLinea.TabIndex = 15;
            this.pbLinea.TabStop = false;
            // 
            // btAggiungiAlCarrello
            // 
            this.btAggiungiAlCarrello.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btAggiungiAlCarrello.FlatAppearance.BorderSize = 0;
            this.btAggiungiAlCarrello.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAggiungiAlCarrello.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btAggiungiAlCarrello.Location = new System.Drawing.Point(918, 108);
            this.btAggiungiAlCarrello.Name = "btAggiungiAlCarrello";
            this.btAggiungiAlCarrello.Size = new System.Drawing.Size(127, 64);
            this.btAggiungiAlCarrello.TabIndex = 16;
            this.btAggiungiAlCarrello.Text = "AGGIUNGI AL CARRELLO";
            this.btAggiungiAlCarrello.UseVisualStyleBackColor = false;
            this.btAggiungiAlCarrello.Click += new System.EventHandler(this.btAggiungiAlCarrello_Click);
            // 
            // ucProdotto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btAggiungiAlCarrello);
            this.Controls.Add(this.pbLinea);
            this.Controls.Add(this.btPiu);
            this.Controls.Add(this.tbQuantita);
            this.Controls.Add(this.btMeno);
            this.Controls.Add(this.lblDimensioni);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.lblDescrizione);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblQuantita);
            this.Controls.Add(this.lblPeso);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblPrezzo);
            this.Controls.Add(this.lblCategoria);
            this.Controls.Add(this.lblNomeProdotto);
            this.Controls.Add(this.pictureBox1);
            this.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ucProdotto";
            this.Size = new System.Drawing.Size(1056, 180);
            this.Load += new System.EventHandler(this.ucProdotto_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLinea)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblNomeProdotto;
        private System.Windows.Forms.Label lblCategoria;
        private System.Windows.Forms.Label lblPrezzo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblPeso;
        private System.Windows.Forms.Label lblQuantita;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblDescrizione;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label lblDimensioni;
        private System.Windows.Forms.Button btMeno;
        private System.Windows.Forms.TextBox tbQuantita;
        private System.Windows.Forms.Button btPiu;
        private System.Windows.Forms.PictureBox pbLinea;
        private System.Windows.Forms.Button btAggiungiAlCarrello;
    }
}
