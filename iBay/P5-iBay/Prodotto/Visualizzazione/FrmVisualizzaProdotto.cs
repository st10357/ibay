﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySqlConnector;

namespace P5_iBay
{
    // Parte di Sebastianelli Tomas
    public partial class FrmProdotto : Form
    {
        #region Variabili
        int _IDProdotto;
        MySqlConnection _connection = new MySqlConnection(Program.CONNECTION_STRING);
        #endregion

        #region Costruttore
        public FrmProdotto(int idProdotto)
        {
            InitializeComponent();

            // Memorizzo l'ID del prodotto
            _IDProdotto = idProdotto;
        }
        #endregion

        #region Metodi
        /// <summary>
        /// Ottiene il prodotto dal DB e lo carica sui vari controlli grafici
        /// </summary>
        private void ottieniProdotto()
        {
            try
            {
                _connection.Open();

                MySqlCommand _query = new MySqlCommand($"SELECT * FROM prodotti WHERE ID = '{_IDProdotto}'", _connection);
                MySqlDataReader _reader = _query.ExecuteReader();
                _reader.Read();

                this.Text = Convert.ToString(_reader["Nome"]);
                lblNomeProdotto.Text = Convert.ToString(_reader["Nome"]);
                lblQuantita.Text = Convert.ToString(_reader["Quantita"]);
                lblPrezzo.Text = Convert.ToString(_reader["Prezzo"]);
                tbDescrizione.Text = Convert.ToString(_reader["Descrizione"]);
                lblCategoria.Text = Enum.GetName(typeof(Program.eCategoriaProdotto), _reader["Categoria"]).Replace('_', ' ');
                lblLunghezza.Text = Convert.ToString(_reader["Lunghezza"]);
                lblAltezza.Text = Convert.ToString(_reader["Altezza"]);
                lblProfondita.Text = Convert.ToString(_reader["Profondita"]);
                

                _connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        /// <summary>
        /// Ottengo le recensioni sul prodotto dal DB e le carica nella Form
        /// </summary>
        private void ottieniRecensioni()
        {
            try
            {
                // Variabile per la distanza dello UserControl recensione rispetto all'alto
                int i = -1;

                _connection.Open();

                MySqlCommand _query = new MySqlCommand($"SELECT ID FROM recensioni WHERE IDProdotto = '{_IDProdotto}'", _connection);
                MySqlDataReader _reader = _query.ExecuteReader();

                while (_reader.Read())
                {
                    i++;
                    UCrecensione _recensione = new UCrecensione((int)_reader["ID"]);
                    _recensione.Location = new Point(0, _recensione.Height * i);
                    pnlRecensioni.Controls.Add(_recensione);
                }

                _connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }
        #endregion

        #region Eventi
        private void FrmProdotto_Load(object sender, EventArgs e)
        {
            // Ottieni il prodotto dal DB e caricalo sui vari controlli grafici
            ottieniProdotto();

            // Ottengo le recensioni sul prodotto dal DB e le carico nella Form
            ottieniRecensioni();
        }

        private void btScriviRecensione_Click(object sender, EventArgs e)
        {
            FrmScriviRecensione frmScriviRecensione = new FrmScriviRecensione(_IDProdotto, -1);
            frmScriviRecensione.Show();

            // Refresh delle recensioni
            ottieniRecensioni();
        }

        // TODO: Evento che ascolta se UserControl recensione ha richiesto la modifica
        #endregion
    }
}
