﻿namespace P5_iBay
{
    partial class FrmProdotto
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNomeProdotto = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPrezzo = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btAcquista = new System.Windows.Forms.Button();
            this.btAggiungiAlCarrello = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblQuantita = new System.Windows.Forms.Label();
            this.lblProfondita = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblAltezza = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblLunghezza = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblCategoria = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblDisponibilita = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblVenditore = new System.Windows.Forms.Label();
            this.lblSpedizione = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbDescrizione = new System.Windows.Forms.TextBox();
            this.pnlRecensioni = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.btScriviRecensione = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblNomeProdotto
            // 
            this.lblNomeProdotto.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNomeProdotto.AutoSize = true;
            this.lblNomeProdotto.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeProdotto.Location = new System.Drawing.Point(20, 17);
            this.lblNomeProdotto.Name = "lblNomeProdotto";
            this.lblNomeProdotto.Size = new System.Drawing.Size(203, 32);
            this.lblNomeProdotto.TabIndex = 0;
            this.lblNomeProdotto.Text = "NomeProdotto";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 19);
            this.label1.TabIndex = 7;
            this.label1.Text = "€";
            // 
            // lblPrezzo
            // 
            this.lblPrezzo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPrezzo.AutoSize = true;
            this.lblPrezzo.Font = new System.Drawing.Font("Century Gothic", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrezzo.Location = new System.Drawing.Point(38, 104);
            this.lblPrezzo.Name = "lblPrezzo";
            this.lblPrezzo.Size = new System.Drawing.Size(16, 19);
            this.lblPrezzo.TabIndex = 8;
            this.lblPrezzo.Text = "-";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label3.Location = new System.Drawing.Point(23, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 16);
            this.label3.TabIndex = 9;
            this.label3.Text = "Spedizione entro il:";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label4.Location = new System.Drawing.Point(137, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(12, 16);
            this.label4.TabIndex = 10;
            this.label4.Text = "-";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label5.Location = new System.Drawing.Point(23, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 16);
            this.label5.TabIndex = 11;
            this.label5.Text = "Quantità";
            // 
            // btAcquista
            // 
            this.btAcquista.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btAcquista.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(61)))), ((int)(((byte)(124)))));
            this.btAcquista.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAcquista.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.btAcquista.ForeColor = System.Drawing.Color.White;
            this.btAcquista.Location = new System.Drawing.Point(26, 135);
            this.btAcquista.Name = "btAcquista";
            this.btAcquista.Size = new System.Drawing.Size(178, 29);
            this.btAcquista.TabIndex = 13;
            this.btAcquista.Text = "Acquista ora";
            this.btAcquista.UseVisualStyleBackColor = false;
            // 
            // btAggiungiAlCarrello
            // 
            this.btAggiungiAlCarrello.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btAggiungiAlCarrello.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(31)))), ((int)(((byte)(66)))));
            this.btAggiungiAlCarrello.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAggiungiAlCarrello.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.btAggiungiAlCarrello.ForeColor = System.Drawing.Color.White;
            this.btAggiungiAlCarrello.Location = new System.Drawing.Point(26, 170);
            this.btAggiungiAlCarrello.Name = "btAggiungiAlCarrello";
            this.btAggiungiAlCarrello.Size = new System.Drawing.Size(178, 29);
            this.btAggiungiAlCarrello.TabIndex = 14;
            this.btAggiungiAlCarrello.Text = "Aggiungi al carrello";
            this.btAggiungiAlCarrello.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblQuantita);
            this.panel1.Controls.Add(this.lblProfondita);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.lblAltezza);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.lblLunghezza);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.lblCategoria);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.lblDisponibilita);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lblVenditore);
            this.panel1.Controls.Add(this.lblSpedizione);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.lblNomeProdotto);
            this.panel1.Controls.Add(this.btAggiungiAlCarrello);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btAcquista);
            this.panel1.Controls.Add(this.lblPrezzo);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(-1, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(241, 333);
            this.panel1.TabIndex = 16;
            // 
            // lblQuantita
            // 
            this.lblQuantita.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQuantita.AutoSize = true;
            this.lblQuantita.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.lblQuantita.Location = new System.Drawing.Point(137, 85);
            this.lblQuantita.Name = "lblQuantita";
            this.lblQuantita.Size = new System.Drawing.Size(12, 16);
            this.lblQuantita.TabIndex = 31;
            this.lblQuantita.Text = "-";
            // 
            // lblProfondita
            // 
            this.lblProfondita.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProfondita.AutoSize = true;
            this.lblProfondita.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.lblProfondita.Location = new System.Drawing.Point(98, 300);
            this.lblProfondita.Name = "lblProfondita";
            this.lblProfondita.Size = new System.Drawing.Size(12, 16);
            this.lblProfondita.TabIndex = 30;
            this.lblProfondita.Text = "-";
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label16.Location = new System.Drawing.Point(23, 300);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(67, 16);
            this.label16.TabIndex = 29;
            this.label16.Text = "Profondità:";
            // 
            // lblAltezza
            // 
            this.lblAltezza.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAltezza.AutoSize = true;
            this.lblAltezza.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.lblAltezza.Location = new System.Drawing.Point(98, 284);
            this.lblAltezza.Name = "lblAltezza";
            this.lblAltezza.Size = new System.Drawing.Size(12, 16);
            this.lblAltezza.TabIndex = 28;
            this.lblAltezza.Text = "-";
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label14.Location = new System.Drawing.Point(23, 284);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 16);
            this.label14.TabIndex = 27;
            this.label14.Text = "Altezza:";
            // 
            // lblLunghezza
            // 
            this.lblLunghezza.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLunghezza.AutoSize = true;
            this.lblLunghezza.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.lblLunghezza.Location = new System.Drawing.Point(98, 268);
            this.lblLunghezza.Name = "lblLunghezza";
            this.lblLunghezza.Size = new System.Drawing.Size(12, 16);
            this.lblLunghezza.TabIndex = 26;
            this.lblLunghezza.Text = "-";
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label12.Location = new System.Drawing.Point(23, 268);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 16);
            this.label12.TabIndex = 25;
            this.label12.Text = "Lunghezza:";
            // 
            // lblCategoria
            // 
            this.lblCategoria.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCategoria.AutoSize = true;
            this.lblCategoria.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.lblCategoria.Location = new System.Drawing.Point(98, 252);
            this.lblCategoria.Name = "lblCategoria";
            this.lblCategoria.Size = new System.Drawing.Size(12, 16);
            this.lblCategoria.TabIndex = 24;
            this.lblCategoria.Text = "-";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label10.Location = new System.Drawing.Point(23, 252);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 16);
            this.label10.TabIndex = 22;
            this.label10.Text = "Categoria";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label11.Location = new System.Drawing.Point(17, 231);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(194, 16);
            this.label11.TabIndex = 21;
            this.label11.Text = "_______________________________";
            // 
            // lblDisponibilita
            // 
            this.lblDisponibilita.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDisponibilita.AutoSize = true;
            this.lblDisponibilita.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.lblDisponibilita.Location = new System.Drawing.Point(137, 69);
            this.lblDisponibilita.Name = "lblDisponibilita";
            this.lblDisponibilita.Size = new System.Drawing.Size(12, 16);
            this.lblDisponibilita.TabIndex = 20;
            this.lblDisponibilita.Text = "-";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label2.Location = new System.Drawing.Point(23, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 16);
            this.label2.TabIndex = 19;
            this.label2.Text = "Disponibilità:";
            // 
            // lblVenditore
            // 
            this.lblVenditore.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVenditore.AutoSize = true;
            this.lblVenditore.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.lblVenditore.Location = new System.Drawing.Point(98, 218);
            this.lblVenditore.Name = "lblVenditore";
            this.lblVenditore.Size = new System.Drawing.Size(12, 16);
            this.lblVenditore.TabIndex = 18;
            this.lblVenditore.Text = "-";
            // 
            // lblSpedizione
            // 
            this.lblSpedizione.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSpedizione.AutoSize = true;
            this.lblSpedizione.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.lblSpedizione.Location = new System.Drawing.Point(98, 206);
            this.lblSpedizione.Name = "lblSpedizione";
            this.lblSpedizione.Size = new System.Drawing.Size(12, 16);
            this.lblSpedizione.TabIndex = 17;
            this.lblSpedizione.Text = "-";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label7.Location = new System.Drawing.Point(23, 219);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 16);
            this.label7.TabIndex = 16;
            this.label7.Text = "Venditore:";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label6.Location = new System.Drawing.Point(23, 206);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 16);
            this.label6.TabIndex = 15;
            this.label6.Text = "Spedizione:";
            // 
            // tbDescrizione
            // 
            this.tbDescrizione.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.tbDescrizione.Location = new System.Drawing.Point(241, 0);
            this.tbDescrizione.Multiline = true;
            this.tbDescrizione.Name = "tbDescrizione";
            this.tbDescrizione.Size = new System.Drawing.Size(505, 333);
            this.tbDescrizione.TabIndex = 20;
            this.tbDescrizione.Text = "-";
            // 
            // pnlRecensioni
            // 
            this.pnlRecensioni.AutoScroll = true;
            this.pnlRecensioni.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlRecensioni.Location = new System.Drawing.Point(0, 387);
            this.pnlRecensioni.Name = "pnlRecensioni";
            this.pnlRecensioni.Size = new System.Drawing.Size(747, 221);
            this.pnlRecensioni.TabIndex = 21;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(19, 344);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(163, 32);
            this.label8.TabIndex = 32;
            this.label8.Text = "Recensioni:";
            // 
            // btScriviRecensione
            // 
            this.btScriviRecensione.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btScriviRecensione.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(61)))), ((int)(((byte)(124)))));
            this.btScriviRecensione.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btScriviRecensione.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.btScriviRecensione.ForeColor = System.Drawing.Color.White;
            this.btScriviRecensione.Location = new System.Drawing.Point(557, 344);
            this.btScriviRecensione.Name = "btScriviRecensione";
            this.btScriviRecensione.Size = new System.Drawing.Size(178, 32);
            this.btScriviRecensione.TabIndex = 32;
            this.btScriviRecensione.Text = "Scrivi recensione";
            this.btScriviRecensione.UseVisualStyleBackColor = false;
            this.btScriviRecensione.Click += new System.EventHandler(this.btScriviRecensione_Click);
            // 
            // FrmProdotto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 608);
            this.Controls.Add(this.btScriviRecensione);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.pnlRecensioni);
            this.Controls.Add(this.tbDescrizione);
            this.Controls.Add(this.panel1);
            this.MinimumSize = new System.Drawing.Size(763, 488);
            this.Name = "FrmProdotto";
            this.Text = "Prodotto";
            this.Load += new System.EventHandler(this.FrmProdotto_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNomeProdotto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPrezzo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btAcquista;
        private System.Windows.Forms.Button btAggiungiAlCarrello;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblVenditore;
        private System.Windows.Forms.Label lblSpedizione;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbDescrizione;
        private System.Windows.Forms.Label lblDisponibilita;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblAltezza;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblLunghezza;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblCategoria;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblProfondita;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblQuantita;
        private System.Windows.Forms.Panel pnlRecensioni;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btScriviRecensione;
    }
}

