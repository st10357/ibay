﻿namespace P5_iBay
{
    partial class FrmInserisciProdotto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.tbPIVA = new System.Windows.Forms.TextBox();
            this.tbNome = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbCategoria = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.nudProfondita = new System.Windows.Forms.NumericUpDown();
            this.nudAltezza = new System.Windows.Forms.NumericUpDown();
            this.nudLunghezza = new System.Windows.Forms.NumericUpDown();
            this.nudPeso = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.nudPrezzo = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.tbDescrizione = new System.Windows.Forms.TextBox();
            this.btSalva = new System.Windows.Forms.Button();
            this.btAnnulla = new System.Windows.Forms.Button();
            this.nudPezziDisponibili = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nudProfondita)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAltezza)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLunghezza)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPeso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrezzo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPezziDisponibili)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label1.Location = new System.Drawing.Point(12, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "P.IVA Azienda";
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.textBox2.Location = new System.Drawing.Point(11, 12);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(237, 66);
            this.textBox2.TabIndex = 0;
            this.textBox2.Text = "Compila i seguenti campi con le informazioni richieste per inserire il prodotto. " +
    "I campi contrassegnati con un * sono obbligatori.";
            // 
            // tbPIVA
            // 
            this.tbPIVA.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.tbPIVA.Location = new System.Drawing.Point(14, 100);
            this.tbPIVA.MaxLength = 11;
            this.tbPIVA.Name = "tbPIVA";
            this.tbPIVA.Size = new System.Drawing.Size(233, 21);
            this.tbPIVA.TabIndex = 2;
            // 
            // tbNome
            // 
            this.tbNome.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.tbNome.Location = new System.Drawing.Point(14, 144);
            this.tbNome.Name = "tbNome";
            this.tbNome.Size = new System.Drawing.Size(233, 21);
            this.tbNome.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label3.Location = new System.Drawing.Point(12, 125);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Nome prodotto*";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label4.Location = new System.Drawing.Point(12, 168);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "Categoria*";
            // 
            // cbCategoria
            // 
            this.cbCategoria.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.cbCategoria.FormattingEnabled = true;
            this.cbCategoria.Items.AddRange(new object[] {
            "Bellezza",
            "Salute",
            "Alimentari",
            "Prima infanzia",
            "Animali",
            "Casa",
            "Sport",
            "Tempo libero",
            "Industria",
            "Scienza",
            "Moda",
            "Giardino",
            "Elettronica",
            "Fai da te",
            "Videogiochi",
            "Auto e moto",
            "Cancelleria",
            "Informatica",
            "Giochi",
            "CD e vinili",
            "Elettrodomestici",
            "Altro"});
            this.cbCategoria.Location = new System.Drawing.Point(15, 187);
            this.cbCategoria.Name = "cbCategoria";
            this.cbCategoria.Size = new System.Drawing.Size(231, 24);
            this.cbCategoria.TabIndex = 4;
            this.cbCategoria.Text = "Seleziona la categoria...";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label5.Location = new System.Drawing.Point(11, 352);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 16);
            this.label5.TabIndex = 10;
            this.label5.Text = "Lunghezza (m)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label6.Location = new System.Drawing.Point(144, 352);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 16);
            this.label6.TabIndex = 12;
            this.label6.Text = "Altezza (m)";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label7.Location = new System.Drawing.Point(10, 395);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 16);
            this.label7.TabIndex = 14;
            this.label7.Text = "Profondità (m)";
            // 
            // nudProfondita
            // 
            this.nudProfondita.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.nudProfondita.Location = new System.Drawing.Point(13, 414);
            this.nudProfondita.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.nudProfondita.Name = "nudProfondita";
            this.nudProfondita.Size = new System.Drawing.Size(98, 21);
            this.nudProfondita.TabIndex = 8;
            // 
            // nudAltezza
            // 
            this.nudAltezza.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.nudAltezza.Location = new System.Drawing.Point(147, 371);
            this.nudAltezza.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.nudAltezza.Name = "nudAltezza";
            this.nudAltezza.Size = new System.Drawing.Size(98, 21);
            this.nudAltezza.TabIndex = 7;
            // 
            // nudLunghezza
            // 
            this.nudLunghezza.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.nudLunghezza.Location = new System.Drawing.Point(14, 371);
            this.nudLunghezza.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.nudLunghezza.Name = "nudLunghezza";
            this.nudLunghezza.Size = new System.Drawing.Size(98, 21);
            this.nudLunghezza.TabIndex = 6;
            // 
            // nudPeso
            // 
            this.nudPeso.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.nudPeso.Location = new System.Drawing.Point(147, 414);
            this.nudPeso.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.nudPeso.Name = "nudPeso";
            this.nudPeso.Size = new System.Drawing.Size(98, 21);
            this.nudPeso.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label8.Location = new System.Drawing.Point(144, 395);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 16);
            this.label8.TabIndex = 18;
            this.label8.Text = "Peso (kg)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label9.Location = new System.Drawing.Point(12, 481);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 16);
            this.label9.TabIndex = 20;
            this.label9.Text = "Prezzo (€)*";
            // 
            // nudPrezzo
            // 
            this.nudPrezzo.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.nudPrezzo.Location = new System.Drawing.Point(14, 500);
            this.nudPrezzo.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.nudPrezzo.Name = "nudPrezzo";
            this.nudPrezzo.Size = new System.Drawing.Size(232, 21);
            this.nudPrezzo.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label10.Location = new System.Drawing.Point(12, 214);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 16);
            this.label10.TabIndex = 22;
            this.label10.Text = "Descrizione";
            // 
            // tbDescrizione
            // 
            this.tbDescrizione.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.tbDescrizione.Location = new System.Drawing.Point(15, 233);
            this.tbDescrizione.Multiline = true;
            this.tbDescrizione.Name = "tbDescrizione";
            this.tbDescrizione.Size = new System.Drawing.Size(231, 116);
            this.tbDescrizione.TabIndex = 5;
            // 
            // btSalva
            // 
            this.btSalva.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(61)))), ((int)(((byte)(124)))));
            this.btSalva.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.btSalva.ForeColor = System.Drawing.Color.White;
            this.btSalva.Location = new System.Drawing.Point(137, 527);
            this.btSalva.Name = "btSalva";
            this.btSalva.Size = new System.Drawing.Size(109, 23);
            this.btSalva.TabIndex = 24;
            this.btSalva.Text = "Salva";
            this.btSalva.UseVisualStyleBackColor = false;
            this.btSalva.Click += new System.EventHandler(this.btSalva_Click);
            // 
            // btAnnulla
            // 
            this.btAnnulla.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.btAnnulla.Location = new System.Drawing.Point(12, 527);
            this.btAnnulla.Name = "btAnnulla";
            this.btAnnulla.Size = new System.Drawing.Size(109, 23);
            this.btAnnulla.TabIndex = 25;
            this.btAnnulla.Text = "Annulla";
            this.btAnnulla.UseVisualStyleBackColor = true;
            this.btAnnulla.Click += new System.EventHandler(this.btAnnulla_Click);
            // 
            // nudPezziDisponibili
            // 
            this.nudPezziDisponibili.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.nudPezziDisponibili.Location = new System.Drawing.Point(13, 457);
            this.nudPezziDisponibili.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.nudPezziDisponibili.Name = "nudPezziDisponibili";
            this.nudPezziDisponibili.Size = new System.Drawing.Size(232, 21);
            this.nudPezziDisponibili.TabIndex = 10;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.label11.Location = new System.Drawing.Point(11, 438);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(95, 16);
            this.label11.TabIndex = 26;
            this.label11.Text = "Pezzi disponibili*";
            // 
            // FrmInserisciProdotto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(258, 563);
            this.Controls.Add(this.nudPezziDisponibili);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.btAnnulla);
            this.Controls.Add(this.btSalva);
            this.Controls.Add(this.tbDescrizione);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.nudPrezzo);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.nudPeso);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.nudLunghezza);
            this.Controls.Add(this.nudAltezza);
            this.Controls.Add(this.nudProfondita);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cbCategoria);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbNome);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbPIVA);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label1);
            this.Name = "FrmInserisciProdotto";
            this.Text = "Inserisci Prodotto";
            ((System.ComponentModel.ISupportInitialize)(this.nudProfondita)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAltezza)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLunghezza)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPeso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrezzo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPezziDisponibili)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox tbPIVA;
        private System.Windows.Forms.TextBox tbNome;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbCategoria;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown nudProfondita;
        private System.Windows.Forms.NumericUpDown nudAltezza;
        private System.Windows.Forms.NumericUpDown nudLunghezza;
        private System.Windows.Forms.NumericUpDown nudPeso;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown nudPrezzo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbDescrizione;
        private System.Windows.Forms.Button btSalva;
        private System.Windows.Forms.Button btAnnulla;
        private System.Windows.Forms.NumericUpDown nudPezziDisponibili;
        private System.Windows.Forms.Label label11;
    }
}