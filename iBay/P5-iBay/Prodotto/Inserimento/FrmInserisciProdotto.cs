﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySqlConnector;

namespace P5_iBay
{
    // Parte di Sebastianelli Tomas
    public partial class FrmInserisciProdotto : Form
    {
        #region Variabili
        int _idProdottoDaModificare;
        MySqlConnection _connection = new MySqlConnection(Program.CONNECTION_STRING);
        #endregion

        #region Costruttore
        public FrmInserisciProdotto(int idProdottoDaModificare)
        {
            InitializeComponent();

            // Memorizzazione ID del prodotto da modificare (-1 se da inserire)
            _idProdottoDaModificare = idProdottoDaModificare;

            // Ottenimento prodotto da modificare dal DB e caricamento nei controlli grafici
            if (_idProdottoDaModificare != -1)
            {
                try
                {
                    _connection.Open();
                    MySqlCommand _query = new MySqlCommand($"SELECT * FROM prodotti WHERE ID = {_idProdottoDaModificare}", _connection);
                    MySqlDataReader reader = _query.ExecuteReader();
                    reader.Read();

                    tbPIVA.Text = (string)reader["PIVAAzienda"];
                    tbNome.Text = (string)reader["Nome"];
                    cbCategoria.SelectedIndex = Convert.ToInt32(reader["Categoria"]);
                    tbDescrizione.Text = (string)reader["Descrizione"];
                    nudLunghezza.Value = Convert.ToInt32(reader["Lunghezza"]);
                    nudAltezza.Value = Convert.ToInt32(reader["Altezza"]);
                    nudProfondita.Value = Convert.ToInt32(reader["Profondita"]);
                    nudPeso.Value = Convert.ToInt32(reader["Peso"]);
                    nudPezziDisponibili.Value = Convert.ToInt32(reader["Quantita"]);
                    nudPrezzo.Value = Convert.ToInt32(reader["Prezzo"]);

                    _connection.Close();
                }catch(Exception ex)
                {
                    MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
            }
        }
        #endregion

        #region Eventi
        private void btAnnulla_Click(object sender, EventArgs e)
        {
            // Richiesta di conferma per l'annullamento dell'inserimento
            DialogResult _dr = MessageBox.Show("Sei sicuro di voler annullare l'inserimento del prodotto? I dati inseriti non verranno salvati.", "Avviso", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

            // Se si conferma l'annullamento chiudi questa form
            if (_dr == DialogResult.Yes)
            {
                this.DialogResult = DialogResult.Abort;
                this.Close();
            } 
        }

        private void btSalva_Click(object sender, EventArgs e)
        {
            // Se è stato compilato tutto correttamente inserisci/modifica il prodotto nel DB
            if (!String.IsNullOrEmpty(tbNome.Text.Trim()) && cbCategoria.SelectedIndex != -1 && nudPrezzo.Value > 0 && nudPezziDisponibili.Value > 0)
            {
                // Controllo della lunghezza della Partita IVA
                if (tbPIVA.Text.Trim().Length == 0 || tbPIVA.Text.Trim().Length == 11)
                {
                    // Effettuo l'operazione solo se l'utente corrente è autorizzato
                    if (Program._venditore || Program._amministratore)
                    {
                        try
                        {
                            _connection.Open();

                            // Se l'ID del prodotto è -1 devo effettuare un inserimento
                            if (_idProdottoDaModificare == -1)
                            {
                                MySqlCommand _query = new MySqlCommand($"INSERT INTO prodotti (emailUtente, Nome, Categoria, Prezzo, Lunghezza, Altezza, Profondita, Peso, Quantita, Descrizione, PIVAAzienda) VALUES ('{Program._email}', '{tbNome.Text.Trim()}', {cbCategoria.SelectedIndex}, {nudPrezzo.Value}, {nudLunghezza.Value}, {nudAltezza.Value}, {nudProfondita.Value}, {nudPeso.Value}, {nudPezziDisponibili.Value}, '{tbDescrizione.Text.Trim()}', '{tbPIVA.Text.Trim()}')", _connection);
                                _query.ExecuteNonQuery();
                            }
                            // Altrimenti una modifica
                            else
                            {
                                MySqlCommand _query = new MySqlCommand($"UPDATE prodotti SET Nome = '{tbNome.Text.Trim()}', Categoria = {cbCategoria.SelectedIndex}, Prezzo = {nudPrezzo.Value}, Lunghezza = {nudLunghezza.Value}, Altezza = {nudAltezza.Value}, Profondita = {nudProfondita.Value}, Peso = {nudPeso.Value}, Quantita = {nudPezziDisponibili.Value}, Descrizione = '{tbDescrizione.Text.Trim()}', PIVAAzienda = '{tbPIVA.Text.Trim()}' WHERE ID = {_idProdottoDaModificare}", _connection);
                                _query.ExecuteNonQuery();
                            }

                            _connection.Close();
                        }catch(Exception ex)
                        {
                            MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            this.Close();
                        }

                        // Conferma di operazione avvenuta con successo
                        MessageBox.Show("Salvataggio effettuato con successo.", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.DialogResult = DialogResult.OK;
                        this.Close();
                    }
                    else
                        MessageBox.Show("L'e-mail inserita non è associata ad un venditore o amministratore.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                    MessageBox.Show("La partita IVA inserita non è formata da 11 cifre. Inserirne una valida e riprovare.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
                MessageBox.Show("Alcuni campi obbligatori non sono stati compilati. Verificare che tutti i campi contrassegnati con l'asterisco siano stati compilati prima di continuare.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        #endregion
    }
}
