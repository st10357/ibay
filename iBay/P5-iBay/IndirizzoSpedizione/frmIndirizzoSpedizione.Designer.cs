﻿namespace P5_iBay
{
    partial class frmIndirizzoSpedizione
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbNome = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbIstruzioniConsegna = new System.Windows.Forms.TextBox();
            this.tbCap = new System.Windows.Forms.TextBox();
            this.tbProvincia = new System.Windows.Forms.TextBox();
            this.tbCitta = new System.Windows.Forms.TextBox();
            this.tbIndirizzo = new System.Windows.Forms.TextBox();
            this.btAnnulla = new System.Windows.Forms.Button();
            this.btSalva = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.tbNazione = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cbTipoProprieta = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tbNome
            // 
            this.tbNome.Location = new System.Drawing.Point(18, 110);
            this.tbNome.Name = "tbNome";
            this.tbNome.Size = new System.Drawing.Size(304, 24);
            this.tbNome.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(270, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "COMPILARE TUTTI I CAMPI";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nome citofono";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(360, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(252, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "Istruzioni di consegna (facoltativo)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 273);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 18);
            this.label4.TabIndex = 5;
            this.label4.Text = "Provincia";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 209);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 18);
            this.label5.TabIndex = 6;
            this.label5.Text = "Città";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 399);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 18);
            this.label6.TabIndex = 7;
            this.label6.Text = "Tipo proprieta";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 337);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 18);
            this.label7.TabIndex = 8;
            this.label7.Text = "Cap";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 145);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 18);
            this.label8.TabIndex = 9;
            this.label8.Text = "Indirizzo";
            // 
            // tbIstruzioniConsegna
            // 
            this.tbIstruzioniConsegna.Location = new System.Drawing.Point(365, 110);
            this.tbIstruzioniConsegna.Multiline = true;
            this.tbIstruzioniConsegna.Name = "tbIstruzioniConsegna";
            this.tbIstruzioniConsegna.Size = new System.Drawing.Size(359, 140);
            this.tbIstruzioniConsegna.TabIndex = 8;
            // 
            // tbCap
            // 
            this.tbCap.Location = new System.Drawing.Point(20, 364);
            this.tbCap.MaxLength = 5;
            this.tbCap.Name = "tbCap";
            this.tbCap.Size = new System.Drawing.Size(304, 24);
            this.tbCap.TabIndex = 5;
            // 
            // tbProvincia
            // 
            this.tbProvincia.Location = new System.Drawing.Point(20, 302);
            this.tbProvincia.MaxLength = 2;
            this.tbProvincia.Name = "tbProvincia";
            this.tbProvincia.Size = new System.Drawing.Size(304, 24);
            this.tbProvincia.TabIndex = 4;
            // 
            // tbCitta
            // 
            this.tbCitta.Location = new System.Drawing.Point(20, 238);
            this.tbCitta.Name = "tbCitta";
            this.tbCitta.Size = new System.Drawing.Size(304, 24);
            this.tbCitta.TabIndex = 3;
            // 
            // tbIndirizzo
            // 
            this.tbIndirizzo.Location = new System.Drawing.Point(20, 174);
            this.tbIndirizzo.Name = "tbIndirizzo";
            this.tbIndirizzo.Size = new System.Drawing.Size(304, 24);
            this.tbIndirizzo.TabIndex = 2;
            // 
            // btAnnulla
            // 
            this.btAnnulla.BackColor = System.Drawing.SystemColors.Control;
            this.btAnnulla.FlatAppearance.BorderSize = 0;
            this.btAnnulla.ForeColor = System.Drawing.Color.Black;
            this.btAnnulla.Location = new System.Drawing.Point(365, 256);
            this.btAnnulla.Name = "btAnnulla";
            this.btAnnulla.Size = new System.Drawing.Size(163, 80);
            this.btAnnulla.TabIndex = 18;
            this.btAnnulla.Text = "ANNULLA";
            this.btAnnulla.UseVisualStyleBackColor = false;
            this.btAnnulla.Click += new System.EventHandler(this.btAnnulla_Click);
            // 
            // btSalva
            // 
            this.btSalva.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(61)))), ((int)(((byte)(124)))));
            this.btSalva.FlatAppearance.BorderSize = 0;
            this.btSalva.ForeColor = System.Drawing.Color.White;
            this.btSalva.Location = new System.Drawing.Point(561, 256);
            this.btSalva.Name = "btSalva";
            this.btSalva.Size = new System.Drawing.Size(163, 80);
            this.btSalva.TabIndex = 19;
            this.btSalva.Text = "SALVA";
            this.btSalva.UseVisualStyleBackColor = false;
            this.btSalva.Click += new System.EventHandler(this.btSalva_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 465);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 18);
            this.label10.TabIndex = 21;
            this.label10.Text = "Nazione";
            // 
            // tbNazione
            // 
            this.tbNazione.Location = new System.Drawing.Point(22, 494);
            this.tbNazione.Name = "tbNazione";
            this.tbNazione.Size = new System.Drawing.Size(304, 24);
            this.tbNazione.TabIndex = 22;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::P5_iBay.Properties.Resources.lineaOrizzontale;
            this.pictureBox1.Location = new System.Drawing.Point(18, 47);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(706, 26);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // cbTipoProprieta
            // 
            this.cbTipoProprieta.FormattingEnabled = true;
            this.cbTipoProprieta.Items.AddRange(new object[] {
            "Casa",
            "Appartamento",
            "Azienda"});
            this.cbTipoProprieta.Location = new System.Drawing.Point(20, 429);
            this.cbTipoProprieta.Name = "cbTipoProprieta";
            this.cbTipoProprieta.Size = new System.Drawing.Size(302, 24);
            this.cbTipoProprieta.TabIndex = 23;
            // 
            // frmIndirizzoSpedizione
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(749, 561);
            this.Controls.Add(this.cbTipoProprieta);
            this.Controls.Add(this.tbNazione);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btSalva);
            this.Controls.Add(this.btAnnulla);
            this.Controls.Add(this.tbIndirizzo);
            this.Controls.Add(this.tbCitta);
            this.Controls.Add(this.tbProvincia);
            this.Controls.Add(this.tbCap);
            this.Controls.Add(this.tbIstruzioniConsegna);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbNome);
            this.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(765, 600);
            this.MinimumSize = new System.Drawing.Size(765, 600);
            this.Name = "frmIndirizzoSpedizione";
            this.Text = "AGGIUNGI INDIRIZZO";
            this.Load += new System.EventHandler(this.frmIndirizzoSpedizione_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbNome;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbIstruzioniConsegna;
        private System.Windows.Forms.TextBox tbCap;
        private System.Windows.Forms.TextBox tbProvincia;
        private System.Windows.Forms.TextBox tbCitta;
        private System.Windows.Forms.TextBox tbIndirizzo;
        private System.Windows.Forms.Button btAnnulla;
        private System.Windows.Forms.Button btSalva;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbNazione;
        private System.Windows.Forms.ComboBox cbTipoProprieta;
    }
}