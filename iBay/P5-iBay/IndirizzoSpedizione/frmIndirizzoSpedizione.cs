﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySqlConnector;

namespace P5_iBay
{
    // Parte di Dello Preite Maurizio
    public partial class frmIndirizzoSpedizione : Form
    {
        // Variabili
        string _tipoForm;
        int _lvTag;

        public frmIndirizzoSpedizione(string tipoForm, int lvTag)
        {
            InitializeComponent();

            Program.roudedEdges(btSalva);
            Program.roudedEdges(btAnnulla);

            _tipoForm = tipoForm;
            _lvTag = lvTag;

            if (tipoForm == "UPDATE")
            {
                popolaCampi();
                btSalva.Text = "MODIFICA";
            }

        }

        private void frmIndirizzoSpedizione_Load(object sender, EventArgs e)
        {
            cbTipoProprieta.SelectedIndex = 0;
        }

        #region EVENTI
        private void btSalva_Click(object sender, EventArgs e)
        {
            if (tbNome.Text != "" && tbIndirizzo.Text != "" && tbCitta.Text != "" && tbProvincia.Text != "" && tbCap.Text != "")
            {
                AggiornaDB(_tipoForm, _lvTag);
                Close();
            }
            else
                MessageBox.Show("Compilare tutti i campi!");
        }

        private void btAnnulla_Click(object sender, EventArgs e)
        {
            Close();
        }
        #endregion

        #region METODI
        public void AggiornaDB(string tipoForm, int _idLv)
        {
            // Effettuo la connessione al DB
            MySqlConnection _connection = new MySqlConnection(Program.CONNECTION_STRING);

            try
            {
                _connection.Open();

                if (tipoForm == "INSERT")
                {
                    MySqlCommand _query = new MySqlCommand($"INSERT INTO indirizzispedizione (emailUtente, Indirizzo, Città, Nome, CAP, Provincia, Nazione, IstruzioniConsegna, TipoProprieta) VALUES ('{Program._email}', '{tbIndirizzo.Text}', '{tbCitta.Text}', '{tbNome.Text}', '{tbCap.Text}', '{tbProvincia.Text}', '{tbNazione.Text}', '{tbIstruzioniConsegna.Text}', '{cbTipoProprieta.SelectedIndex}')", _connection);
                    _query.ExecuteNonQuery();
                }
                else if (tipoForm == "UPDATE")
                {
                    MySqlCommand _query = new MySqlCommand($"UPDATE indirizzispedizione SET emailUtente = '{Program._email}', Indirizzo = '{tbIndirizzo.Text}', Nome = '{tbNome.Text}', CAP = '{tbCap.Text}', Città = '{tbCitta.Text}', Provincia = '{tbProvincia.Text}', Nazione = '{tbNazione.Text}', IstruzioniConsegna = '{tbIstruzioniConsegna.Text}', TipoProprieta = '{cbTipoProprieta.SelectedIndex}' WHERE ID = {_idLv}", _connection);
                    _query.ExecuteNonQuery();
                }

                _connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString() + "Errore", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void popolaCampi()
        {
            tbNome.Text = Program.rigaSelezionata.Nome;
            tbIndirizzo.Text = Program.rigaSelezionata.Indirizzo;
            tbCitta.Text = Program.rigaSelezionata.Città;
            tbProvincia.Text = Program.rigaSelezionata.Provincia;
            tbCap.Text = Program.rigaSelezionata.CAP;
            cbTipoProprieta.SelectedIndex = Program.rigaSelezionata.TipoProprieta;  // TODO: Capire perché seleziona ma non mostra
            tbNazione.Text = Program.rigaSelezionata.Nazione;
            tbIstruzioniConsegna.Text = Program.rigaSelezionata.IstruzioniConsegna;
        }
        #endregion


    }
}
