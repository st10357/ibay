﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySqlConnector;

namespace P5_iBay
{
    // Parte di Dello Preite Maurizio
    public partial class frmIndirizziSpedizione : Form
    {
        public frmIndirizziSpedizione()
        {
            InitializeComponent();

            // Applica lo stile arrotondato al vari pulsanti
            Program.roudedEdges(btSpedisci);
            Program.roudedEdges(btInserisciIndirizzo);
            Program.roudedEdges(btRimuovi);
        }

        private void frmIndirizziSpedizione_Load(object sender, EventArgs e)
        {
            CaricaIndirizzi();
        }


        #region Variabili
        string tipoForm = "INSERT";
        #endregion


        #region Metodi
        private void CaricaIndirizzi()
        {
            // Effettuo la connessione al DB
            MySqlConnection _connection = new MySqlConnection(Program.CONNECTION_STRING);

            try
            {
                _connection.Open();
                MySqlCommand _query = new MySqlCommand($"SELECT * FROM indirizzispedizione", _connection);
                MySqlDataReader _reader = _query.ExecuteReader();

                bool lettura = false;
                int i = 0;

                //popolamento listView
                lvIndirizzi.Items.Clear();
                while (_reader.Read())
                {
                    i++;
                    lettura = true;
                    ListViewItem lv = new ListViewItem();
                    lv.Text = _reader["Nome"] as string;


                    lv.SubItems.Add((string)_reader["Indirizzo"]);
                    lv.SubItems.Add(Convert.ToString(_reader["Città"]));
                    lv.Tag = _reader["ID"];
                    lvIndirizzi.Items.Add(lv);

                }

                //auto-allineare le colonne della listView
                columnHeader1.AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
                columnHeader2.AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
                columnHeader3.AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);

                _connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        /// <summary>
        /// In base all'elemento selezionato nella lv, acquisisco l'intera riga dal db
        /// </summary>
        private void AcquisisciRiga()
        {
            int id;

            // Effettuo la connessione al DB
            MySqlConnection _connection = new MySqlConnection(Program.CONNECTION_STRING);

            try
            {
                _connection.Open();
                MySqlCommand _query = new MySqlCommand($"SELECT * FROM indirizzispedizione", _connection);
                MySqlDataReader _reader = _query.ExecuteReader();

                while (_reader.Read())
                {
                    id = Convert.ToInt16(_reader["ID"]);

                    if (lvIndirizzi.SelectedItems.Count > 0)
                    {
                        if (id == (int)lvIndirizzi.SelectedItems[0].Tag)
                        {
                            Program.rigaSelezionata.Indirizzo = _reader["Indirizzo"] as string;
                            Program.rigaSelezionata.Città = _reader["Città"] as string;
                            Program.rigaSelezionata.Nome = _reader["Nome"] as string;
                            Program.rigaSelezionata.CAP = _reader["CAP"] as string;
                            Program.rigaSelezionata.Provincia = _reader["Provincia"] as string;
                            Program.rigaSelezionata.Nazione = _reader["Nazione"] as string;
                            Program.rigaSelezionata.IstruzioniConsegna = _reader["IstruzioniConsegna"] as string;
                            Program.rigaSelezionata.TipoProprieta = Convert.ToInt16(_reader["TipoProprieta"]);
                        }
                    }
                }
                _connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        private void RimuoviRiga()
        {
            // Effettuo la connessione al DB
            MySqlConnection _connection = new MySqlConnection(Program.CONNECTION_STRING);

            try
            {
                if (lvIndirizzi.SelectedItems.Count > 0)
                {
                    _connection.Open();
                    MySqlCommand _query = new MySqlCommand($"DELETE FROM indirizzispedizione WHERE ID = {(int)lvIndirizzi.SelectedItems[0].Tag}", _connection);
                    MySqlDataReader _reader = _query.ExecuteReader();

                    _connection.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        #endregion


        #region Eventi

        //quando clicco un elemento della lv
        private void lvIndirizzi_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvIndirizzi.SelectedItems.Count > 0)
            {
                tipoForm = "UPDATE";
                btInserisciIndirizzo.Text = "Modifica indirizzo";

                AcquisisciRiga();
            }
            else
            {
                tipoForm = "INSERT";
                btInserisciIndirizzo.Text = "Inserisci un nuovo indirizzo";
            }
        }

        private void btSpedisci_Click(object sender, EventArgs e)
        {
            // Se è stato selezionato un solo indirizzo di spedizione memorizzalo e chiudi questa form
            if (lvIndirizzi.SelectedIndices.Count == 1)
            {
                Program.idIndirizzoSelezionato = (int)lvIndirizzi.SelectedItems[0].Tag;
                this.Close();
            }
            else
                MessageBox.Show("Nessun indirizzo di spedizione selezionato, selezionarne prima uno per continuare.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void frmIndirizziSpedizione_Click(object sender, EventArgs e)
        {
            tipoForm = "INSERT";
            btInserisciIndirizzo.Text = "Inserisci un nuovo indirizzo";
        }

        private void btInserisciIndirizzo_Click(object sender, EventArgs e)
        {
            if (lvIndirizzi.SelectedIndices.Count == 1)
            {
                if (tipoForm == "INSERT")
                {
                    frmIndirizzoSpedizione indirizzoSpedizione = new frmIndirizzoSpedizione(tipoForm, 0);
                    indirizzoSpedizione.ShowDialog();
                }
                else
                {
                    frmIndirizzoSpedizione indirizzoSpedizione = new frmIndirizzoSpedizione(tipoForm, (int)lvIndirizzi.SelectedItems[0].Tag);
                    indirizzoSpedizione.ShowDialog();
                }
                CaricaIndirizzi();
            }
            else
            {
                frmIndirizzoSpedizione indirizzoSpedizione = new frmIndirizzoSpedizione(tipoForm, 0);
                indirizzoSpedizione.ShowDialog();
            }
            
        }

        private void btRimuovi_Click(object sender, EventArgs e)
        {
            RimuoviRiga();
            CaricaIndirizzi();
        }

        #endregion

    }
}
