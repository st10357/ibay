﻿namespace P5_iBay
{
    partial class frmIndirizziSpedizione
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btSpedisci = new System.Windows.Forms.Button();
            this.lvIndirizzi = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btRimuovi = new System.Windows.Forms.Button();
            this.btInserisciIndirizzo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btSpedisci
            // 
            this.btSpedisci.BackColor = System.Drawing.Color.Yellow;
            this.btSpedisci.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btSpedisci.FlatAppearance.BorderSize = 0;
            this.btSpedisci.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSpedisci.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSpedisci.Location = new System.Drawing.Point(13, 264);
            this.btSpedisci.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btSpedisci.Name = "btSpedisci";
            this.btSpedisci.Size = new System.Drawing.Size(279, 42);
            this.btSpedisci.TabIndex = 15;
            this.btSpedisci.Text = "Spedisci a questo indirizzo";
            this.btSpedisci.UseVisualStyleBackColor = false;
            this.btSpedisci.Click += new System.EventHandler(this.btSpedisci_Click);
            // 
            // lvIndirizzi
            // 
            this.lvIndirizzi.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lvIndirizzi.FullRowSelect = true;
            this.lvIndirizzi.HideSelection = false;
            this.lvIndirizzi.Location = new System.Drawing.Point(12, 12);
            this.lvIndirizzi.MultiSelect = false;
            this.lvIndirizzi.Name = "lvIndirizzi";
            this.lvIndirizzi.Size = new System.Drawing.Size(907, 236);
            this.lvIndirizzi.TabIndex = 16;
            this.lvIndirizzi.UseCompatibleStateImageBehavior = false;
            this.lvIndirizzi.View = System.Windows.Forms.View.Details;
            this.lvIndirizzi.SelectedIndexChanged += new System.EventHandler(this.lvIndirizzi_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "NOME CITOFONO";
            this.columnHeader1.Width = 147;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "INDIRIZZO";
            this.columnHeader2.Width = 142;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "CITTA";
            this.columnHeader3.Width = 133;
            // 
            // btRimuovi
            // 
            this.btRimuovi.BackColor = System.Drawing.Color.Yellow;
            this.btRimuovi.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btRimuovi.FlatAppearance.BorderSize = 0;
            this.btRimuovi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btRimuovi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btRimuovi.Location = new System.Drawing.Point(639, 264);
            this.btRimuovi.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btRimuovi.Name = "btRimuovi";
            this.btRimuovi.Size = new System.Drawing.Size(279, 42);
            this.btRimuovi.TabIndex = 17;
            this.btRimuovi.Text = "Rimuovi indirizzo";
            this.btRimuovi.UseVisualStyleBackColor = false;
            this.btRimuovi.Click += new System.EventHandler(this.btRimuovi_Click);
            // 
            // btInserisciIndirizzo
            // 
            this.btInserisciIndirizzo.BackColor = System.Drawing.Color.Yellow;
            this.btInserisciIndirizzo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btInserisciIndirizzo.FlatAppearance.BorderSize = 0;
            this.btInserisciIndirizzo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btInserisciIndirizzo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btInserisciIndirizzo.Location = new System.Drawing.Point(326, 264);
            this.btInserisciIndirizzo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btInserisciIndirizzo.Name = "btInserisciIndirizzo";
            this.btInserisciIndirizzo.Size = new System.Drawing.Size(279, 42);
            this.btInserisciIndirizzo.TabIndex = 18;
            this.btInserisciIndirizzo.Text = "Inserisci un nuovo indirizzo";
            this.btInserisciIndirizzo.UseVisualStyleBackColor = false;
            this.btInserisciIndirizzo.Click += new System.EventHandler(this.btInserisciIndirizzo_Click);
            // 
            // frmIndirizziSpedizione
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(931, 332);
            this.Controls.Add(this.btInserisciIndirizzo);
            this.Controls.Add(this.btRimuovi);
            this.Controls.Add(this.lvIndirizzi);
            this.Controls.Add(this.btSpedisci);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(953, 388);
            this.MinimumSize = new System.Drawing.Size(953, 388);
            this.Name = "frmIndirizziSpedizione";
            this.Text = "INDIRIZZI SALVATI";
            this.Load += new System.EventHandler(this.frmIndirizziSpedizione_Load);
            this.Click += new System.EventHandler(this.frmIndirizziSpedizione_Click);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btSpedisci;
        private System.Windows.Forms.ListView lvIndirizzi;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button btRimuovi;
        private System.Windows.Forms.Button btInserisciIndirizzo;
    }
}