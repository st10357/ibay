﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySqlConnector;

namespace P5_iBay
{
    // Parte di Sebastianelli Tomas
    public partial class FrmDetailsContoCorrente : Form
    {
        #region Variabili
        string _IBANContoDaModificare;
        MySqlConnection _connection = new MySqlConnection(Program.CONNECTION_STRING);
        #endregion

        #region Costruttore
        public FrmDetailsContoCorrente(string IBANContoDaModificare)
        {
            InitializeComponent();

            // Memorizzo l'IBAN del conto da modificare (null se devo effettuare un inserimento)
            _IBANContoDaModificare = IBANContoDaModificare;

            // Ottenimento conto da modificare dal DB
            if (!String.IsNullOrEmpty(_IBANContoDaModificare))
            {
                try
                {
                    _connection.Open();
                    MySqlCommand _query = new MySqlCommand($"SELECT * FROM conticorrente WHERE IBAN = '{_IBANContoDaModificare}'", _connection);
                    MySqlDataReader _reader = _query.ExecuteReader();
                    _reader.Read();

                    tbBIC.Text = (string)_reader["BIC"];
                    tbIBAN.Text = (string)_reader["IBAN"];
                    tbNomeIntestatario.Text = (string)_reader["NomeIntestatario"];
                    tbCognomeIntestatario.Text = (string)_reader["CognomeIntestatario"];
                    tbNomePersonalizzato.Text = (string)_reader["NomePersonalizzato"];

                    _connection.Close();
                }catch (Exception ex)
                {
                    MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
            }
        }
        #endregion

        #region Eventi
        private void btSalva_Click(object sender, EventArgs e)
        {
            // Se è stato compilato tutto correttamente inserisci/modifica il conto nel DB
            if ((tbBIC.Text.Length >= 8 && tbBIC.Text.Length <= 11) && tbIBAN.Text.Length == 27 && !String.IsNullOrEmpty(tbNomeIntestatario.Text) && !String.IsNullOrEmpty(tbCognomeIntestatario.Text) && !String.IsNullOrEmpty(tbNomePersonalizzato.Text))
            {
                try
                {
                    _connection.Open();

                    // Se il codice della carta da modificare è vuoto devo effettuare una modifica
                    if (_IBANContoDaModificare != null)
                    {
                        MySqlCommand _query = new MySqlCommand($"UPDATE conticorrente SET emailUtente = '{Program._email}', BIC = '{tbBIC.Text}', IBAN = '{tbIBAN.Text}', NomeIntestatario = '{tbNomeIntestatario.Text}', CognomeIntestatario = '{tbCognomeIntestatario.Text}', NomePersonalizzato = '{tbNomePersonalizzato.Text}' WHERE IBAN = '{_IBANContoDaModificare}';", _connection);
                        _query.ExecuteNonQuery();
                    }
                    // Altrimenti un inserimento
                    else
                    {
                        MySqlCommand _query = new MySqlCommand($"INSERT INTO conticorrente (emailUtente, BIC, IBAN, NomeIntestatario, CognomeIntestatario, NomePersonalizzato) VALUES ('{Program._email}', '{tbBIC.Text}', '{tbIBAN.Text}', '{tbNomeIntestatario.Text}', '{tbCognomeIntestatario.Text}', '{tbNomePersonalizzato.Text}')", _connection);
                        _query.ExecuteNonQuery();
                    }

                    _connection.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }

                // Conferma di operazione avvenuta con successo
                MessageBox.Show("Salvataggio effettuato con successo.", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
                MessageBox.Show("Alcuni campi non sono stati compilati. Verificare che tutti i campi siano stati compilati prima di continuare.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btAnnulla_Click(object sender, EventArgs e)
        {
            // Richiesta di conferma per l'annullamento dell'inserimento
            DialogResult _dr = MessageBox.Show("Sei sicuro di voler annullare l'inserimento del conto corrente? I dati inseriti non verranno salvati.", "Avviso", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

            // Se si conferma l'annullamento chiudi questa form
            if (_dr == DialogResult.Yes)
            {
                this.DialogResult = DialogResult.Abort;
                this.Close();
            }
        }
        #endregion
    }
}
