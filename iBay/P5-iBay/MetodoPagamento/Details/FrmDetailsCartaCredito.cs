﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySqlConnector;

namespace P5_iBay
{
    // Parte di Sebastianelli Tomas
    public partial class FrmDetailsCartaCredito : Form
    {
        #region Variabili
        string _codiceCartaDaModificare;
        MySqlConnection _connection = new MySqlConnection(Program.CONNECTION_STRING);
        #endregion

        #region Costruttore
        public FrmDetailsCartaCredito(string codiceCartaDaModificare)
        {
            InitializeComponent();

            // Memorizzazione codice carta da modificare (null se da inserire)
            _codiceCartaDaModificare = codiceCartaDaModificare;

            // Ottenimento carta da modificare dal DB e caricamento nei controlli grafici
            if(!String.IsNullOrEmpty(_codiceCartaDaModificare))
            {
                try
                {
                    _connection.Open();
                    MySqlCommand _query = new MySqlCommand($"SELECT * FROM cartedicredito WHERE CodiceCarta = '{_codiceCartaDaModificare}'", _connection);
                    MySqlDataReader _reader = _query.ExecuteReader();
                    _reader.Read();

                    tbCodiceCarta.Text = (string)_reader["CodiceCarta"];
                    tbNomeIntestatario.Text = (string)_reader["NomeIntestatario"];
                    tbCognomeIntestatario.Text = (string)_reader["CognomeIntestatario"];
                    tbCVV.Text = (string)_reader["CVV"];
                    nudMeseScadenza.Value = Convert.ToInt32(_reader["MeseScadenza"]);
                    nudAnnoScadenza.Value = Convert.ToInt32(_reader["AnnoScadenza"]);
                    tbNomePersonalizzato.Text = (string)_reader["NomePersonalizzato"];

                    _connection.Close();
                }catch(Exception ex)
                {
                    MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
            }
        }
        #endregion

        #region Eventi
        private void btSalva_Click(object sender, EventArgs e)
        {
            // Se è stato compilato tutto correttamente inserisci/modifica la carta nel DB
            if (tbCodiceCarta.Text.Length == 16 && !String.IsNullOrEmpty(tbNomeIntestatario.Text) && !String.IsNullOrEmpty(tbCognomeIntestatario.Text) && (tbCVV.Text.Length == 3 || tbCVV.Text.Length == 4) && nudAnnoScadenza.Value >= Convert.ToInt32(DateTime.Now.Year.ToString()) && !String.IsNullOrEmpty(tbNomePersonalizzato.Text))
            {
                try
                {
                    _connection.Open();

                    // Se il codice della carta da modificare è vuoto devo effettuare una modifica
                    if (_codiceCartaDaModificare != null)
                    {
                        MySqlCommand _query = new MySqlCommand($"UPDATE cartedicredito SET CodiceCarta = '{tbCodiceCarta.Text}', NomeIntestatario = '{tbNomeIntestatario.Text}', CognomeIntestatario = '{tbCognomeIntestatario.Text}', CVV = '{tbCVV.Text}', MeseScadenza = {nudMeseScadenza.Value}, AnnoScadenza = {nudAnnoScadenza.Value}, NomePersonalizzato = '{tbNomePersonalizzato.Text}' WHERE CodiceCarta = '{_codiceCartaDaModificare}'", _connection);
                        _query.ExecuteNonQuery();
                    }
                    // Altrimenti un inserimento
                    else
                    {
                        MySqlCommand _query = new MySqlCommand($"INSERT INTO cartedicredito (CodiceCarta, emailUtente, NomeIntestatario, CognomeIntestatario, CVV, MeseScadenza, AnnoScadenza, NomePersonalizzato) VALUES ('{tbCodiceCarta.Text}', '{Program._email}', '{tbNomeIntestatario.Text}', '{tbCognomeIntestatario.Text}', '{tbCVV.Text}', {nudMeseScadenza.Value}, {nudAnnoScadenza.Value}, '{tbNomePersonalizzato.Text}')", _connection);
                        _query.ExecuteNonQuery();
                    }

                    _connection.Close();
                }catch(Exception ex)
                {
                    MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
                

                // Conferma di operazione avvenuta con successo
                MessageBox.Show("Salvataggio effettuato con successo.", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
                MessageBox.Show("Alcuni campi non sono stati compilati. Verificare che tutti i campi siano stati compilati prima di continuare.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btAnnulla_Click(object sender, EventArgs e)
        {
            // Richiesta di conferma per l'annullamento dell'inserimento
            DialogResult _dr = MessageBox.Show("Sei sicuro di voler annullare l'inserimento della carta di credito? I dati inseriti non verranno salvati.", "Avviso", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

            // Se si conferma l'annullamento chiudi questa form
            if (_dr == DialogResult.Yes)
            {
                this.DialogResult = DialogResult.Abort;
                this.Close();
            }
        }
        #endregion
    }
}
