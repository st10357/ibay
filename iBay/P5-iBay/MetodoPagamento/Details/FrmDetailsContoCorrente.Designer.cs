﻿namespace P5_iBay
{
    partial class FrmDetailsContoCorrente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btSalva = new System.Windows.Forms.Button();
            this.btAnnulla = new System.Windows.Forms.Button();
            this.tbNomePersonalizzato = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbCognomeIntestatario = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbNomeIntestatario = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbIBAN = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbBIC = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btSalva
            // 
            this.btSalva.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(61)))), ((int)(((byte)(124)))));
            this.btSalva.ForeColor = System.Drawing.Color.White;
            this.btSalva.Location = new System.Drawing.Point(127, 274);
            this.btSalva.Name = "btSalva";
            this.btSalva.Size = new System.Drawing.Size(100, 23);
            this.btSalva.TabIndex = 31;
            this.btSalva.Text = "Salva";
            this.btSalva.UseVisualStyleBackColor = false;
            this.btSalva.Click += new System.EventHandler(this.btSalva_Click);
            // 
            // btAnnulla
            // 
            this.btAnnulla.Location = new System.Drawing.Point(13, 274);
            this.btAnnulla.Name = "btAnnulla";
            this.btAnnulla.Size = new System.Drawing.Size(100, 23);
            this.btAnnulla.TabIndex = 30;
            this.btAnnulla.Text = "Annulla";
            this.btAnnulla.UseVisualStyleBackColor = true;
            this.btAnnulla.Click += new System.EventHandler(this.btAnnulla_Click);
            // 
            // tbNomePersonalizzato
            // 
            this.tbNomePersonalizzato.Location = new System.Drawing.Point(13, 248);
            this.tbNomePersonalizzato.MaxLength = 30;
            this.tbNomePersonalizzato.Name = "tbNomePersonalizzato";
            this.tbNomePersonalizzato.Size = new System.Drawing.Size(214, 20);
            this.tbNomePersonalizzato.TabIndex = 29;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(10, 229);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(123, 16);
            this.label6.TabIndex = 28;
            this.label6.Text = "Nome personalizzato:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 187);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 16);
            this.label4.TabIndex = 24;
            this.label4.Text = "Cognome intestatario:";
            // 
            // tbCognomeIntestatario
            // 
            this.tbCognomeIntestatario.Location = new System.Drawing.Point(13, 206);
            this.tbCognomeIntestatario.MaxLength = 50;
            this.tbCognomeIntestatario.Name = "tbCognomeIntestatario";
            this.tbCognomeIntestatario.Size = new System.Drawing.Size(214, 20);
            this.tbCognomeIntestatario.TabIndex = 23;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 16);
            this.label3.TabIndex = 22;
            this.label3.Text = "Nome intestatario:";
            // 
            // tbNomeIntestatario
            // 
            this.tbNomeIntestatario.Location = new System.Drawing.Point(13, 164);
            this.tbNomeIntestatario.MaxLength = 50;
            this.tbNomeIntestatario.Name = "tbNomeIntestatario";
            this.tbNomeIntestatario.Size = new System.Drawing.Size(214, 20);
            this.tbNomeIntestatario.TabIndex = 21;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 16);
            this.label2.TabIndex = 20;
            this.label2.Text = "IBAN:";
            // 
            // tbIBAN
            // 
            this.tbIBAN.Location = new System.Drawing.Point(13, 122);
            this.tbIBAN.MaxLength = 27;
            this.tbIBAN.Name = "tbIBAN";
            this.tbIBAN.Size = new System.Drawing.Size(214, 20);
            this.tbIBAN.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 16);
            this.label1.TabIndex = 18;
            this.label1.Text = "BIC:";
            // 
            // tbBIC
            // 
            this.tbBIC.Location = new System.Drawing.Point(13, 80);
            this.tbBIC.MaxLength = 11;
            this.tbBIC.Name = "tbBIC";
            this.tbBIC.Size = new System.Drawing.Size(214, 20);
            this.tbBIC.TabIndex = 17;
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(13, 12);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(214, 34);
            this.textBox1.TabIndex = 16;
            this.textBox1.Text = "Compili i seguenti campi con le informazioni del suo conto corrente.";
            // 
            // FrmInserimentoContoCorrente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(237, 308);
            this.Controls.Add(this.btSalva);
            this.Controls.Add(this.btAnnulla);
            this.Controls.Add(this.tbNomePersonalizzato);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbCognomeIntestatario);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbNomeIntestatario);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbIBAN);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbBIC);
            this.Controls.Add(this.textBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FrmInserimentoContoCorrente";
            this.Text = "Details conto corrente";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btSalva;
        private System.Windows.Forms.Button btAnnulla;
        private System.Windows.Forms.TextBox tbNomePersonalizzato;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbCognomeIntestatario;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbNomeIntestatario;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbIBAN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbBIC;
        private System.Windows.Forms.TextBox textBox1;
    }
}