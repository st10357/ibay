﻿namespace P5_iBay
{
    partial class FrmDetailsCartaCredito
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tbCodiceCarta = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbNomeIntestatario = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbCognomeIntestatario = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbCVV = new System.Windows.Forms.TextBox();
            this.nudMeseScadenza = new System.Windows.Forms.NumericUpDown();
            this.nudAnnoScadenza = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbNomePersonalizzato = new System.Windows.Forms.TextBox();
            this.btAnnulla = new System.Windows.Forms.Button();
            this.btSalva = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nudMeseScadenza)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAnnoScadenza)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(14, 13);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(214, 34);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "Compili i seguenti campi con le informazioni della sua carta di credito.";
            // 
            // tbCodiceCarta
            // 
            this.tbCodiceCarta.Location = new System.Drawing.Point(14, 81);
            this.tbCodiceCarta.MaxLength = 16;
            this.tbCodiceCarta.Name = "tbCodiceCarta";
            this.tbCodiceCarta.Size = new System.Drawing.Size(214, 20);
            this.tbCodiceCarta.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Codice carta:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Nome intestatario:";
            // 
            // tbNomeIntestatario
            // 
            this.tbNomeIntestatario.Location = new System.Drawing.Point(14, 123);
            this.tbNomeIntestatario.MaxLength = 50;
            this.tbNomeIntestatario.Name = "tbNomeIntestatario";
            this.tbNomeIntestatario.Size = new System.Drawing.Size(214, 20);
            this.tbNomeIntestatario.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Cognome intestatario:";
            // 
            // tbCognomeIntestatario
            // 
            this.tbCognomeIntestatario.Location = new System.Drawing.Point(14, 165);
            this.tbCognomeIntestatario.MaxLength = 50;
            this.tbCognomeIntestatario.Name = "tbCognomeIntestatario";
            this.tbCognomeIntestatario.Size = new System.Drawing.Size(214, 20);
            this.tbCognomeIntestatario.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(11, 188);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "CVV:";
            // 
            // tbCVV
            // 
            this.tbCVV.Location = new System.Drawing.Point(14, 207);
            this.tbCVV.MaxLength = 4;
            this.tbCVV.Name = "tbCVV";
            this.tbCVV.Size = new System.Drawing.Size(214, 20);
            this.tbCVV.TabIndex = 7;
            // 
            // nudMeseScadenza
            // 
            this.nudMeseScadenza.Location = new System.Drawing.Point(14, 249);
            this.nudMeseScadenza.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.nudMeseScadenza.Name = "nudMeseScadenza";
            this.nudMeseScadenza.Size = new System.Drawing.Size(92, 20);
            this.nudMeseScadenza.TabIndex = 9;
            // 
            // nudAnnoScadenza
            // 
            this.nudAnnoScadenza.Location = new System.Drawing.Point(136, 249);
            this.nudAnnoScadenza.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nudAnnoScadenza.Name = "nudAnnoScadenza";
            this.nudAnnoScadenza.Size = new System.Drawing.Size(92, 20);
            this.nudAnnoScadenza.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(11, 230);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(131, 16);
            this.label5.TabIndex = 11;
            this.label5.Text = "Mese/anno scadenza:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(11, 272);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(123, 16);
            this.label6.TabIndex = 12;
            this.label6.Text = "Nome personalizzato:";
            // 
            // tbNomePersonalizzato
            // 
            this.tbNomePersonalizzato.Location = new System.Drawing.Point(14, 291);
            this.tbNomePersonalizzato.MaxLength = 30;
            this.tbNomePersonalizzato.Name = "tbNomePersonalizzato";
            this.tbNomePersonalizzato.Size = new System.Drawing.Size(214, 20);
            this.tbNomePersonalizzato.TabIndex = 13;
            // 
            // btAnnulla
            // 
            this.btAnnulla.Location = new System.Drawing.Point(14, 317);
            this.btAnnulla.Name = "btAnnulla";
            this.btAnnulla.Size = new System.Drawing.Size(100, 23);
            this.btAnnulla.TabIndex = 14;
            this.btAnnulla.Text = "Annulla";
            this.btAnnulla.UseVisualStyleBackColor = true;
            this.btAnnulla.Click += new System.EventHandler(this.btAnnulla_Click);
            // 
            // btSalva
            // 
            this.btSalva.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(61)))), ((int)(((byte)(124)))));
            this.btSalva.ForeColor = System.Drawing.Color.White;
            this.btSalva.Location = new System.Drawing.Point(128, 317);
            this.btSalva.Name = "btSalva";
            this.btSalva.Size = new System.Drawing.Size(100, 23);
            this.btSalva.TabIndex = 15;
            this.btSalva.Text = "Salva";
            this.btSalva.UseVisualStyleBackColor = false;
            this.btSalva.Click += new System.EventHandler(this.btSalva_Click);
            // 
            // FrmInserimentoCartaCredito
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(238, 352);
            this.Controls.Add(this.btSalva);
            this.Controls.Add(this.btAnnulla);
            this.Controls.Add(this.tbNomePersonalizzato);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.nudAnnoScadenza);
            this.Controls.Add(this.nudMeseScadenza);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbCVV);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbCognomeIntestatario);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbNomeIntestatario);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbCodiceCarta);
            this.Controls.Add(this.textBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FrmInserimentoCartaCredito";
            this.Text = "Inserimento carta di credito";
            ((System.ComponentModel.ISupportInitialize)(this.nudMeseScadenza)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAnnoScadenza)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox tbCodiceCarta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbNomeIntestatario;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbCognomeIntestatario;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbCVV;
        private System.Windows.Forms.NumericUpDown nudMeseScadenza;
        private System.Windows.Forms.NumericUpDown nudAnnoScadenza;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbNomePersonalizzato;
        private System.Windows.Forms.Button btAnnulla;
        private System.Windows.Forms.Button btSalva;
    }
}