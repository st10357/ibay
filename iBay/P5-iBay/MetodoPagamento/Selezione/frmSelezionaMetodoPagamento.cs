﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace P5_iBay
{
    public partial class frmSelezionaMetodoPagamento : Form
    {
        public frmSelezionaMetodoPagamento()
        {
            InitializeComponent();
        }

        private void btCartaCredito_Click(object sender, EventArgs e)
        {
            
            frmCarteSalvate carteSalvate = new frmCarteSalvate();
            carteSalvate.ShowDialog();
            Close();
        }

        private void frmSelezionaMetodoPagamento_Load(object sender, EventArgs e)
        {
            Program.roudedEdges(btCartaCredito);
            Program.roudedEdges(btContoCorrente);
        }

        private void btContoCorrente_Click(object sender, EventArgs e)
        {
            frmContiCorrente contiCorrente = new frmContiCorrente();
            contiCorrente.ShowDialog();
            Close();
        }
    }
}
