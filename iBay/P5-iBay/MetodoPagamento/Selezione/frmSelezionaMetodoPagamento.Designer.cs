﻿namespace P5_iBay
{
    partial class frmSelezionaMetodoPagamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btCartaCredito = new System.Windows.Forms.Button();
            this.btContoCorrente = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btCartaCredito
            // 
            this.btCartaCredito.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btCartaCredito.FlatAppearance.BorderSize = 0;
            this.btCartaCredito.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCartaCredito.Location = new System.Drawing.Point(12, 12);
            this.btCartaCredito.Name = "btCartaCredito";
            this.btCartaCredito.Size = new System.Drawing.Size(145, 58);
            this.btCartaCredito.TabIndex = 0;
            this.btCartaCredito.Text = "CARTA DI CREDITO";
            this.btCartaCredito.UseVisualStyleBackColor = false;
            this.btCartaCredito.Click += new System.EventHandler(this.btCartaCredito_Click);
            // 
            // btContoCorrente
            // 
            this.btContoCorrente.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btContoCorrente.FlatAppearance.BorderSize = 0;
            this.btContoCorrente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btContoCorrente.Location = new System.Drawing.Point(163, 12);
            this.btContoCorrente.Name = "btContoCorrente";
            this.btContoCorrente.Size = new System.Drawing.Size(145, 58);
            this.btContoCorrente.TabIndex = 1;
            this.btContoCorrente.Text = "CONTO CORRENTE";
            this.btContoCorrente.UseVisualStyleBackColor = false;
            this.btContoCorrente.Click += new System.EventHandler(this.btContoCorrente_Click);
            // 
            // frmSelezionaMetodoPagamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(326, 87);
            this.Controls.Add(this.btContoCorrente);
            this.Controls.Add(this.btCartaCredito);
            this.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmSelezionaMetodoPagamento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SELEZIONA METODO PAGAMENTO";
            this.Load += new System.EventHandler(this.frmSelezionaMetodoPagamento_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btCartaCredito;
        private System.Windows.Forms.Button btContoCorrente;
    }
}