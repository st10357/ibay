﻿namespace P5_iBay
{
    partial class frmContiCorrente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmContiCorrente));
            this.lvContiCorrente = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btUsaCarta = new System.Windows.Forms.Button();
            this.lblNuovoConto = new System.Windows.Forms.LinkLabel();
            this.pbNuovaCarta = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbNuovaCarta)).BeginInit();
            this.SuspendLayout();
            // 
            // lvContiCorrente
            // 
            this.lvContiCorrente.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.lvContiCorrente.FullRowSelect = true;
            this.lvContiCorrente.GridLines = true;
            this.lvContiCorrente.HideSelection = false;
            this.lvContiCorrente.Location = new System.Drawing.Point(15, 11);
            this.lvContiCorrente.Margin = new System.Windows.Forms.Padding(4);
            this.lvContiCorrente.MultiSelect = false;
            this.lvContiCorrente.Name = "lvContiCorrente";
            this.lvContiCorrente.Size = new System.Drawing.Size(751, 493);
            this.lvContiCorrente.TabIndex = 0;
            this.lvContiCorrente.UseCompatibleStateImageBehavior = false;
            this.lvContiCorrente.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "IBAN";
            this.columnHeader1.Width = 106;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "NOME";
            this.columnHeader2.Width = 132;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "COGNOME";
            this.columnHeader3.Width = 201;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "NOME PERSONALIZZATO";
            this.columnHeader4.Width = 305;
            // 
            // btUsaCarta
            // 
            this.btUsaCarta.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(61)))), ((int)(((byte)(124)))));
            this.btUsaCarta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btUsaCarta.FlatAppearance.BorderSize = 0;
            this.btUsaCarta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btUsaCarta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btUsaCarta.ForeColor = System.Drawing.Color.White;
            this.btUsaCarta.Location = new System.Drawing.Point(609, 515);
            this.btUsaCarta.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btUsaCarta.Name = "btUsaCarta";
            this.btUsaCarta.Size = new System.Drawing.Size(157, 23);
            this.btUsaCarta.TabIndex = 20;
            this.btUsaCarta.Text = "Utilizza Conto Corrente";
            this.btUsaCarta.UseVisualStyleBackColor = false;
            this.btUsaCarta.Click += new System.EventHandler(this.btUsaCarta_Click);
            // 
            // lblNuovoConto
            // 
            this.lblNuovoConto.AutoSize = true;
            this.lblNuovoConto.Location = new System.Drawing.Point(41, 515);
            this.lblNuovoConto.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNuovoConto.Name = "lblNuovoConto";
            this.lblNuovoConto.Size = new System.Drawing.Size(159, 18);
            this.lblNuovoConto.TabIndex = 21;
            this.lblNuovoConto.TabStop = true;
            this.lblNuovoConto.Text = "Inserisci nuovo conto";
            this.lblNuovoConto.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblNuovoConto_LinkClicked);
            // 
            // pbNuovaCarta
            // 
            this.pbNuovaCarta.BackgroundImage = global::P5_iBay.Properties.Resources.add;
            this.pbNuovaCarta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbNuovaCarta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbNuovaCarta.InitialImage = null;
            this.pbNuovaCarta.Location = new System.Drawing.Point(15, 514);
            this.pbNuovaCarta.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbNuovaCarta.Name = "pbNuovaCarta";
            this.pbNuovaCarta.Size = new System.Drawing.Size(23, 23);
            this.pbNuovaCarta.TabIndex = 22;
            this.pbNuovaCarta.TabStop = false;
            // 
            // frmContiCorrente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(780, 548);
            this.Controls.Add(this.pbNuovaCarta);
            this.Controls.Add(this.lblNuovoConto);
            this.Controls.Add(this.btUsaCarta);
            this.Controls.Add(this.lvContiCorrente);
            this.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmContiCorrente";
            this.Text = "Conti corrente";
            this.Load += new System.EventHandler(this.frmContiCorrente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbNuovaCarta)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvContiCorrente;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button btUsaCarta;
        private System.Windows.Forms.PictureBox pbNuovaCarta;
        private System.Windows.Forms.LinkLabel lblNuovoConto;
        private System.Windows.Forms.ColumnHeader columnHeader4;
    }
}