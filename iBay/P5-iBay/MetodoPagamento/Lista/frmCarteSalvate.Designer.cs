﻿namespace P5_iBay
{
    partial class frmCarteSalvate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCarteSalvate));
            this.lvCarte = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btUsaCarta = new System.Windows.Forms.Button();
            this.lblNuovaCarta = new System.Windows.Forms.LinkLabel();
            this.pbNuovaCarta = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbNuovaCarta)).BeginInit();
            this.SuspendLayout();
            // 
            // lvCarte
            // 
            this.lvCarte.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lvCarte.FullRowSelect = true;
            this.lvCarte.GridLines = true;
            this.lvCarte.HideSelection = false;
            this.lvCarte.Location = new System.Drawing.Point(9, 9);
            this.lvCarte.MultiSelect = false;
            this.lvCarte.Name = "lvCarte";
            this.lvCarte.Size = new System.Drawing.Size(599, 519);
            this.lvCarte.TabIndex = 20;
            this.lvCarte.UseCompatibleStateImageBehavior = false;
            this.lvCarte.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Codice carta";
            this.columnHeader1.Width = 203;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Nome personalizzato";
            this.columnHeader2.Width = 208;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Data scadenza";
            this.columnHeader3.Width = 182;
            // 
            // btUsaCarta
            // 
            this.btUsaCarta.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(61)))), ((int)(((byte)(124)))));
            this.btUsaCarta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btUsaCarta.FlatAppearance.BorderSize = 0;
            this.btUsaCarta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btUsaCarta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btUsaCarta.ForeColor = System.Drawing.Color.White;
            this.btUsaCarta.Location = new System.Drawing.Point(451, 536);
            this.btUsaCarta.Name = "btUsaCarta";
            this.btUsaCarta.Size = new System.Drawing.Size(157, 23);
            this.btUsaCarta.TabIndex = 19;
            this.btUsaCarta.Text = "Utilizza questa carta";
            this.btUsaCarta.UseVisualStyleBackColor = false;
            this.btUsaCarta.Click += new System.EventHandler(this.btUsaCarta_Click);
            // 
            // lblNuovaCarta
            // 
            this.lblNuovaCarta.AutoSize = true;
            this.lblNuovaCarta.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNuovaCarta.Location = new System.Drawing.Point(35, 538);
            this.lblNuovaCarta.Name = "lblNuovaCarta";
            this.lblNuovaCarta.Size = new System.Drawing.Size(138, 17);
            this.lblNuovaCarta.TabIndex = 17;
            this.lblNuovaCarta.TabStop = true;
            this.lblNuovaCarta.Text = "Inserisci nuova carta";
            this.lblNuovaCarta.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblNuovaCarta_LinkClicked);
            // 
            // pbNuovaCarta
            // 
            this.pbNuovaCarta.BackgroundImage = global::P5_iBay.Properties.Resources.add;
            this.pbNuovaCarta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbNuovaCarta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbNuovaCarta.InitialImage = null;
            this.pbNuovaCarta.Location = new System.Drawing.Point(9, 536);
            this.pbNuovaCarta.Name = "pbNuovaCarta";
            this.pbNuovaCarta.Size = new System.Drawing.Size(23, 23);
            this.pbNuovaCarta.TabIndex = 18;
            this.pbNuovaCarta.TabStop = false;
            // 
            // frmCarteSalvate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(628, 569);
            this.Controls.Add(this.lvCarte);
            this.Controls.Add(this.btUsaCarta);
            this.Controls.Add(this.pbNuovaCarta);
            this.Controls.Add(this.lblNuovaCarta);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmCarteSalvate";
            this.Text = "Carte di credito";
            ((System.ComponentModel.ISupportInitialize)(this.pbNuovaCarta)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvCarte;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button btUsaCarta;
        private System.Windows.Forms.PictureBox pbNuovaCarta;
        private System.Windows.Forms.LinkLabel lblNuovaCarta;
    }
}