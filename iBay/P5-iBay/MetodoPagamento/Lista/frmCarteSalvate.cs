﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySqlConnector;

namespace P5_iBay
{
    // Parte di Dello Preite Maurizio
    public partial class frmCarteSalvate : Form
    {
        #region Costruttore
        public frmCarteSalvate()
        {
            InitializeComponent();

            CaricaCarteCredito();
        }
        #endregion

        #region Eventi
        private void lblNuovaCarta_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // Apri la Form details carta di credito in modalità inserimento
            FrmDetailsCartaCredito frmDetailsCartaCredito = new FrmDetailsCartaCredito(null);
            DialogResult dr = frmDetailsCartaCredito.ShowDialog();

            // Refresh della lista di conti se l'operazione è riuscita
            if (dr == DialogResult.OK)
                CaricaCarteCredito();
        }

        private void btUsaCarta_Click(object sender, EventArgs e)
        {
            // Confermo la selezione solo se è stata selezionata una carta
            if (lvCarte.SelectedIndices.Count == 1)
            {
                // Ottieni la riga selezionata
                ListViewItem selectedItem = lvCarte.SelectedItems[0]; // Assume che sia stato selezionato solo un elemento

                // Ottieni il contenuto della casella in una colonna specifica (per esempio, la prima colonna)
                Program.codiceCartaSelezionata = selectedItem.SubItems[0].Text;
                
                this.Close();
            }
            else
                MessageBox.Show("Non è stata selezionata alcuna carta. Selezionarne una e riprovare.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        #endregion

        #region Metodi
        /// <summary>
        /// Carica tutte le carte di credito dell'utente connesso sulla ListView
        /// </summary>
        private void CaricaCarteCredito()
        {
            // Ottenimento carte dell'utente connesso dal DB
            MySqlConnection _connection = new MySqlConnection(Program.CONNECTION_STRING);

            try
            {
                _connection.Open();
                MySqlCommand _query = new MySqlCommand($"SELECT * FROM cartedicredito WHERE emailUtente = '{Program._email}'", _connection);
                MySqlDataReader _reader = _query.ExecuteReader();

                // Inserimento carte sulla ListView
                while (_reader.Read())
                {
                    ListViewItem lv = new ListViewItem();
                    lv.Text = _reader["CodiceCarta"] as string;
                    lv.SubItems.Add((string)_reader["NomePersonalizzato"]);
                    lv.SubItems.Add(Convert.ToString(_reader["MeseScadenza"]) + "/" + Convert.ToString(_reader["AnnoScadenza"]));
                    lvCarte.Items.Add(lv);

                    columnHeader1.AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
                    columnHeader2.AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
                    columnHeader3.AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
                }

                _connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }
        #endregion
    }
}