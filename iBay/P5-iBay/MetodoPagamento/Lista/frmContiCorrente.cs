﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySqlConnector;

namespace P5_iBay
{
    public partial class frmContiCorrente : Form
    {
        #region Costruttore
        public frmContiCorrente()
        {
            InitializeComponent();
        }
        #endregion

        #region Eventi
        private void frmContiCorrente_Load(object sender, EventArgs e)
        {
            // Carica tutti i conti corrente sulla ListView all'avvio della Form
            CaricaContiCorrente();
        }

        private void lblNuovoConto_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // Apri la Form details conto corrente in modalità inserimento
            FrmDetailsContoCorrente frmInserimentoContoCorrente = new FrmDetailsContoCorrente(null);
            DialogResult dr = frmInserimentoContoCorrente.ShowDialog();

            // Refresh della lista di conti se l'operazione è riuscita
            if (dr == DialogResult.OK)
                CaricaContiCorrente();
        }

        private void btUsaCarta_Click(object sender, EventArgs e)
        {
            // Confermo la selezione solo se è stata selezionata una carta
            if (lvContiCorrente.SelectedIndices.Count == 1)
            {
                // Ottieni la riga selezionata
                ListViewItem selectedItem = lvContiCorrente.SelectedItems[0]; // Assume che sia stato selezionato solo un elemento

                // Ottieni il contenuto della casella in una colonna specifica (per esempio, la prima colonna)
                Program.ibanContoSelezionato = selectedItem.SubItems[0].Text;
                this.Close();
            }
            else
                MessageBox.Show("Non è stata selezionato alcun conto corrente. Selezionarne uno e riprovare.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            
        }
        #endregion

        #region Metodi
        private void CaricaContiCorrente()
        {
            // Effettuo la connessione al DB
            MySqlConnection _connection = new MySqlConnection(Program.CONNECTION_STRING);

            try
            {
                _connection.Open();
                MySqlCommand _query = new MySqlCommand($"SELECT * FROM conticorrente", _connection);
                MySqlDataReader _reader = _query.ExecuteReader();

                bool lettura = false;

                while (_reader.Read())
                {
                    lettura = true;
                    ListViewItem lv = new ListViewItem();
                    lv.Text = _reader["IBAN"] as string;


                    lv.SubItems.Add((string)_reader["NomeIntestatario"]);
                    lv.SubItems.Add(Convert.ToString(_reader["CognomeIntestatario"]));
                    lv.SubItems.Add(Convert.ToString(_reader["NomePersonalizzato"]));
                    lvContiCorrente.Items.Add(lv);

                    columnHeader1.AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
                    columnHeader2.AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
                    columnHeader3.AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
                    columnHeader4.AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
                }

                _connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }
        #endregion
    }
}
