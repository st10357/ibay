﻿namespace P5_iBay
{
    partial class FrmAmministratore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lv_utenti = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bt_setAdmin = new System.Windows.Forms.Button();
            this.bt_setVenditore = new System.Windows.Forms.Button();
            this.bt_delete = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lv_utenti
            // 
            this.lv_utenti.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.lv_utenti.FullRowSelect = true;
            this.lv_utenti.HideSelection = false;
            this.lv_utenti.Location = new System.Drawing.Point(12, 12);
            this.lv_utenti.Name = "lv_utenti";
            this.lv_utenti.Size = new System.Drawing.Size(623, 341);
            this.lv_utenti.TabIndex = 0;
            this.lv_utenti.UseCompatibleStateImageBehavior = false;
            this.lv_utenti.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Email";
            this.columnHeader1.Width = 196;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Nome";
            this.columnHeader2.Width = 116;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Cognome";
            this.columnHeader3.Width = 117;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Amministratore";
            this.columnHeader4.Width = 83;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Venditore";
            this.columnHeader5.Width = 68;
            // 
            // bt_setAdmin
            // 
            this.bt_setAdmin.Location = new System.Drawing.Point(641, 12);
            this.bt_setAdmin.Name = "bt_setAdmin";
            this.bt_setAdmin.Size = new System.Drawing.Size(147, 24);
            this.bt_setAdmin.TabIndex = 1;
            this.bt_setAdmin.Text = "Toggle Amministratore";
            this.bt_setAdmin.UseVisualStyleBackColor = true;
            this.bt_setAdmin.Click += new System.EventHandler(this.bt_setAdmin_Click);
            // 
            // bt_setVenditore
            // 
            this.bt_setVenditore.Location = new System.Drawing.Point(641, 42);
            this.bt_setVenditore.Name = "bt_setVenditore";
            this.bt_setVenditore.Size = new System.Drawing.Size(147, 24);
            this.bt_setVenditore.TabIndex = 2;
            this.bt_setVenditore.Text = "Toggle Venditore";
            this.bt_setVenditore.UseVisualStyleBackColor = true;
            this.bt_setVenditore.Click += new System.EventHandler(this.bt_setVenditore_Click);
            // 
            // bt_delete
            // 
            this.bt_delete.Location = new System.Drawing.Point(641, 72);
            this.bt_delete.Name = "bt_delete";
            this.bt_delete.Size = new System.Drawing.Size(147, 24);
            this.bt_delete.TabIndex = 3;
            this.bt_delete.Text = "Rimuovi utente";
            this.bt_delete.UseVisualStyleBackColor = true;
            this.bt_delete.Click += new System.EventHandler(this.bt_delete_Click);
            // 
            // FrmAmministratore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 365);
            this.Controls.Add(this.bt_delete);
            this.Controls.Add(this.bt_setVenditore);
            this.Controls.Add(this.bt_setAdmin);
            this.Controls.Add(this.lv_utenti);
            this.Name = "FrmAmministratore";
            this.Text = "Area amministratore";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lv_utenti;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Button bt_setAdmin;
        private System.Windows.Forms.Button bt_setVenditore;
        private System.Windows.Forms.Button bt_delete;
    }
}