﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySqlConnector;

namespace P5_iBay
{
    // Parte di Filonzi Jacopo
    public partial class FrmAmministratore : Form
    {
        MySqlConnection connection = new MySqlConnection("Server=127.0.0.1;User ID=root;Password=root;Database=ibay");


        public FrmAmministratore()
        {
            InitializeComponent();
            initializeListView();
        }

        private void initializeListView()
        {
            lv_utenti.Items.Clear();

            connection.Open();

            var command = new MySqlCommand($"SELECT Email, Nome, Cognome, Amministratore, Venditore FROM utenti", connection);

            var reader = command.ExecuteReader();

            while (reader.Read())
            {
                ListViewItem row = new ListViewItem(reader.GetString(0));
                row.Tag = reader.GetString(0);

                row.SubItems.Add(reader.GetString("Nome"));
                row.SubItems.Add(reader.GetString("Cognome"));
                row.SubItems.Add(reader.GetInt16("Amministratore") == 1 ? "Si" : "No");
                row.SubItems.Add(reader.GetInt16("Venditore") == 1 ? "Si" : "No");

                lv_utenti.Items.Add(row);
            }

            connection.Close();

        }

        private void bt_setAdmin_Click(object sender, EventArgs e)
        {
            if (lv_utenti.SelectedItems.Count != 1)
                MessageBox.Show("Seleziona un utente");
            else
            {
                string toggle = lv_utenti.SelectedItems[0].SubItems[3].Text == "Si" ? "0" : "1";

                connection.Open();
                var command = new MySqlCommand($"UPDATE utenti SET Amministratore='{toggle}' WHERE Email='{lv_utenti.SelectedItems[0].Tag as string}'", connection);
                command.ExecuteReader();
                connection.Close();

                initializeListView();
            }
        }
        private void bt_setVenditore_Click(object sender, EventArgs e)
        {
            if (lv_utenti.SelectedItems.Count != 1)
                MessageBox.Show("Seleziona un utente");
            else
            {
                string toggle = lv_utenti.SelectedItems[0].SubItems[4].Text == "Si" ? "0" : "1";

                connection.Open();
                var command = new MySqlCommand($"UPDATE utenti SET Venditore='{toggle}' WHERE Email='{lv_utenti.SelectedItems[0].Tag as string}'", connection);
                command.ExecuteReader();
                connection.Close();

                initializeListView();
            }
        }


        private void bt_delete_Click(object sender, EventArgs e)
        {
            if (lv_utenti.SelectedItems.Count != 1)
                MessageBox.Show("Seleziona un utente");
            else
            {
                DialogResult dr = MessageBox.Show("Sei sicuro di voler eliminare questo utente?", "ATTENZIONE", MessageBoxButtons.YesNo);

                if (dr == DialogResult.No)
                    return;

                connection.Open();
                var command = new MySqlCommand($"DELETE FROM utenti WHERE Email='{lv_utenti.SelectedItems[0].Tag as string}'", connection);
                command.ExecuteReader();
                connection.Close();

                initializeListView();

            }


        }


    }
}
