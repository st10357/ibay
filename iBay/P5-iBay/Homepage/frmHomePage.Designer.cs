﻿namespace P5_iBay
{
    partial class frmHomePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHomePage));
            this.lbCategorie = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlProdotti = new System.Windows.Forms.Panel();
            this.lbCarrello = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblProdotti = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pbOrdini = new System.Windows.Forms.PictureBox();
            this.pbLogout = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pbLinea = new System.Windows.Forms.PictureBox();
            this.pbCarrello = new System.Windows.Forms.PictureBox();
            this.pbUtente = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbOrdini)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLinea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCarrello)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbUtente)).BeginInit();
            this.SuspendLayout();
            // 
            // lbCategorie
            // 
            this.lbCategorie.FormattingEnabled = true;
            this.lbCategorie.ItemHeight = 16;
            this.lbCategorie.Items.AddRange(new object[] {
            "BELLEZZA",
            "SALUTE",
            "ALIMENTARI",
            "PRIMA INFANZIA",
            "ANIMALI",
            "CASA",
            "SPORT",
            "TEMPO LIBERO",
            "INDUSTRIA",
            "SCIENZA",
            "MODA",
            "GIARDINO",
            "ELETTRONICA",
            "FAI DA TE",
            "VIDEOGIOCHI",
            "AUTO MOTO",
            "CANCELLERIA",
            "INFORMATICA",
            "GIOCHI",
            "CD VINILI",
            "ELETTRODOMESTICI",
            "ALTRO",
            "TUTTO"});
            this.lbCategorie.Location = new System.Drawing.Point(19, 198);
            this.lbCategorie.Name = "lbCategorie";
            this.lbCategorie.Size = new System.Drawing.Size(235, 596);
            this.lbCategorie.TabIndex = 2;
            this.lbCategorie.SelectedIndexChanged += new System.EventHandler(this.lbCategorie_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(53, 128);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(165, 29);
            this.label1.TabIndex = 3;
            this.label1.Text = "CATEGORIE";
            // 
            // pnlProdotti
            // 
            this.pnlProdotti.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlProdotti.AutoScroll = true;
            this.pnlProdotti.Location = new System.Drawing.Point(286, 198);
            this.pnlProdotti.Name = "pnlProdotti";
            this.pnlProdotti.Size = new System.Drawing.Size(1050, 596);
            this.pnlProdotti.TabIndex = 5;
            // 
            // lbCarrello
            // 
            this.lbCarrello.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCarrello.FormattingEnabled = true;
            this.lbCarrello.ItemHeight = 16;
            this.lbCarrello.Location = new System.Drawing.Point(1368, 198);
            this.lbCarrello.Name = "lbCarrello";
            this.lbCarrello.Size = new System.Drawing.Size(252, 596);
            this.lbCarrello.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1550, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 20);
            this.label3.TabIndex = 8;
            this.label3.Text = "UTENTE";
            // 
            // lblProdotti
            // 
            this.lblProdotti.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProdotti.AutoSize = true;
            this.lblProdotti.Font = new System.Drawing.Font("MS Reference Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProdotti.Location = new System.Drawing.Point(648, 132);
            this.lblProdotti.Name = "lblProdotti";
            this.lblProdotti.Size = new System.Drawing.Size(327, 34);
            this.lblProdotti.TabIndex = 9;
            this.lblProdotti.Text = "SFOGLIA I PRODOTTI";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(1380, 144);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 20);
            this.label5.TabIndex = 17;
            this.label5.Text = "CARRELLO";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1404, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 20);
            this.label2.TabIndex = 19;
            this.label2.Text = "LOGOUT";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1532, 144);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 20);
            this.label4.TabIndex = 20;
            this.label4.Text = "ORDINI";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox9.Image = global::P5_iBay.Properties.Resources.lineaOrizzontale;
            this.pictureBox9.Location = new System.Drawing.Point(1368, 45);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(114, 20);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 24;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.Image = global::P5_iBay.Properties.Resources.lineaOrizzontale;
            this.pictureBox3.Location = new System.Drawing.Point(1514, 45);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(109, 20);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 23;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox7.Image = global::P5_iBay.Properties.Resources.lineaVerticale;
            this.pictureBox7.Location = new System.Drawing.Point(1488, 4);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(20, 160);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 22;
            this.pictureBox7.TabStop = false;
            // 
            // pbOrdini
            // 
            this.pbOrdini.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbOrdini.Image = global::P5_iBay.Properties.Resources.orders;
            this.pbOrdini.Location = new System.Drawing.Point(1533, 71);
            this.pbOrdini.Name = "pbOrdini";
            this.pbOrdini.Size = new System.Drawing.Size(70, 70);
            this.pbOrdini.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbOrdini.TabIndex = 20;
            this.pbOrdini.TabStop = false;
            this.pbOrdini.Click += new System.EventHandler(this.pbOrdini_Click);
            // 
            // pbLogout
            // 
            this.pbLogout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbLogout.Image = global::P5_iBay.Properties.Resources.exit;
            this.pbLogout.Location = new System.Drawing.Point(1368, 4);
            this.pbLogout.Name = "pbLogout";
            this.pbLogout.Size = new System.Drawing.Size(35, 35);
            this.pbLogout.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLogout.TabIndex = 18;
            this.pbLogout.TabStop = false;
            this.pbLogout.Click += new System.EventHandler(this.pbLogout_Click);
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::P5_iBay.Properties.Resources.lineaOrizzontale;
            this.pictureBox8.Location = new System.Drawing.Point(19, 172);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(235, 20);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 15;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox6.Image = global::P5_iBay.Properties.Resources.lineaOrizzontale;
            this.pictureBox6.Location = new System.Drawing.Point(1368, 172);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(252, 20);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 14;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::P5_iBay.Properties.Resources.lineaVerticale;
            this.pictureBox5.Location = new System.Drawing.Point(260, 4);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(20, 790);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 11;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox4.Image = global::P5_iBay.Properties.Resources.lineaVerticale;
            this.pictureBox4.Location = new System.Drawing.Point(1342, 4);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(20, 790);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 10;
            this.pictureBox4.TabStop = false;
            // 
            // pbLinea
            // 
            this.pbLinea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbLinea.Image = global::P5_iBay.Properties.Resources.lineaOrizzontale;
            this.pbLinea.Location = new System.Drawing.Point(286, 172);
            this.pbLinea.Name = "pbLinea";
            this.pbLinea.Size = new System.Drawing.Size(1050, 20);
            this.pbLinea.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLinea.TabIndex = 4;
            this.pbLinea.TabStop = false;
            // 
            // pbCarrello
            // 
            this.pbCarrello.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbCarrello.Image = global::P5_iBay.Properties.Resources.shopping_cart;
            this.pbCarrello.Location = new System.Drawing.Point(1392, 71);
            this.pbCarrello.Name = "pbCarrello";
            this.pbCarrello.Size = new System.Drawing.Size(70, 70);
            this.pbCarrello.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbCarrello.TabIndex = 1;
            this.pbCarrello.TabStop = false;
            this.pbCarrello.Click += new System.EventHandler(this.pbCarrello_Click);
            // 
            // pbUtente
            // 
            this.pbUtente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbUtente.Image = ((System.Drawing.Image)(resources.GetObject("pbUtente.Image")));
            this.pbUtente.Location = new System.Drawing.Point(1514, 4);
            this.pbUtente.Name = "pbUtente";
            this.pbUtente.Size = new System.Drawing.Size(35, 35);
            this.pbUtente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbUtente.TabIndex = 0;
            this.pbUtente.TabStop = false;
            this.pbUtente.Click += new System.EventHandler(this.pbUtente_Click);
            // 
            // frmHomePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1635, 814);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pbOrdini);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pbLogout);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.lblProdotti);
            this.Controls.Add(this.lbCarrello);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pnlProdotti);
            this.Controls.Add(this.pbLinea);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbCategorie);
            this.Controls.Add(this.pbCarrello);
            this.Controls.Add(this.pbUtente);
            this.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1313, 828);
            this.Name = "frmHomePage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Homepage";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmHomePage_FormClosed);
            this.Load += new System.EventHandler(this.frmHomePage_Load);
            this.Resize += new System.EventHandler(this.frmHomePage_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbOrdini)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLinea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCarrello)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbUtente)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbUtente;
        private System.Windows.Forms.PictureBox pbCarrello;
        private System.Windows.Forms.ListBox lbCategorie;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pbLinea;
        private System.Windows.Forms.Panel pnlProdotti;
        private System.Windows.Forms.ListBox lbCarrello;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblProdotti;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pbLogout;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pbOrdini;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox9;
    }
}