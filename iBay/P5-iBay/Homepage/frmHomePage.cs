﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySqlConnector;
using System.IO;

namespace P5_iBay
{
    // Parte di Dello Preite Maurizio
    public partial class frmHomePage : Form
    {
        #region Variabili
        MySqlConnection _connection = new MySqlConnection(Program.CONNECTION_STRING);
        MySqlCommand _query;
        MySqlDataReader _reader;
        #endregion

        #region Costruttore
        public frmHomePage()
        {
            InitializeComponent();
        }
        #endregion

        #region Eventi
        private void frmHomePage_Load(object sender, EventArgs e)
        {
            //metodo per popolare la lista dei prodotti nel carrello dal file
            PopolaOggettiCarrello();

            if (Program.ProdottiCarrello.Count > 0)
                popolaLbCarrello();

            //popolo ogni userControl con i dettagli dei prodotti dal db
            popolaProdotti();

            // Nascondi la form di accesso
            this.Owner.Hide();
        }

        private void pbUtente_Click(object sender, EventArgs e)
        {
            // Apro la form con le informazioni sull'utente connesso
            FrmUtente frmUtente = new FrmUtente();
            frmUtente.ShowDialog();
            popolaProdotti();
        }

        private void pbCarrello_Click(object sender, EventArgs e)
        {
            // Apri la form carrello
            Program.riepilogoCarrello = true;
            frmCarrello frmCarrello = new frmCarrello();
            frmCarrello.ShowDialog();
        }

        private void lbCategorie_SelectedIndexChanged(object sender, EventArgs e)
        {
            popolaProdotti();
        }

        private void frmHomePage_Resize(object sender, EventArgs e)
        {
            int x, y = 132;

            x = (pbLinea.Width + lblProdotti.Width) / 2;


            lblProdotti.Location = new Point(x, y);
        }

        private void pbLogout_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Abort;
            Owner.Show();
            Close();
        }

        private void frmHomePage_FormClosed(object sender, FormClosedEventArgs e)
        {
            // Indico che ho chiuso l'app (quindi si deve chiudere anche la Login)
            // (Barbatrucco)
            if(DialogResult != DialogResult.Abort)
                DialogResult = DialogResult.Cancel;
        }

        private void pbOrdini_Click(object sender, EventArgs e)
        {
            FrmOrdini frmOrdini = new FrmOrdini(Program._email);
            frmOrdini.ShowDialog();
        }
        #endregion

        #region Metodi
        private string RicavaProdotto(int id)
        {
            string nomeProdotto = "";

            // Effettuo la connessione al DB
            MySqlConnection _connection = new MySqlConnection(Program.CONNECTION_STRING);

            try
            {
                //apro la connessione
                _connection.Open();

                //invio il comando sql
                MySqlCommand _query = new MySqlCommand($"SELECT * FROM prodotti WHERE ID = {id}", _connection);
                MySqlDataReader _reader = _query.ExecuteReader();

                _reader.Read();

                //mi acquisisco il nome prodotto
                nomeProdotto = Convert.ToString(_reader["Nome"]);

                //chiude la connessione
                _connection.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return nomeProdotto;
        }

        public void popolaProdotti()   //altezza uc - 180 + 10 di distanza
        {
            // Variabili per la posizione
            int x = 10, y = 10;

            // Elimina tutti i prodotti presenti sul panel prodotti
            pnlProdotti.Controls.Clear();

            try
            {
                _connection.Open();

                // Ottengo i prodotti dal DB
                if (lbCategorie.SelectedIndex == -1 || lbCategorie.SelectedIndex == 22)
                {
                    _query = new MySqlCommand("SELECT * FROM prodotti", _connection);
                    _reader = _query.ExecuteReader();
                }
                else
                {
                    _query = new MySqlCommand($"SELECT * FROM prodotti WHERE categoria = '{lbCategorie.SelectedIndex}'", _connection);
                    _reader = _query.ExecuteReader();
                }

                while (_reader.Read())
                {
                    if(Convert.ToInt32(_reader["Quantita"]) > 0)
                    {
                        //dichiarazione userControl
                        ucProdotto ucProdotto = new ucProdotto();

                        //posizione dell'user control nel panel
                        ucProdotto.Location = new Point(x, y);

                        //accedo agli elementi grafici dell'userControl
                        Label lblNomeProdotto = ucProdotto.Controls["lblNomeProdotto"] as Label;
                        Label lblCategoria = ucProdotto.Controls["lblCategoria"] as Label;
                        Label lblQuantita = ucProdotto.Controls["lblQuantita"] as Label;
                        Label lblPeso = ucProdotto.Controls["lblPeso"] as Label;
                        Label lblPrezzo = ucProdotto.Controls["lblPrezzo"] as Label;
                        Label lblDescrizione = ucProdotto.Controls["lblDescrizione"] as Label;
                        Label lblDimensioni = ucProdotto.Controls["lblDimensioni"] as Label;

                        //modifico i campi precedentemente acquisiti
                        lblNomeProdotto.Text = Convert.ToString(_reader["Nome"]);
                        lblCategoria.Text = "Categoria - " + Enum.GetName(typeof(Program.eCategoriaProdotto), Convert.ToInt32(_reader["Categoria"]));
                        lblQuantita.Text = "Quantità - " + Convert.ToString(_reader["Quantita"]) + " pcs";
                        lblPeso.Text = "Peso - " + Convert.ToString(_reader["Peso"]) + " grammi";
                        lblPrezzo.Text = "Prezzo - €" + Convert.ToString(_reader["Prezzo"]);
                        lblDescrizione.Text = Convert.ToString(_reader["Descrizione"]);
                        lblDimensioni.Text = "Lunghezza - " + Convert.ToString(_reader["Lunghezza"]) + "\nAltezza - " + Convert.ToString(_reader["Lunghezza"]) + "\nProfondità - " + Convert.ToString(_reader["Profondita"]);
                        ucProdotto.idProdotto = Convert.ToInt32(_reader["ID"]);

                        //aggiungo l'uc al panel
                        pnlProdotti.Controls.Add(ucProdotto);

                        //spaziatura per il prossimo uc
                        y += 190;
                    }
                }

                _connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Errore di connessione al DB:\r\n" + ex.ToString(), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void popolaLbCarrello()
        {
            lbCarrello.Items.Clear();

            foreach (Program.sProdottiCarrello Prodotto in Program.ProdottiCarrello)
            {
                if (Prodotto.emailUtente == Program._email)
                {
                    //mi ricavo il nome del prodotto dall'ID
                    string nomeProdotto = RicavaProdotto(Prodotto.idProdotto);

                    lbCarrello.Items.Add(nomeProdotto + " x" + Prodotto.quantitaSelezionata);
                }
            }
        }

        private void PopolaOggettiCarrello()
        {
            Program.ProdottiCarrello.Clear();

            if (File.Exists(Program.pathCarrello))
            {
                StreamReader sr = new StreamReader(Program.pathCarrello);

                string riga = sr.ReadLine();

                while (riga != null)
                {
                    string[] campi = riga.Split('|');

                    Program.ProdottoCarrello.emailUtente = campi[0];
                    Program.ProdottoCarrello.idProdotto = Convert.ToInt16(campi[1]);
                    Program.ProdottoCarrello.quantitaSelezionata = Convert.ToInt16(campi[2]);

                    Program.ProdottiCarrello.Add(Program.ProdottoCarrello);

                    riga = sr.ReadLine();
                }
                sr.Close();
            }
            else
                File.Create(Program.pathCarrello);
        }
        #endregion
    }
}
