-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Creato il: Set 26, 2023 alle 08:52
-- Versione del server: 8.0.29
-- Versione PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ibay`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `cartedicredito`
--

CREATE TABLE `cartedicredito` (
  `CodiceCarta` char(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'Codice numerico della carta',
  `emailUtente` varchar(50) NOT NULL COMMENT 'Chiave secondaria che associa la carta all''utente',
  `NomeIntestatario` varchar(50) NOT NULL COMMENT 'Nome dell''intestatario',
  `CognomeIntestatario` varchar(50) NOT NULL COMMENT 'Cognome dell''intestatario',
  `CVV` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'CVV della carta',
  `MeseScadenza` tinyint NOT NULL COMMENT 'Mese di scadenza della carta',
  `AnnoScadenza` smallint NOT NULL COMMENT 'Anno di scadenza della carta',
  `NomePersonalizzato` varchar(30) NOT NULL COMMENT 'Nome personalizzato della carta'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dump dei dati per la tabella `cartedicredito`
--

INSERT INTO `cartedicredito` (`CodiceCarta`, `emailUtente`, `NomeIntestatario`, `CognomeIntestatario`, `CVV`, `MeseScadenza`, `AnnoScadenza`, `NomePersonalizzato`) VALUES
('1234567891234567', 'admin@admin.it', 'Mario', 'Rossi', '104', 3, 2025, 'Mros');

-- --------------------------------------------------------

--
-- Struttura della tabella `conticorrente`
--

CREATE TABLE `conticorrente` (
  `emailUtente` varchar(50) NOT NULL COMMENT 'ID dell''account a cui il conto è associato',
  `BIC` varchar(11) NOT NULL COMMENT 'BIC del conto corrente',
  `IBAN` char(27) NOT NULL COMMENT 'IBAN del conto corrente',
  `NomeIntestatario` varchar(50) NOT NULL COMMENT 'Nome dell''intestatario',
  `CognomeIntestatario` varchar(50) NOT NULL COMMENT 'Cognome dell''intestatario',
  `NomePersonalizzato` varchar(30) NOT NULL COMMENT 'Nome personalizzato del conto corrente'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dump dei dati per la tabella `conticorrente`
--

INSERT INTO `conticorrente` (`emailUtente`, `BIC`, `IBAN`, `NomeIntestatario`, `CognomeIntestatario`, `NomePersonalizzato`) VALUES
('admin@admin.it', '12345678885', '121425457234124677573452311', 'Mario', 'Rossi', 'RossMR');

-- --------------------------------------------------------

--
-- Struttura della tabella `indirizzispedizione`
--

CREATE TABLE `indirizzispedizione` (
  `ID` int NOT NULL COMMENT 'Identificativo dell''indirizzo',
  `emailUtente` varchar(50) NOT NULL COMMENT 'Identificativo dell''utente ',
  `Indirizzo` varchar(40) NOT NULL COMMENT 'Indirizzo di spedizione',
  `Città` varchar(50) NOT NULL COMMENT 'Città di spedizione',
  `Nome` varchar(50) NOT NULL COMMENT 'Nome del citofono',
  `CAP` char(5) NOT NULL COMMENT 'CAP della città',
  `Provincia` char(2) NOT NULL COMMENT 'Sigla della provincia',
  `Nazione` varchar(50) NOT NULL COMMENT 'Nome della nazione',
  `IstruzioniConsegna` text COMMENT 'Istruzioni di consegna',
  `TipoProprieta` char(1) NOT NULL COMMENT 'Tipo di proprietà (casa, appartamento, azienda)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dump dei dati per la tabella `indirizzispedizione`
--

INSERT INTO `indirizzispedizione` (`ID`, `emailUtente`, `Indirizzo`, `Città`, `Nome`, `CAP`, `Provincia`, `Nazione`, `IstruzioniConsegna`, `TipoProprieta`) VALUES
(2, 'admin@admin.it', 'via roma 2', 'Jesi', 'Mario Rossi', '60035', 'AN', 'Italia', '', '0');

-- --------------------------------------------------------

--
-- Struttura della tabella `ordini`
--

CREATE TABLE `ordini` (
  `IDOrdine` int NOT NULL COMMENT 'ID dell''ordine',
  `emailUtente` varchar(50) NOT NULL COMMENT 'ID dell''utente',
  `CodiceCartaCredito` char(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'ID del metodo di pagamento',
  `IBANcontoCorrente` char(27) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Stato` char(2) NOT NULL COMMENT 'Stato dell''ordine (elaborato, approvato, spedito, consegnato, reso)',
  `indirizzoSpedizioneID` int NOT NULL,
  `idProdotto` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dump dei dati per la tabella `ordini`
--

INSERT INTO `ordini` (`IDOrdine`, `emailUtente`, `CodiceCartaCredito`, `IBANcontoCorrente`, `Stato`, `indirizzoSpedizioneID`, `idProdotto`) VALUES
(21, 'admin@admin.it', '1234567891234567', NULL, '0', 2, 29);

-- --------------------------------------------------------

--
-- Struttura della tabella `ordiniprodotti`
--

CREATE TABLE `ordiniprodotti` (
  `IDProdotto` int NOT NULL COMMENT 'ID del prodotto',
  `IDOrdine` int NOT NULL COMMENT 'ID dell''ordine'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotti`
--

CREATE TABLE `prodotti` (
  `ID` int NOT NULL COMMENT 'ID del prodotto',
  `emailUtente` varchar(50) NOT NULL,
  `Nome` varchar(150) NOT NULL COMMENT 'Nome del prodotto',
  `Categoria` tinyint NOT NULL COMMENT 'Categoria del prodotto (bellezza, salute e cura della persona, alimentari, casa e cucina, sport e tempo libero, giardinaggio, elettronica e informatica, videogiochi, altro)',
  `Prezzo` double NOT NULL COMMENT 'Prezzo del prodotto',
  `Lunghezza` double NOT NULL COMMENT 'Lunghezza del prodotto',
  `Altezza` double NOT NULL COMMENT 'Altezza del prodotto',
  `Profondita` double NOT NULL COMMENT 'Profondità del prodotto',
  `Peso` int NOT NULL COMMENT 'Peso del prodotto',
  `Quantita` int NOT NULL COMMENT 'Quantità di pezzi disponibili del prodotto',
  `Descrizione` text NOT NULL COMMENT 'Descrizione del prodotto',
  `PIVAAzienda` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'Partita IVA dell''azienda'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dump dei dati per la tabella `prodotti`
--

INSERT INTO `prodotti` (`ID`, `emailUtente`, `Nome`, `Categoria`, `Prezzo`, `Lunghezza`, `Altezza`, `Profondita`, `Peso`, `Quantita`, `Descrizione`, `PIVAAzienda`) VALUES
(28, 'admin@admin.it', 'T-shirt orientata al dato', 10, 20, 0, 0, 0, 0, 7, 'Una t-shirt di colore nero perfettamente orientata al dato', '11111111111'),
(29, 'admin@admin.it', 'T-shirt purista', 10, 20, 0, 0, 0, 0, 8, 'Una t-shirt di colore rosso perfettamente purista', '11111111111'),
(30, 'admin@admin.it', 'DBMS', 17, 1000000, 0, 0, 0, 0, 103, 'DBMS ottimizzato per DB molto grandi', '11111111111');

-- --------------------------------------------------------

--
-- Struttura della tabella `recensioni`
--

CREATE TABLE `recensioni` (
  `ID` int NOT NULL COMMENT 'ID della recensione',
  `IDProdotto` int NOT NULL COMMENT 'ID del prodotto recensito',
  `EmailUtente` varchar(50) NOT NULL COMMENT 'Email dell''utente',
  `Stelle` int NOT NULL COMMENT 'Stelle della recensione',
  `Testo` text COMMENT 'Testo della recensione'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `utenti`
--

CREATE TABLE `utenti` (
  `CF` char(16) NOT NULL COMMENT 'Codice fiscale dell''utente a cui appartiene l''account',
  `Nome` varchar(50) NOT NULL COMMENT 'Nome dell''utente a cui appartiene l''account',
  `Cognome` varchar(50) NOT NULL COMMENT 'Cognome dell''utente a cui appartiene l''account',
  `P.IVA` char(11) DEFAULT NULL COMMENT 'Partita IVA dell''utente a cui appartiene l''account',
  `DataNascita` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data di nascita dell''utente a cui appartiene l''account',
  `Genere` char(1) DEFAULT NULL COMMENT 'Genere dell''utente a cui appartiene l''account (M = Maschio, F = Femmina, A = Altro)',
  `Email` varchar(50) NOT NULL COMMENT 'Email dell''utente a cui appartiene l''account',
  `NumeroCellulare` char(14) NOT NULL COMMENT 'Numero di cellulare dell''utente a cui appartiene l''account',
  `Password` varchar(50) NOT NULL COMMENT 'Password dell''account',
  `Venditore` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Questa flag definisce se l''utente è un venditore o meno',
  `Amministratore` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Questa flag definisce se l''utente è un amministratore o meno'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dump dei dati per la tabella `utenti`
--

INSERT INTO `utenti` (`CF`, `Nome`, `Cognome`, `P.IVA`, `DataNascita`, `Genere`, `Email`, `NumeroCellulare`, `Password`, `Venditore`, `Amministratore`) VALUES
('AAAAAAAAAAAAAAAA', 'Admin', 'Admin', NULL, '2023-09-24 16:26:40', 'A', 'admin@admin.it', '1044011004', 'admin', 1, 1),
('AAAAAAAAAAAAAAAA', 'Tomas', 'Sebastianelli', NULL, '2000-02-01 18:24:20', 'M', 'st10453@iismarconipieralisi.it', '3664567623', 'Tomas', 0, 0);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `cartedicredito`
--
ALTER TABLE `cartedicredito`
  ADD PRIMARY KEY (`CodiceCarta`),
  ADD KEY `emailUtente-cartaCredito` (`emailUtente`);

--
-- Indici per le tabelle `conticorrente`
--
ALTER TABLE `conticorrente`
  ADD PRIMARY KEY (`IBAN`),
  ADD KEY `emailUtente-contoCorrente` (`emailUtente`);

--
-- Indici per le tabelle `indirizzispedizione`
--
ALTER TABLE `indirizzispedizione`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `emailUtente-indirizziSpedizione` (`emailUtente`);

--
-- Indici per le tabelle `ordini`
--
ALTER TABLE `ordini`
  ADD PRIMARY KEY (`IDOrdine`),
  ADD KEY `IDcontoCorrente-ordine` (`IBANcontoCorrente`),
  ADD KEY `emailUtente-ordine` (`emailUtente`),
  ADD KEY `CodiceCartaCredito-ordine` (`CodiceCartaCredito`),
  ADD KEY `indirizzoSpedizione-ordini` (`indirizzoSpedizioneID`),
  ADD KEY `idProdotto` (`idProdotto`);

--
-- Indici per le tabelle `ordiniprodotti`
--
ALTER TABLE `ordiniprodotti`
  ADD KEY `idProdotto-ordine` (`IDProdotto`),
  ADD KEY `IdOrdine-prodotto` (`IDOrdine`);

--
-- Indici per le tabelle `prodotti`
--
ALTER TABLE `prodotti`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `emailUtente-prodotto` (`emailUtente`);

--
-- Indici per le tabelle `recensioni`
--
ALTER TABLE `recensioni`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `idProdotto-recensione` (`IDProdotto`),
  ADD KEY `emailUtente-recensione` (`EmailUtente`);

--
-- Indici per le tabelle `utenti`
--
ALTER TABLE `utenti`
  ADD PRIMARY KEY (`Email`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `indirizzispedizione`
--
ALTER TABLE `indirizzispedizione`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT COMMENT 'Identificativo dell''indirizzo', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `ordini`
--
ALTER TABLE `ordini`
  MODIFY `IDOrdine` int NOT NULL AUTO_INCREMENT COMMENT 'ID dell''ordine', AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT per la tabella `prodotti`
--
ALTER TABLE `prodotti`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT COMMENT 'ID del prodotto', AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT per la tabella `recensioni`
--
ALTER TABLE `recensioni`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT COMMENT 'ID della recensione', AUTO_INCREMENT=4;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `cartedicredito`
--
ALTER TABLE `cartedicredito`
  ADD CONSTRAINT `emailUtente-cartaCredito` FOREIGN KEY (`emailUtente`) REFERENCES `utenti` (`Email`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `conticorrente`
--
ALTER TABLE `conticorrente`
  ADD CONSTRAINT `emailUtente-contoCorrente` FOREIGN KEY (`emailUtente`) REFERENCES `utenti` (`Email`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `indirizzispedizione`
--
ALTER TABLE `indirizzispedizione`
  ADD CONSTRAINT `emailUtente-indirizziSpedizione` FOREIGN KEY (`emailUtente`) REFERENCES `utenti` (`Email`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `ordini`
--
ALTER TABLE `ordini`
  ADD CONSTRAINT `CodiceCartaCredito` FOREIGN KEY (`CodiceCartaCredito`) REFERENCES `cartedicredito` (`CodiceCarta`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `emailUtente` FOREIGN KEY (`emailUtente`) REFERENCES `utenti` (`Email`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `IBANcontoCorrente` FOREIGN KEY (`IBANcontoCorrente`) REFERENCES `conticorrente` (`IBAN`),
  ADD CONSTRAINT `indirizzoSpedizione` FOREIGN KEY (`indirizzoSpedizioneID`) REFERENCES `indirizzispedizione` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `ordiniprodotti`
--
ALTER TABLE `ordiniprodotti`
  ADD CONSTRAINT `idOrdine-ordiniprodotti` FOREIGN KEY (`IDOrdine`) REFERENCES `ordini` (`IDOrdine`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `idProdotto-ordiniprodotti` FOREIGN KEY (`IDProdotto`) REFERENCES `prodotti` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limiti per la tabella `prodotti`
--
ALTER TABLE `prodotti`
  ADD CONSTRAINT `emailUtente-prodotti` FOREIGN KEY (`emailUtente`) REFERENCES `utenti` (`Email`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `recensioni`
--
ALTER TABLE `recensioni`
  ADD CONSTRAINT `emailUtente-recensione` FOREIGN KEY (`EmailUtente`) REFERENCES `utenti` (`Email`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `idProdotto-recensione` FOREIGN KEY (`IDProdotto`) REFERENCES `prodotti` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
